package struct.impl;

import org.junit.Test;

import static org.junit.Assert.*;

public class DynamicQueueTest {
    @Test
    public void enqueueAndDequeue() throws Exception {
        DynamicQueue<Integer> DynamicQueue = new DynamicQueue<>();
        DynamicQueue.enqueue(new Integer(1));
        DynamicQueue.enqueue(new Integer(2));
        DynamicQueue.enqueue(new Integer(3));
        DynamicQueue.enqueue(new Integer(4));
        DynamicQueue.enqueue(new Integer(5));
        DynamicQueue.enqueue(new Integer(6));
        //System.out.println(DynamicQueue.dequeue());
        assertEquals(new Integer(1),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(2),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(3),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(4),DynamicQueue.dequeue(),0.1);
        DynamicQueue.enqueue(new Integer(7));
        DynamicQueue.enqueue(new Integer(8));
        assertEquals(new Integer(5),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(6),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(7),DynamicQueue.dequeue(),0.1);
        assertEquals(new Integer(8),DynamicQueue.dequeue(),0.1);
    }

    @Test
    public void isEmpty() throws Exception {
        DynamicQueue<Integer> dynamicQueue = new DynamicQueue<>();
        dynamicQueue.enqueue(new Integer(1));
        dynamicQueue.dequeue();
        assertTrue(dynamicQueue.isEmpty());
    }

}