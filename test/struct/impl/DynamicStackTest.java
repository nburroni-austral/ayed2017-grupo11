package struct.impl;

import org.junit.Test;

import static org.junit.Assert.*;

public class DynamicStackTest {
    @Test
    public void peekAndPop() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(5);
        stack.push(2);
        stack.push(7);
        stack.push(10);

        assertEquals(new Integer(10), stack.peek());
        stack.pop();
        assertEquals(new Integer(7), stack.peek());
        stack.pop();
        assertEquals(new Integer(2), stack.peek());
        stack.pop();
        assertEquals(new Integer(5), stack.peek());
    }

    @Test
    public void empty() throws Exception{
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(5);
        stack.push(2);
        stack.push(7);
        stack.push(10);
        assertFalse(stack.isEmpty());
        stack.empty();
        assertTrue(stack.isEmpty());
        stack.push(7);
        stack.push(10);
        while(stack.size() != 0){
            stack.pop();
        }
        assertTrue(stack.isEmpty());
    }

    @Test
    public void stackToString(){
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(5);
        stack.push(2);
        stack.push(7);
        stack.push(10);
        System.out.println(stack.toString());
    }
}