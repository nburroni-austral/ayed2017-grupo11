package struct.impl;

import org.junit.Test;

import static org.junit.Assert.*;

public class StaticQueueTest {

    @Test
    public void enqueueAndDequeue() throws Exception {
        StaticQueue<Integer> staticQueue = new StaticQueue(3);
        staticQueue.enqueue(new Integer(1));
        staticQueue.enqueue(new Integer(2));
        staticQueue.enqueue(new Integer(3));
        staticQueue.enqueue(new Integer(4));
        staticQueue.enqueue(new Integer(5));
        staticQueue.enqueue(new Integer(6));
        assertEquals(new Integer(1),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(2),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(3),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(4),staticQueue.dequeue(),0.1);
        staticQueue.enqueue(new Integer(7));
        staticQueue.enqueue(new Integer(8));
        assertEquals(new Integer(5),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(6),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(7),staticQueue.dequeue(),0.1);
        assertEquals(new Integer(8),staticQueue.dequeue(),0.1);
    }

    @Test
    public void isEmpty() throws Exception {
        StaticQueue<Integer> staticQueue = new StaticQueue(3);
        staticQueue.enqueue(new Integer(1));
        staticQueue.dequeue();
        assertTrue(staticQueue.isEmpty());
    }
}