package struct.impl;

import main.TP7.BinaryTreeAPI;
import main.TP7.BinaryTreeShower;
import org.junit.Test;
import struct.impl.Exceptions.ObjectAlreadyInTreeException;
import struct.impl.Exceptions.ObjectNotFoundException;

import static org.junit.Assert.*;

public class SearchBinaryTreeTest {
    BinaryTreeAPI btapi = new BinaryTreeAPI();
    BinaryTreeShower bts = new BinaryTreeShower();

    @Test
    public void insert() throws Exception{
        SearchBinaryTree<Integer> l2 = new SearchBinaryTree(2);
        SearchBinaryTree<Integer> r1 = new SearchBinaryTree(
                new Integer(21), new SearchBinaryTree(new Integer(20)),new SearchBinaryTree(new Integer(50)));
        SearchBinaryTree<Integer> l1 = new SearchBinaryTree(new Integer(10),l2,new SearchBinaryTree());
        SearchBinaryTree<Integer> sbt1 = new SearchBinaryTree(15,l1,r1);

        SearchBinaryTree<Integer> sbt = new SearchBinaryTree<>();
        sbt.insert(15);
        sbt.insert(10);
        sbt.insert(2);
        sbt.insert(21);
        sbt.insert(20);
        sbt.insert(50);
        try{
            sbt.insert(10);
            assertTrue(false);
        }
        catch (ObjectAlreadyInTreeException e){
            assertTrue(true);
        }

        System.out.println(bts.inOrderString(sbt));
        System.out.println(bts.inOrderString(sbt1));
        assertTrue(btapi.equals(sbt,sbt1));

    }

    @Test
    public void exists() throws Exception {
        SearchBinaryTree<Integer> sbt = new SearchBinaryTree<>();
        sbt.insert(32);
        sbt.insert(15);
        sbt.insert(53);
        sbt.insert(10);
        sbt.insert(2);
        sbt.insert(50);
        sbt.insert(21);
        sbt.insert(48);
        sbt.insert(35);


        assertTrue(sbt.exists(10));
        assertTrue(sbt.exists(48));
        assertTrue(sbt.exists(35));
        assertTrue(sbt.exists(2));
        assertFalse(sbt.exists(11));

    }

    @Test
    public void search() throws Exception {
        SearchBinaryTree<Integer> sbt = new SearchBinaryTree<>();
        sbt.insert(32);
        sbt.insert(15);
        sbt.insert(53);
        sbt.insert(10);
        sbt.insert(2);
        sbt.insert(50);
        sbt.insert(21);
        sbt.insert(48);
        sbt.insert(35);


        assertEquals(10,sbt.search(10),0.1);
        assertEquals(48,sbt.search(48),0.1);
        assertEquals(35,sbt.search(35),0.1);
        assertEquals(2,sbt.search(2),0.1);
        try {
            sbt.search(11);
            assertTrue(false);
        }
        catch (ObjectNotFoundException e) {
            assertTrue(true);
        }
    }

    @Test
    public void delete() throws Exception {

        SearchBinaryTree<Integer> sbt = new SearchBinaryTree();
        sbt.insert(15);
        sbt.insert(10);
        sbt.insert(2);
        sbt.insert(25);
        sbt.insert(20);
        sbt.insert(17);
        sbt.insert(23);
        sbt.insert(30);
        sbt.insert(27);
        sbt.insert(35);

        SearchBinaryTree<Integer> sbt1 = new SearchBinaryTree();
        sbt1.insert(15);
        sbt1.insert(10);
        sbt1.insert(2);
        sbt1.insert(27);
        sbt1.insert(20);
        sbt1.insert(17);
        sbt1.insert(23);
        sbt1.insert(30);
        sbt1.insert(35);

        sbt.delete(25);
        assertTrue(btapi.equals(sbt,sbt1));
        sbt.delete(35);
        assertFalse(sbt.exists(35));
        sbt.delete(10);
        assertFalse(sbt.exists(10));
        assertTrue(sbt.exists(2));
    }

    @Test
    public void getMinAndMax() throws Exception {

        SearchBinaryTree<Integer> sbt = new SearchBinaryTree();
        sbt.insert(15);
        sbt.insert(10);
        sbt.insert(2);
        sbt.insert(21);
        sbt.insert(20);
        sbt.insert(50);

        assertEquals(2,sbt.getMin(),0.1);
        assertEquals(50,sbt.getMax(),0.1);

    }


}