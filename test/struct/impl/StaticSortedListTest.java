package struct.impl;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Tomas Perez Molina
 */
public class StaticSortedListTest {
    @Test
    public void insert() throws Exception {
        StaticSortedList<Integer> sortedList = new StaticSortedList<>(2);
        sortedList.insert(6);
        sortedList.insert(1);
        sortedList.insert(52);
        sortedList.insert(0);
        sortedList.insert(-6);
        sortedList.insert(32);
        sortedList.insert(7);

        assertEquals(7, sortedList.size());

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAllMinor() throws Exception {
        StaticSortedList<Integer> sortedList = new StaticSortedList<>(2);
        sortedList.insert(6);
        sortedList.insert(5);
        sortedList.insert(4);
        sortedList.insert(3);
        sortedList.insert(2);
        sortedList.insert(1);
        sortedList.insert(0);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAllMayor() throws Exception {
        StaticSortedList<Integer> sortedList = new StaticSortedList<>(2);
        sortedList.insert(0);
        sortedList.insert(1);
        sortedList.insert(2);
        sortedList.insert(3);
        sortedList.insert(4);
        sortedList.insert(5);
        sortedList.insert(6);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertEqual() throws Exception {
        StaticSortedList<Integer> sortedList = new StaticSortedList<>(2);
        sortedList.insert(0);
        sortedList.insert(0);
        sortedList.insert(0);
        sortedList.insert(0);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAndRemove() throws Exception {
        StaticSortedList<Integer> sortedList = new StaticSortedList<>(2);
        sortedList.insert(6);
        sortedList.insert(1);
        sortedList.insert(52);
        sortedList.insert(0);
        sortedList.insert(-6);
        sortedList.insert(32);
        sortedList.insert(7);

        sortedList.goTo(3);
        sortedList.remove();

        assertTrue(ordered(sortedList));
    }


    public boolean ordered(StaticSortedList<Integer> sortedList) throws Exception{
        for(int i = 0; i < sortedList.size()-1; i++){
            sortedList.goTo(i);
            Integer actualValue = sortedList.getActual();
            sortedList.goTo(i+1);
            Integer nextValue = sortedList.getActual();
            if(actualValue.compareTo(nextValue) > 0) return false;
        }
        return true;
    }

}