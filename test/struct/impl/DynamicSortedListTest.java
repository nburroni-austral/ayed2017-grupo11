package struct.impl;

import org.junit.Test;
import java.util.Random;
import static org.junit.Assert.*;

/**
 * @author Tomas Perez Molina
 */
public class DynamicSortedListTest {
    @Test
    public void insert() throws Exception {
        DynamicSortedList<Integer> sortedList = new DynamicSortedList<>();
        sortedList.insert(6);
        sortedList.insert(1);
        sortedList.insert(52);
        sortedList.insert(0);
        sortedList.insert(-6);
        sortedList.insert(32);
        sortedList.insert(7);

        assertEquals(7, sortedList.size());

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAllMinor() throws Exception {
        DynamicSortedList<Integer> sortedList = new DynamicSortedList<>();
        sortedList.insert(6);
        sortedList.insert(5);
        sortedList.insert(4);
        sortedList.insert(3);
        sortedList.insert(2);
        sortedList.insert(1);
        sortedList.insert(0);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAllMayor() throws Exception {
        DynamicSortedList<Integer> sortedList = new DynamicSortedList<>();
        sortedList.insert(0);
        sortedList.insert(1);
        sortedList.insert(2);
        sortedList.insert(3);
        sortedList.insert(4);
        sortedList.insert(5);
        sortedList.insert(6);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertEqual() throws Exception {
        DynamicSortedList<Integer> sortedList = new DynamicSortedList<>();
        sortedList.insert(0);
        sortedList.insert(0);
        sortedList.insert(0);
        sortedList.insert(0);

        assertTrue(ordered(sortedList));
    }

    @Test
    public void insertAndRemove() throws Exception {
        DynamicSortedList<Integer> sortedList = new DynamicSortedList<>();
        sortedList.insert(6);
        sortedList.insert(1);
        sortedList.insert(52);
        sortedList.insert(0);
        sortedList.insert(-6);
        sortedList.insert(32);
        sortedList.insert(7);
        Random r = new Random();

        sortedList.goTo(r.nextInt(sortedList.size()));
        sortedList.remove();

        assertTrue(ordered(sortedList));
    }


    public boolean ordered(DynamicSortedList<Integer> sortedList) throws Exception{
        for(int i = 0; i < sortedList.size()-1; i++){
            sortedList.goTo(i);
            Integer actualValue = sortedList.getActual();
            sortedList.goTo(i+1);
            Integer nextValue = sortedList.getActual();
            if(actualValue.compareTo(nextValue) > 0) return false;
        }
        return true;
    }
}
