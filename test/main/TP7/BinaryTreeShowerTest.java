package main.TP7;

import org.junit.Test;
import struct.impl.BinaryTree;

public class BinaryTreeShowerTest {
    BinaryTreeShower binaryTreeShower = new BinaryTreeShower();

    BinaryTree<Character> a = new BinaryTree<>('a');
    BinaryTree<Character> c = new BinaryTree<>('c');
    BinaryTree<Character> e = new BinaryTree<>('e');
    BinaryTree<Character> h = new BinaryTree<>('h');
    BinaryTree<Character> d = new BinaryTree<>('d', c, e);
    BinaryTree<Character> i = new BinaryTree<>('i', h, new BinaryTree<>());
    BinaryTree<Character> b = new BinaryTree<>('b', a, d);
    BinaryTree<Character> g = new BinaryTree<>('g', new BinaryTree<>(), i);
    BinaryTree<Character> f = new BinaryTree<>('f', b, g);

    @Test
    public void levelOrder() throws Exception {
        binaryTreeShower.levelOrderPrint(f);
        binaryTreeShower.levelOrderPrint(new BinaryTree());
    }

    @Test
    public void preOrder() throws Exception {
        binaryTreeShower.preOrderPrint(f);
        binaryTreeShower.preOrderPrint(new BinaryTree());
    }

    @Test
    public void inOrder() throws Exception {
        binaryTreeShower.inOrderPrint(f);
        binaryTreeShower.inOrderPrint(new BinaryTree());
    }

    @Test
    public void postOrder() throws Exception {
        binaryTreeShower.postOrderPrint(f);
        binaryTreeShower.postOrderPrint(new BinaryTree());
    }

}