package main.TP7;

import org.junit.Test;
import struct.impl.BinaryTree;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class BinaryTreeAPITest {
    BinaryTreeAPI binaryTreeAPI = new BinaryTreeAPI();

    @Test
    public void numberOfRepetitions() throws Exception {
        BinaryTree<Integer> leftTree = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree = new BinaryTree<>(4, leftTree, rightTree);
        assertEquals(2, binaryTreeAPI.numberOfRepetitions(mainTree, 4));
        assertEquals(2, binaryTreeAPI.numberOfRepetitions(mainTree, 5));
        assertEquals(0, binaryTreeAPI.numberOfRepetitions(mainTree, 1));
    }

    @Test
    public void height() throws Exception {
        BinaryTree<Integer> leftTree = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree = new BinaryTree<>(4, leftTree, rightTree);
        assertEquals(2, binaryTreeAPI.height(mainTree));
        assertEquals(0, binaryTreeAPI.height(leftTree));
    }

    @Test
    public void weight() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l4 = new BinaryTree<>(4);
        BinaryTree<Integer> r1 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r4 = new BinaryTree<>(8);
        BinaryTree<Integer> r2 = new BinaryTree<>(25,l4,r4);
        BinaryTree<Integer> l2 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);
        assertEquals(9, binaryTreeAPI.weight(b),0.1);

    }

    @Test
    public void leafAmount() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l4 = new BinaryTree<>(4);
        BinaryTree<Integer> r1 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r4 = new BinaryTree<>(8);
        BinaryTree<Integer> r2 = new BinaryTree<>(25,l4,r4);
        BinaryTree<Integer> l2 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);
        assertEquals(5, binaryTreeAPI.leafAmount(b),0.1);
    }

    @Test
    public void elementsInLevel() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l4 = new BinaryTree<>(4);
        BinaryTree<Integer> r1 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r4 = new BinaryTree<>(8);
        BinaryTree<Integer> r2 = new BinaryTree<>(25,l4,r4);
        BinaryTree<Integer> l2 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);
        assertEquals(2, binaryTreeAPI.elementsInLevel(b,1),0.1);
        assertEquals(4, binaryTreeAPI.elementsInLevel(b,3),0.1);
    }

    //Exercise14

    @Test
    public void binaryTreeStable() throws Exception {
        BinaryTree<Integer> b1 = new BinaryTree<>(4);

        assertTrue(binaryTreeAPI.isStable(b1));

        BinaryTree<Integer> leftTree = new BinaryTree<>(2);
        BinaryTree<Integer> rightTree = new BinaryTree<>(3);
        BinaryTree<Integer> mainTree = new BinaryTree<>(4, leftTree, rightTree);

        assertTrue(binaryTreeAPI.isStable(mainTree));

        BinaryTree<Integer> leftTree2 = new BinaryTree<>(4);
        BinaryTree<Integer> rightTree2 = new BinaryTree<>(3);
        BinaryTree<Integer> mainTree2 = new BinaryTree<>(1, leftTree2, rightTree2);

        assertFalse(binaryTreeAPI.isStable(mainTree2));

        BinaryTree<Integer> leftTree3 = new BinaryTree<>(4);
        BinaryTree<Integer> mainTree3 = new BinaryTree<>(1, leftTree3, new BinaryTree<>());
        BinaryTree<Integer> mainTree4 = new BinaryTree<>(1, new BinaryTree<>(), leftTree3);
        BinaryTree<Integer> mainTree5 = new BinaryTree<>(6, new BinaryTree<>(), leftTree3);
        BinaryTree<Integer> mainTree6 = new BinaryTree<>(1, leftTree3, new BinaryTree<>());
        BinaryTree<Integer> mainTree7 = new BinaryTree<>(6, leftTree3, new BinaryTree<>());

        assertFalse(binaryTreeAPI.isStable(mainTree3));
        assertFalse(binaryTreeAPI.isStable(mainTree4));
        assertTrue(binaryTreeAPI.isStable(mainTree5));
        assertFalse(binaryTreeAPI.isStable(mainTree6));
        assertTrue(binaryTreeAPI.isStable(mainTree7));
    }

    @Test
    public void binaryTreeSimilar() throws Exception {
        BinaryTree<Integer> leftTree = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree1 = new BinaryTree<>(4, leftTree, rightTree);

        assertTrue(binaryTreeAPI.similar(mainTree1, mainTree1));

        BinaryTree<Integer> leftTree2 = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree2 = new BinaryTree<>(6, new BinaryTree<>(5), new BinaryTree<>(4));
        BinaryTree<Integer> mainTree2 = new BinaryTree<>(4, rightTree2, leftTree2);

        assertTrue(binaryTreeAPI.similar(mainTree1, mainTree2));

        BinaryTree<Integer> leftTree3 = new BinaryTree<>(7);
        BinaryTree<Integer> rightTree3 = new BinaryTree<>(6, new BinaryTree<>(2),  new BinaryTree<>(1));
        BinaryTree<Integer> mainTree3 = new BinaryTree<>(5, leftTree3, rightTree3);

        assertFalse(binaryTreeAPI.similar(mainTree1, mainTree3));

        BinaryTree<Integer> rightTree4 = new BinaryTree<>(6, new BinaryTree<>(),  new BinaryTree<>(1));
        BinaryTree<Integer> mainTree4 = new BinaryTree<>(5, new BinaryTree<>(), rightTree4);

        assertFalse(binaryTreeAPI.similar(mainTree1, mainTree4));
        assertFalse(binaryTreeAPI.similar(mainTree2, mainTree3));
    }

    @Test
    public void binaryTreeIsomorf() throws Exception {
        BinaryTree<Integer> leftTree = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree1 = new BinaryTree<>(4, leftTree, rightTree);

        assertTrue(binaryTreeAPI.isomorf(mainTree1, mainTree1));

        BinaryTree<Integer> leftTree2 = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree2 = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree2 = new BinaryTree<>(4, leftTree2, rightTree2);

        assertTrue(binaryTreeAPI.isomorf(mainTree1, mainTree2));

        BinaryTree<Integer> leftTree3 = new BinaryTree<>(7);
        BinaryTree<Integer> rightTree3 = new BinaryTree<>(6, new BinaryTree<>(),  new BinaryTree<>());
        BinaryTree<Integer> mainTree3 = new BinaryTree<>(5, leftTree3, rightTree3);

        assertFalse(binaryTreeAPI.isomorf(mainTree1, mainTree3));

        BinaryTree<Integer> b1 = new BinaryTree<>(4);
        BinaryTree<Integer> b2 = new BinaryTree<>(7);

        assertTrue(binaryTreeAPI.isomorf(b1,b2));
        assertFalse(binaryTreeAPI.isomorf(b1,mainTree2));
        assertFalse(binaryTreeAPI.isomorf(mainTree2,b2));


    }

    @Test
    public void binaryTreeEquals() throws Exception {
        BinaryTree<Integer> b1 = new BinaryTree<>(4);
        BinaryTree<Integer> b2 = new BinaryTree<>(4);
        assertTrue(binaryTreeAPI.equals(b1,b2));
        b2 = new BinaryTree<>(5);
        assertFalse(binaryTreeAPI.equals(b1,b2));

        BinaryTree<Integer> leftTree = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree1 = new BinaryTree<>(4, leftTree, rightTree);

        BinaryTree<Integer> leftTree2 = new BinaryTree<>(5);
        BinaryTree<Integer> rightTree2 = new BinaryTree<>(6, new BinaryTree<>(4), new BinaryTree<>(5));
        BinaryTree<Integer> mainTree2 = new BinaryTree<>(4, leftTree2, rightTree2);

        BinaryTree<Integer> leftTree3 = new BinaryTree<>(7);
        BinaryTree<Integer> rightTree3 = new BinaryTree<>(6, new BinaryTree<>(),  new BinaryTree<>(5));
        BinaryTree<Integer> mainTree3 = new BinaryTree<>(5, leftTree3, rightTree3);

        assertTrue(binaryTreeAPI.equals(mainTree1,mainTree2));
        assertFalse(binaryTreeAPI.equals(mainTree1,b1));
        assertFalse(binaryTreeAPI.equals(b1,mainTree1));
        assertFalse(binaryTreeAPI.equals(mainTree1, mainTree3));

    }

    @Test
    public void binaryTreeComplete() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l4 = new BinaryTree<>(4);
        BinaryTree<Integer> r1 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r4 = new BinaryTree<>(8);
        BinaryTree<Integer> r2 = new BinaryTree<>(25,l4,r4);
        BinaryTree<Integer> l2 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);

        assertTrue(binaryTreeAPI.isComplete(b));

        BinaryTree<Integer> r12 = new BinaryTree<>(8);
        BinaryTree<Integer> l12 = new BinaryTree<>(38);
        BinaryTree<Integer> l11 = new BinaryTree<>(32,l12,r12);
        BinaryTree<Integer> r11 = new BinaryTree<>(25,new BinaryTree<>(),r4);
        BinaryTree<Integer> b1 = new BinaryTree<>(94,l11,r11);
        assertFalse(binaryTreeAPI.isComplete(b1));

        assertFalse(binaryTreeAPI.isComplete(new BinaryTree()));
    }

    @Test
    public void binaryTreeIsFull() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(25);
        BinaryTree<Integer> r1 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);

        assertTrue(binaryTreeAPI.isFull(b));

        BinaryTree<Integer> l13 = new BinaryTree<>(3);
        BinaryTree<Integer> l12 = new BinaryTree<>();
        BinaryTree<Integer> r13 = new BinaryTree<>(9);
        BinaryTree<Integer> r12 = new BinaryTree<>();
        BinaryTree<Integer> r11 = new BinaryTree<>(38,l13,r13);
        BinaryTree<Integer> l11 = new BinaryTree<>(32,l12,r12);
        BinaryTree<Integer> b1 = new BinaryTree<>(94,l11,r11);

        assertFalse(binaryTreeAPI.isFull(b1));
    }

    @Test
    public void binaryTreeOccurs() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(25);
        BinaryTree<Integer> r1 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);

        BinaryTree<Integer> l13 = new BinaryTree<>(3);
        BinaryTree<Integer> l12 = new BinaryTree<>(43);
        BinaryTree<Integer> r13 = new BinaryTree<>(9);
        BinaryTree<Integer> r12 = new BinaryTree<>(25);
        BinaryTree<Integer> r11 = new BinaryTree<>(38,l13,r13);
        BinaryTree<Integer> l11 = new BinaryTree<>(32,l12,r12);

        assertTrue(binaryTreeAPI.occurs(b,r11));
        assertFalse(binaryTreeAPI.occurs(b,l11));
        assertTrue(binaryTreeAPI.occurs(b,new BinaryTree<>(3)));
        assertFalse(binaryTreeAPI.occurs(b,new BinaryTree<>(321)));
    }

    @Test
    public void binaryTreeShowFrontier() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(25);
        BinaryTree<Integer> r1 = new BinaryTree<>(38, l3, r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32, l2, r2);
        BinaryTree<Integer> b = new BinaryTree<>(94, l1, r1);

        binaryTreeAPI.showFrontier(b);
    }

    @Test
    public void binaryTreeFrontier() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(25);
        BinaryTree<Integer> r1 = new BinaryTree<>(38, l3, r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32, l2, r2);
        BinaryTree<Integer> b = new BinaryTree<>(94, l1, r1);

        ArrayList<Integer> list = binaryTreeAPI.frontier(b);

        for (Integer i: list) {
            System.out.println(i);
        }
    }

    @Test
    public void binaryTreeSum() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(25);
        BinaryTree<Integer> r1 = new BinaryTree<>(38, l3, r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32, l2, r2);
        BinaryTree<Integer> b = new BinaryTree<>(94, l1, r1);

        assertEquals(209,binaryTreeAPI.sum(b),0.1);
    }

    @Test
    public void binaryTreeThreeMultipleSum() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l2 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r2 = new BinaryTree<>(15);
        BinaryTree<Integer> r1 = new BinaryTree<>(38, l3, r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(30, l2, r2);
        BinaryTree<Integer> b = new BinaryTree<>(94, l1, r1);

        assertEquals(57,binaryTreeAPI.threeMultipleSum(b),0.1);
    }
}