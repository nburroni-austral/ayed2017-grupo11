package main.TP7;

import main.Utils.ObjectUtil.ObjectSaver;
import org.junit.Test;
import struct.impl.BinaryTree;

import static org.junit.Assert.*;

public class ObjectSaverTest {
    ObjectSaver objectSaver = new ObjectSaver();

    @Test
    public void readWriteObject() throws Exception {
        BinaryTree<Integer> l3 = new BinaryTree<>(3);
        BinaryTree<Integer> l4 = new BinaryTree<>(4);
        BinaryTree<Integer> r1 = new BinaryTree<>(8);
        BinaryTree<Integer> r3 = new BinaryTree<>(9);
        BinaryTree<Integer> r4 = new BinaryTree<>(8);
        BinaryTree<Integer> r2 = new BinaryTree<>(25,l4,r4);
        BinaryTree<Integer> l2 = new BinaryTree<>(38,l3,r3);
        BinaryTree<Integer> l1 = new BinaryTree<>(32,l2,r2);
        BinaryTree<Integer> b = new BinaryTree<>(94,l1,r1);

        objectSaver.writeObject(b,"./src/main/TP7/savedTree");
        BinaryTree<Integer> recovered = (BinaryTree<Integer>) objectSaver.readObject("./src/main/TP7/savedTree");

        BinaryTreeAPI binaryTreeAPI = new BinaryTreeAPI();

        assertTrue(binaryTreeAPI.equals(recovered,b));
    }

}