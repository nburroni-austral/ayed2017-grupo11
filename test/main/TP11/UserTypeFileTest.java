package main.TP11;

import main.TP11.DataEntryLogic.Attribute;
import main.TP11.DataEntryLogic.TypeEnum;
import main.TP11.DataEntryLogic.UserType;
import main.TP11.DataEntryLogic.UserTypeFile;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * @author Manuel Pedrozo
 */
public class UserTypeFileTest {
    @Test
    public void writeRegistry() throws Exception {
        ArrayList<Attribute> uArray = new ArrayList<>();
        uArray.add(new Attribute("aaaaaaaaaa aaaaaaaab", 6, TypeEnum.STRING_TYPE));
        uArray.add(new Attribute("Age", -1, TypeEnum.INT_TYPE));
        uArray.add(new Attribute("Surname", 17, TypeEnum.STRING_TYPE));
        uArray.add(new Attribute("RandomDouble", -1, TypeEnum.DOUBLE_TYPE));
        uArray.add(new Attribute("Dni", -1, TypeEnum.INT_TYPE));
        uArray.add(new Attribute("Si", -1, TypeEnum.BOOLEAN_TYPE));

        UserType u1 = new UserType();
        u1.addString("Name", 6, "U1name");
        u1.addString("Surname", 17, "U1sname");
        u1.addDouble("RandomDouble", 420d);
        u1.addInteger("Age", 12);
        u1.addInteger("Dni", 1);
        u1.addBoolean("Si", false);
        u1.setCodePosition(1);

        UserType u2 = new UserType();
        u2.addString("Name", 15, "U2name");
        u2.addString("Surname", 17, "U2sname");
        u2.addDouble("RandomDouble", 320d);
        u2.addInteger("Age", 42);
        u2.addInteger("Dni", 2);
        u2.addBoolean("Si", true);
        u2.setCodePosition(1);

        UserType u3 = new UserType();
        u3.addString("Name", 15, "U3name");
        u3.addString("Surname", 17, "U4sname");
        u3.addDouble("RandomDouble", 4658d);
        u3.addInteger("Age", 31);
        u3.addInteger("Dni", 3);
        u3.addBoolean("Si", false);
        u3.setCodePosition(1);

        UserType u4 = new UserType();
        u4.addString("Name", 15, "U4name");
        u4.addString("Surname", 17, "U4sname");
        u4.addDouble("RandomDouble", 450d);
        u4.addInteger("Age", 82);
        u4.addInteger("Dni", 4);
        u4.addBoolean("Si",true);
        u4.setCodePosition(1);


        UserTypeFile userTypeFile = new UserTypeFile("Test", uArray, 20, 1);
        //UserTypeFile userTypeFile = new UserTypeFile("Test");

        userTypeFile.writeRegistry(u1);
        userTypeFile.writeRegistry(u2);
        userTypeFile.writeRegistry(u3);
        userTypeFile.writeRegistry(u4);


        userTypeFile.delete(1);



        userTypeFile.compact();

        userTypeFile.goToReg(0);
        System.out.println(userTypeFile.readRegistry().toString());


        u3.editDouble(23d,0);
        u3.editString("Edited", 1);
        u3.editInteger(5, 1);
        u3.editInteger(1234123, 0);
        userTypeFile.updateRegistry(u3);
        userTypeFile.goToReg(2);
        System.out.println(userTypeFile.readRegistry().toString());

        System.out.println("\nINDEXING");
        userTypeFile.autoIndex();
        UserType Su2 = userTypeFile.search(2);
        System.out.println(Su2.toString());

        userTypeFile.close();

        userTypeFile = new UserTypeFile("Test");
        System.out.println("\nAFTER RELOAD");
        UserType aux = userTypeFile.search(2);
        System.out.println(aux.toString());

        //System.out.println(userTypeFile.getFilePointer());
    }
}