package main.HorseJumps;

import org.junit.Test;
import struct.impl.DynamicStack;

import java.awt.*;

import static org.junit.Assert.*;

public class CoordinateTranslatorTest {
    @Test
    public void translateCoordinates() throws Exception {
        CoordinateTranslator translator = new CoordinateTranslator();
        DynamicStack<DynamicStack<Point>> pointCoordinateStack = new DynamicStack<>();
        DynamicStack<Point> firstStack = new DynamicStack<>();
        firstStack.push(new Point(1,1));
        firstStack.push(new Point(2,1));
        firstStack.push(new Point(4,2));
        DynamicStack<Point> secondStack = new DynamicStack<>();
        secondStack.push(new Point(5,6));
        secondStack.push(new Point(7,2));
        DynamicStack<Point> thirdStack = new DynamicStack<>();
        thirdStack.push(new Point(1,7));
        thirdStack.push(new Point(4,6));
        thirdStack.push(new Point(2,5));
        thirdStack.push(new Point(0,2));
        DynamicStack<Point> lastStack = new DynamicStack<>();
        lastStack.push(new Point(6,6));
        pointCoordinateStack.push(firstStack);
        pointCoordinateStack.push(secondStack);
        pointCoordinateStack.push(thirdStack);
        pointCoordinateStack.push(lastStack);
        String[][] coordinateStringArray = translator.translateCoordinates(pointCoordinateStack, 4, 8);
        printStringArray(coordinateStringArray);
    }

    public static void printStringArray(String[][] array){
        for(int i = array[0].length-1; i>= 0; i--){
            for(int j = 0; j<array.length; j++){
                if(array[j][i] == null){
                    System.out.print("00" + "\t");
                }
                else System.out.print(array[j][i] + "\t");
            }
            System.out.print("\n");
        }
    }

}