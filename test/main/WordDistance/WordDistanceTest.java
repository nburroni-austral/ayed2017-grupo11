package main.WordDistance;

import org.junit.Test;

import static org.junit.Assert.*;

public class WordDistanceTest {
    @Test
    public void hammingDistance1() throws Exception {
        WordDistance wordDistance = new WordDistance();
        assertEquals(3, wordDistance.hammingDistance("casa", "hola"));
    }

    @Test
    public void levenshteinDistanceTest() throws Exception {
        WordDistance wordDistance = new WordDistance();
        assertEquals(9, wordDistance.levenshteinDistance("murcielago", "dinosaurio"));

    }
}