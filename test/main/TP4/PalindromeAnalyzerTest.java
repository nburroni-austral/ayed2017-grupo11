package main.TP4;

import main.TP4.PalindromeAnalizer.PalindromeAnalyzer;
import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromeAnalyzerTest {
    @Test
    public void isPalindrome() throws Exception {
        PalindromeAnalyzer palindromeAnalyzer = new PalindromeAnalyzer();
        String neuquen = "neuquen";
        String hola = "HoLa";
        String menem = "meNeM";
        String empty = "";
        assertTrue(palindromeAnalyzer.isPalindrome(neuquen));
        assertFalse(palindromeAnalyzer.isPalindrome(hola));
        assertTrue(palindromeAnalyzer.isPalindrome(menem));
        assertTrue(palindromeAnalyzer.isPalindrome(empty));
        System.out.println(neuquen);
        System.out.println(hola);
        System.out.println(menem);
        System.out.println(empty);
    }

}