package main.TP4.PriorityQueue;

import org.junit.Test;

import static org.junit.Assert.*;

public class PriorityQueueTest {
    @Test
    public void priorityQueueTest() throws Exception {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(5);
        priorityQueue.enqueue(1,1);
        priorityQueue.enqueue(2,4);
        priorityQueue.enqueue(3,3);
        priorityQueue.enqueue(4,2);
        priorityQueue.enqueue(5,1);
        priorityQueue.enqueue(6,5);
        priorityQueue.enqueue(7,3);
        priorityQueue.enqueue(8,1);
        priorityQueue.enqueue(9,2);
        priorityQueue.enqueue(10,5);

        assertEquals(1,priorityQueue.dequeue(),0.1);
        assertEquals(5,priorityQueue.dequeue(),0.1);
        assertEquals(8,priorityQueue.dequeue(),0.1);
        assertEquals(4,priorityQueue.dequeue(),0.1);
        assertEquals(9,priorityQueue.dequeue(),0.1);
        assertEquals(3,priorityQueue.dequeue(),0.1);
        assertEquals(7,priorityQueue.dequeue(),0.1);
        assertEquals(2,priorityQueue.dequeue(),0.1);
        assertEquals(6,priorityQueue.dequeue(),0.1);
        assertEquals(10,priorityQueue.dequeue(),0.1);
    }

}