package main.TP4.BankSimulation;

import org.junit.Test;

public class BankSimulationTest {
    BankSingleQ bankSingleQ = new BankSingleQ(3000);
    BankTripleQ bankTripleQ = new BankTripleQ(3000);
    Cashier cashier1 = new Cashier(5, 15);
    Cashier cashier2 = new Cashier(5, 20);
    Cashier cashier3 = new Cashier(5, 25);

    @Test
    public void run() throws Exception {

        ClientFactory clientFactory = new ClientFactory(4000,15,5,20);

        BankSimulation simulationSingleQ = new BankSimulation(bankSingleQ, clientFactory);
        BankSimulation simulationTripleQ = new BankSimulation(bankTripleQ, clientFactory);

        bankSingleQ.addCashier(cashier1);
        bankSingleQ.addCashier(cashier2);
        bankSingleQ.addCashier(cashier3);
        bankTripleQ.addCashier(cashier1);
        bankTripleQ.addCashier(cashier2);
        bankTripleQ.addCashier(cashier3);


        System.out.println("Single Queue");
        simulationSingleQ.run();
        System.out.println("Clients approaching the bank: " + simulationSingleQ.getCreatedClients());
        System.out.println("Clients taken care of: " + simulationSingleQ.getActualClients());
        System.out.println("Finished Time: " + simulationSingleQ.getFinishedTime());
        System.out.println("Time after closing: " + simulationSingleQ.getExtraTime());

        System.out.println("\nTriple Queue");
        simulationTripleQ.run();
        System.out.println("Clients approaching the bank: " + simulationTripleQ.getCreatedClients());
        System.out.println("Clients taken care of: " + simulationTripleQ.getActualClients());
        System.out.println("Finished Time: " + simulationTripleQ.getFinishedTime());
        System.out.println("Time after closing: " + simulationTripleQ.getExtraTime());
    }

    @Test
    public void median() throws Exception{
        int totalClients = 2000;
        int repetitions = 100;
        ClientFactory clientFactory = new ClientFactory(totalClients,15,5,20);
        BankSimulation simulationSingleQ = new BankSimulation(bankSingleQ, clientFactory);
        BankSimulation simulationTripleQ = new BankSimulation(bankTripleQ, clientFactory);

        bankSingleQ.addCashier(cashier1);
        bankSingleQ.addCashier(cashier2);
        bankSingleQ.addCashier(cashier3);
        bankTripleQ.addCashier(cashier1);
        bankTripleQ.addCashier(cashier2);
        bankTripleQ.addCashier(cashier3);

        int totalActualClientsSingleQ = 0;
        int totalExtraTimeSingleQ = 0;
        int totalFinishedTimeSingleQ = 0;

        int totalActualClientsTripleQ = 0;
        int totalExtraTimeTripleQ = 0;
        int totalFinishedTimeTripeQ = 0;

        for(int i = 0; i < repetitions; i++){
            simulationSingleQ.run();
            totalActualClientsSingleQ += simulationSingleQ.getActualClients();
            totalExtraTimeSingleQ += simulationSingleQ.getExtraTime();
            totalFinishedTimeSingleQ += simulationSingleQ.getFinishedTime();

            simulationTripleQ.run();
            totalActualClientsTripleQ += simulationTripleQ.getActualClients();
            totalExtraTimeTripleQ += simulationTripleQ.getExtraTime();
            totalFinishedTimeTripeQ += simulationTripleQ.getFinishedTime();
        }

        double medianActualClientsSQ = (double) totalActualClientsSingleQ / (double) repetitions;
        double medianExtraTimeSQ = (double) totalExtraTimeSingleQ / (double) repetitions;
        double medianFinishedTimeSQ = (double) totalFinishedTimeSingleQ / (double) repetitions;

        double medianActualClientsTQ = (double) totalActualClientsTripleQ / (double) repetitions;
        double medianExtraTimeTQ = (double) totalExtraTimeTripleQ / (double) repetitions;
        double medianFinishedTimeTQ = (double) totalFinishedTimeTripeQ / (double) repetitions;

        System.out.println("Clients approaching the bank: " + totalClients);

        System.out.println("\nSingle Queue");
        System.out.println("Clients taken care of: " + medianActualClientsSQ);
        System.out.println("Finished time: " + medianFinishedTimeSQ);
        System.out.println("Time after closing: " + medianExtraTimeSQ);

        System.out.println("\nTriple Queue");
        System.out.println("Clients taken care of: " + medianActualClientsTQ);
        System.out.println("Finished time: " + medianFinishedTimeTQ);
        System.out.println("Time after closing: " + medianExtraTimeTQ);
    }
}