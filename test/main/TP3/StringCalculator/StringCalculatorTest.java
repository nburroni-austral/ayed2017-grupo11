package main.TP3.StringCalculator;

import main.TP3.StringCalculator.Exceptions.InvalidCharExc;
import main.TP3.StringCalculator.Exceptions.InvalidFormatExc;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringCalculatorTest {
    @Test
    public void calculateTest() throws Exception {
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(5, stringCalculator.calculate("5"), 0.1);
        assertEquals(10, stringCalculator.calculate("5+5"), 0.1);
        assertEquals(0, stringCalculator.calculate("5-5"), 0.1);
        assertEquals(25, stringCalculator.calculate("5*5"), 0.1);
        assertEquals(1, stringCalculator.calculate("5/5"), 0.1);
        assertEquals(14, stringCalculator.calculate("2+3*5-6/2"), 0.1);
        assertEquals(61, stringCalculator.calculate("66-5"), 0.1);
        assertEquals(9, stringCalculator.calculate("6/2*3"), 0.1);
        assertEquals(-26, stringCalculator.calculate("6-8*4+0*3"), 0.1);
        assertEquals(0.4, stringCalculator.calculate("2/5"), 0.1);
        assertEquals(0.83, stringCalculator.calculate("50/60"), 0.01);
    }

    @Test (expected = InvalidCharExc.class)
    public void calculateExc() throws Exception {
        StringCalculator stringCalculator = new StringCalculator();
        stringCalculator.calculate("5]5");
    }

    @Test (expected = InvalidFormatExc.class)
    public void calculateExc2() throws Exception {
        StringCalculator stringCalculator = new StringCalculator();
        stringCalculator.calculate("5+*5");
    }

}