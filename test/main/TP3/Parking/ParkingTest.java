package main.TP3.Parking;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParkingTest {
    @Test
    public void parkCar() throws Exception {
        Car car1 = new Car("1","bmw","blue");
        Car car2 = new Car("2","fiat","white");
        Car car3 = new Car("3","auto","black");
        Car car4 = new Car("k","chevrolet","yellow");
        Parking parking = new Parking();
        parking.parkCar(car1);
        parking.parkCar(car2);
        parking.parkCar(car3);
        parking.parkCar(car4);
        System.out.println(parking.toString());
    }

    @Test
    public void removeCarByCar() throws Exception {
        Car car1 = new Car("1","bmw","blue");
        Car car2 = new Car("2","fiat","white");
        Car car3 = new Car("3","auto","black");
        Car car4 = new Car("k","chevrolet","yellow");
        Parking parking = new Parking();
        parking.parkCar(car1);
        parking.parkCar(car2);
        parking.parkCar(car3);
        parking.parkCar(car4);

        System.out.println(parking.toString());
        parking.removeCar(car1);

        System.out.println(parking.toString());
        parking.removeCar(car4);

        System.out.println(parking.toString());
    }

    @Test
    public void removeCarByPlace() throws Exception {
        Car car1 = new Car("1","bmw","blue");
        Car car2 = new Car("2","fiat","white");
        Car car3 = new Car("3","auto","black");
        Car car4 = new Car("k","chevrolet","yellow");
        Parking parking = new Parking();
        parking.parkCar(car1);
        parking.parkCar(car2);
        parking.parkCar(car3);
        parking.parkCar(car4);

        parking.removeCar(9);

        System.out.println(parking.toString());
        parking.removeCar(4);

        System.out.println(parking.toString());
        parking.removeCar(1);

        System.out.println(parking.toString());
    }

    @Test
    public void getInput() throws Exception {
        Car car1 = new Car("1","bmw","blue");
        Car car2 = new Car("2","fiat","white");
        Car car3 = new Car("3","auto","black");
        Car car4 = new Car("k","chevrolet","yellow");
        Parking parking = new Parking();
        parking.parkCar(car1);
        parking.parkCar(car2);
        parking.parkCar(car3);
        parking.parkCar(car4);

        assertEquals(20,parking.getIncome(),0.01);

        parking.removeCar(3);

        assertEquals(20,parking.getIncome(),0.01);
    }
}