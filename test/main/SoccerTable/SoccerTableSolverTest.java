package main.SoccerTable;

import main.SoccerTable.Exceptions.IncompatibleSoccerTableExc;
import org.junit.Test;
import struct.impl.DynamicStack;
import sun.reflect.annotation.ExceptionProxy;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SoccerTableSolverTest {

    SoccerTableSolver soccerTableSolver = new SoccerTableSolver();

    @Test
    public void possibleResults() throws Exception {
        SoccerTable soccerTable = new SoccerTable(2,1);
        soccerTable.addTeam("Barcelona", 1);
        soccerTable.addTeam("Madrid", 1);
        soccerTable.addMatch("Barcelona", "Madrid");

        DynamicStack<Integer> results = soccerTableSolver.possibleResults(soccerTable, new ArrayList<>(), 0);
        assertEquals(0, results.peek(), 0.1);

        soccerTable.removeTeam("Barcelona");
        soccerTable.removeTeam("Madrid");

        soccerTable.addTeam("Barcelona", 3);
        soccerTable.addTeam("Madrid", 0);

        results = soccerTableSolver.possibleResults(soccerTable, new ArrayList<>(), 0);
        assertEquals(1, results.peek(), 0.1);

        soccerTable.removeTeam("Barcelona");
        soccerTable.removeTeam("Madrid");

        soccerTable.addTeam("Barcelona", 0);
        soccerTable.addTeam("Madrid", 3);

        results = soccerTableSolver.possibleResults(soccerTable, new ArrayList<>(), 0);
        assertEquals(2, results.peek(), 0.1);
    }

    @Test
    public void solve() throws Exception {
        SoccerTable soccerTable1 = new SoccerTable(2,1);
        soccerTable1.addTeam("Barcelona", 1);
        soccerTable1.addTeam("Madrid", 1);
        soccerTable1.addMatch("Madrid", "Barcelona");

        assertEquals("X", soccerTableSolver.solve(soccerTable1));

        SoccerTable soccerTable2 = new SoccerTable(2,1);
        soccerTable2.addTeam("Barcelona", 3);
        soccerTable2.addTeam("Madrid", 0);
        soccerTable2.addMatch("Madrid", "Barcelona");

        assertEquals("2", soccerTableSolver.solve(soccerTable2));

        SoccerTable soccerTable3 = new SoccerTable(4,4);
        soccerTable3.addTeam("Barcelona", 4);
        soccerTable3.addTeam("Madrid", 4);
        soccerTable3.addTeam("Sevilla", 2);
        soccerTable3.addTeam("Getafe", 0);
        soccerTable3.addMatch("Barcelona", "Getafe");
        soccerTable3.addMatch("Madrid", "Sevilla");
        soccerTable3.addMatch("Sevilla", "Barcelona");
        soccerTable3.addMatch("Getafe", "Madrid");

        assertEquals("1XX2", soccerTableSolver.solve(soccerTable3));
    }

    @Test
    public void solve2() throws Exception {
        SoccerTable soccerTable = new SoccerTable(10,18);
        soccerTable.addTeam("Deportivo", 11);
        soccerTable.addTeam("Betis", 9);
        soccerTable.addTeam("Sevilla", 6);
        soccerTable.addTeam("AtlMadrid", 6);
        soccerTable.addTeam("Barcelona", 5);
        soccerTable.addTeam("AthBilbao", 4);
        soccerTable.addTeam("Madrid", 2);
        soccerTable.addTeam("Espanyol", 2);
        soccerTable.addTeam("Valencia", 1);
        soccerTable.addTeam("RealSociedad", 1);
        soccerTable.addMatch("Deportivo", "RealSociedad");
        soccerTable.addMatch("Barcelona", "AtlMadrid");
        soccerTable.addMatch("AthBilbao", "Espanyol");
        soccerTable.addMatch("AtlMadrid", "Madrid");
        soccerTable.addMatch("Deportivo", "Madrid");
        soccerTable.addMatch("Betis", "Deportivo");
        soccerTable.addMatch("RealSociedad", "Espanyol");
        soccerTable.addMatch("Valencia", "Deportivo");
        soccerTable.addMatch("Deportivo", "Barcelona");
        soccerTable.addMatch("Madrid", "Barcelona");
        soccerTable.addMatch("Espanyol", "Sevilla");
        soccerTable.addMatch("Sevilla", "AtlMadrid");
        soccerTable.addMatch("Madrid", "Betis");
        soccerTable.addMatch("Valencia", "AthBilbao");
        soccerTable.addMatch("Betis", "AthBilbao");
        soccerTable.addMatch("Valencia", "AtlMadrid");
        soccerTable.addMatch("RealSociedad", "Betis");
        soccerTable.addMatch("Barcelona", "Betis");

        assertEquals("11X11XXX1X21X2122X", soccerTableSolver.solve(soccerTable));
    }

    @Test (expected = IncompatibleSoccerTableExc.class)
    public void solveExc() throws Exception{
        SoccerTable soccerTable2 = new SoccerTable(2,1);
        soccerTable2.addTeam("Barcelona", 5);
        soccerTable2.addTeam("Madrid", 0);
        soccerTable2.addMatch("Madrid", "Barcelona");

        soccerTableSolver.solve(soccerTable2);
    }
}