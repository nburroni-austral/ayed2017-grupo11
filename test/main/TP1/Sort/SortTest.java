package main.TP1.Sort;
import java.util.Random;

public class SortTest{
    @org.junit.Test
    public void selectionSort() throws Exception {
        Integer[] array = randomIntegerArrayGenerator(10);
        System.out.println("SELECTION SORT");
        System.out.println("Array: " + arrayToString(array));
        Sort.selectionSort(array);
        System.out.println("Sorted Array: " + arrayToString(array));
    }

    @org.junit.Test
    public void selectionSortRecursive() throws Exception {
        Integer[] array = randomIntegerArrayGenerator(10);
        System.out.println("RECURSIVE SELECTION SORT");
        System.out.println("Array: " + arrayToString(array));
        Sort.selectionSortRecursive(array);
        System.out.println("Sorted Array: " + arrayToString(array));
    }

    @org.junit.Test
    public void insertSort() throws Exception {
        Integer[] array = randomIntegerArrayGenerator(10);
        System.out.println("INSERTION SORT");
        System.out.println("Array: " + arrayToString(array));
        Sort.insertSort(array);
        System.out.println("Sorted Array: " + arrayToString(array));
    }

    @org.junit.Test
    public void bubbleSort() throws Exception {
        Integer[] array = randomIntegerArrayGenerator(10);
        System.out.println("BUBBLE SORT");
        System.out.println("Array: " + arrayToString(array));
        Sort.bubbleSort(array);
        System.out.println("Sorted Array: " + arrayToString(array));
    }

    @org.junit.Test
    public void timeComparator() throws Exception{
        Integer[] array = randomIntegerArrayGenerator(1000);
        Integer[] array2 = array.clone();
        Integer[] array3 = array.clone();
        Integer[] array4 = array.clone();
        long bubbleStart = System.nanoTime();
        Sort.bubbleSort(array);
        long bubbleEnd = System.nanoTime();
        long selectionStart = System.nanoTime();
        Sort.selectionSort(array2);
        long selectionEnd = System.nanoTime();
        long selectionRStart = System.nanoTime();
        Sort.selectionSortRecursive(array3);
        long selectionREnd = System.nanoTime();
        long insertionStart = System.nanoTime();
        Sort.insertSort(array4);
        long insertionEnd = System.nanoTime();
        System.out.println("Bubble sort time: " + (float)(bubbleEnd-bubbleStart)/1000000);
        System.out.println("Selection sort time: " + (float)(selectionEnd-selectionStart)/1000000);
        System.out.println("Recursive selection sort time: " + (float)(selectionREnd-selectionRStart)/1000000);
        System.out.println("Insertion sort time: " + (float)(insertionEnd-insertionStart)/1000000);
    }

    private String arrayToString(Integer[] array){
        String string = "[";
        for (int i = 0; i<array.length;i++) {
            string += array[i].toString();
            if(i<array.length-1)
                string += ",";
        }
        return string + "]";
    }

    private Integer[] randomIntegerArrayGenerator(int n){
        int max = 100;//max absolute value.
        Random r = new Random();
        Integer[] array = new Integer[n];
        for (int i = 0 ; i < n ; i++){
            //array[i] = r.nextInt() % max;//positive and negative
            array[i] = Math.abs(r.nextInt() % max);//positive only
        }
        return array;
    }

}