package main.TP1.Merge;

import org.junit.Test;

public class MergeTest {
    @Test
    public void merge() throws Exception {
        Integer[] a = {1, 2, 3, 4};
        Integer[] b = {3, 5, 6};
        System.out.println("MERGE");
        System.out.println("Array 1: " + arrayToString(a));
        System.out.println("Array 2: " + arrayToString(b));
        Comparable[] c = Merge.merge(a,b);
        System.out.println("Merged Array: " + arrayToString(c));

    }

    private String arrayToString(Comparable[] array){
        String string = "[";
        for (int i = 0; i<array.length;i++) {
            string += array[i].toString();
            if(i<array.length-1)
                string += ",";
        }
        return string + "]";
    }

}