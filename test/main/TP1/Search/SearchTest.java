package main.TP1.Search;

public class SearchTest {
    @org.junit.Test
    public void sequentialSearch() throws Exception {
        int[] a = {1,3,7,6,8,9,10};
        System.out.println("SEQUENTIAL SEARCH");
        System.out.println("Array: " + arrayToString(a));
        int k = 5;
        System.out.println("Searched for Number: " + k);
        System.out.println("Result: " + Search.sequentialSearch(k,a));
        k = 9;
        System.out.println("Searched for Number: " + k);
        System.out.println("Result: " + Search.sequentialSearch(k,a));
    }

    @org.junit.Test
    public void binarySearch() throws Exception {
        int[] a = {1,3,7,6,8,9,10};
        System.out.println("BINARY SEARCH");
        System.out.println("Array: " + arrayToString(a));
        int k = 5;
        System.out.println("Searched for Number: " + k);
        System.out.println("Result: " + Search.binarySearch(k,a));
        k = 9;
        System.out.println("Searched for Number: " + k);
        System.out.println("Result: " + Search.binarySearch(k,a));
    }

    private String arrayToString(int[] array){
        String string = "[";
        for (int i = 0; i<array.length;i++) {
            string += array[i];
            if(i<array.length-1)
                string += ",";
        }
        return string + "]";
    }
}