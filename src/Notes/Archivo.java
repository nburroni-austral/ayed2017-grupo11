package Notes;

import java.io.File;

/**
 * @author Tomas Perez Molina
 */
public class Archivo {
    public static void main(String[] args){
        File f = new File("prog");
        System.out.println(f.exists());
        System.out.println(f.isFile());
        System.out.println(f.isDirectory());
        System.out.println(f.length());
    }
}
