package struct.impl.HashTable;
import struct.impl.DynamicList;
import struct.impl.Exceptions.ObjectNotFoundException;
import struct.impl.SearchBinaryTree;
import struct.istruct.Hashable;

import java.io.Serializable;

/**
 * @author Manuel Pedrozo
 */
public class OAHashTable <T extends Hashable & Comparable<T> & Serializable>{
    private DynamicList<T> t[];
    private int capacity;

    public OAHashTable(int m) {
        if (!Prime.isPrime(m))
            m = Prime.nextPrime(m);
        capacity = m;
        t = new DynamicList[m];
        for(int i = 0; i < m ; i++)
            t[i] = new DynamicList<T>();
    }

    public void insert (T x) {
        int k = x.hashCode(capacity);
        t[k].insertNext(x);
    }

    public T search (T x) {
        int k = x.hashCode(capacity);
        for (int i = 0 ; i < t[k].size() ; i ++ )
            t[k].goTo(i);
            if (x.compareTo(t[k].getActual())== 0)
                return t[k].getActual();
        throw new ObjectNotFoundException();
    }

    public boolean exists(T x){
        int k = x.hashCode(capacity);
        for (int i = 0 ; i < t[k].size() ; i ++ ) {
            t[k].goTo(i);
            if (x.equals(t[k].getActual()))
                return true;
        }
        return false;
    }

    public boolean delete(T x) {
        int k = x.hashCode(capacity);
        for (int i = 0; i < t[k].size(); i++) {
            t[k].goTo(i);
            if (x.compareTo(t[k].getActual()) == 0) {
                t[k].remove();
                return true;
            }
        }
        return false;
    }

    public DynamicList<T> getAll(T x){
        int k = x.hashCode(capacity);
        return t[k];
    }

    public SearchBinaryTree<T> getSbt() {
        SearchBinaryTree<T> a = new SearchBinaryTree<T>();
        for (int i = 0; i < capacity; i++ ) {
            if (!t[i].isVoid()) {
                for (int j = 0; j < t[i].size() ; j++) {
                    t[i].goTo(j);
                    a.insert(t[i].getActual());
                }
            }
        }
        return a;
    }
}
