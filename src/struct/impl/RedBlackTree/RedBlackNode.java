package struct.impl.RedBlackTree;

import static struct.impl.RedBlackTree.NodeColor.BLACK;
import static struct.impl.RedBlackTree.NodeColor.RED;

/**
 * @author Tomas Perez Molina
 */



public class RedBlackNode<T extends Comparable<T>>{
    public RedBlackNode<T> father;
    public T data;
    public NodeColor type;
    public RedBlackNode<T> left;
    public RedBlackNode<T> right;

    public int x;
    public int y;

    public RedBlackNode(T data){
        this.data = data;
        this.type = RED;
    }

    public RedBlackNode<T> balance(){
        RedBlackNode<T> root = this;
        balanceCase1();
        while (root.father != null){
            root = root.father;
            root.balanceCase1();
        }
        root.balanceCase1();
        return root;
    }

    public boolean isLeaf(){
        return left == null && right == null;
    }

    private void balanceCase1(){
        if (type == RED) {
            if (father == null) this.type = BLACK;
            else if (father.type == RED) balanceCase4();
        }
    }
    private void balanceCase2(){
        father.type = BLACK;
        RedBlackNode<T> grandfather = getGrandfather();
        if(grandfather != null) grandfather.type = RED;
        if(isLeftNode()){
            rotateRight(this.father);
        }
        else rotateLeft(this.father);
    }
    private void balanceCase4(){
        RedBlackNode<T> uncle = getUncle();
        if(uncle != null && uncle.type == RED){
            RedBlackNode<T> grandfather = getGrandfather();
            uncle.type = BLACK;
            father.type = BLACK;
            grandfather.type = RED;
        }
        else {
            balanceCase3();
        }
    }
    private void balanceCase3(){
        RedBlackNode<T> grandfather = getGrandfather();
        if(grandfather == null) return;
        if(isLeftNode() && !father.isLeftNode()) {
            type = BLACK;
            grandfather.type = RED;
            rotateRight(this);
            rotateLeft(this);
        }
        else if(!isLeftNode() && father.isLeftNode()) {
            type = BLACK;
            grandfather.type = RED;
            rotateLeft(this);
            rotateRight(this);
        }
        else balanceCase2();
    }

    private void rotateRight(RedBlackNode<T> pivot){
        RedBlackNode<T> oldRoot = pivot.father;
        pivot.father = oldRoot.father;
        if(oldRoot.father != null) {
            if (oldRoot.isLeftNode()) {
                oldRoot.father.left = pivot;
            } else {
                oldRoot.father.right = pivot;
            }
        }
        oldRoot.left = pivot.right;
        if(pivot.right != null) pivot.right.father = oldRoot;
        pivot.right = oldRoot;
        oldRoot.father = pivot;
    }
    private void rotateLeft(RedBlackNode<T> pivot){
        RedBlackNode<T> oldRoot = pivot.father;
        pivot.father = oldRoot.father;
        if(oldRoot.father != null) {
            if (oldRoot.isLeftNode()) {
                oldRoot.father.left = pivot;
            } else {
                oldRoot.father.right = pivot;
            }
        }
        oldRoot.right = pivot.left;
        if(pivot.left != null) pivot.left.father = oldRoot;
        pivot.left = oldRoot;
        oldRoot.father = pivot;
    }

    private RedBlackNode<T> getUncle(){
        RedBlackNode<T> grandfather = getGrandfather();
        if(grandfather == null) return null;
        if(this.father == grandfather.left) return grandfather.right;
        else return grandfather.left;
    }

    private RedBlackNode<T> getGrandfather(){
        if(father != null && father.father != null) return father.father;
        else return null;
    }

    private boolean isLeftNode(){
        return father.left == this;
    }
}
