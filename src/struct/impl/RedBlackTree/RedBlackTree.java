package struct.impl.RedBlackTree;

import main.TP4.BankSimulation.Bank;
import struct.impl.Exceptions.ObjectAlreadyInTreeException;
import struct.impl.Tree234.Node;
import struct.impl.Tree234.Node2;

/**
 * @author Tomas Perez Molina
 */
public class RedBlackTree<T extends Comparable<T>>  {
    private RedBlackNode<T> root;

    public RedBlackTree(T rootData) {
        this.root = new RedBlackNode<>(rootData);
        this.root.x = 400;
        this.root.type = NodeColor.BLACK;
    }

    public void insert(T obj){
        RedBlackNode<T> inserted = new RedBlackNode<>(obj);
        insert(inserted, root);
        root = inserted.balance();
    }

    private void insert(RedBlackNode<T> node, RedBlackNode<T> father){
        int compare = father.data.compareTo(node.data);
        if(compare > 0){
            if(father.left != null) insert(node, father.left);
            else{
                father.left = node;
                node.father = father;
            }
        }
        else if(compare < 0){
            if(father.right != null) insert(node, father.right);
            else{
                father.right = node;
                node.father = father;
            }
        }
        else {
            throw new ObjectAlreadyInTreeException();
        }
    }

    public RedBlackNode<T> getRoot(){
        return root;
    }
}
