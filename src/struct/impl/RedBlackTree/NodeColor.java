package struct.impl.RedBlackTree;

/**
 * @author Tomas Perez Molina
 */
public enum NodeColor {
    RED, BLACK
}
