package struct.impl;

import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

/**
 * StaticSortedList is an implementation of a SortedList, a data structure that keeps all of its contents ordered
 * and allows random access to all elements, via a moving window.
 *
 * This implementation is based on an array.
 *
 * @param <T> the data type to be stored
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class StaticSortedList<T extends Comparable<T>> implements SortedList<T>{
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] data;
    private int window;
    private int size;
    private final int capacity;

    public StaticSortedList() {
        this(DEFAULT_CAPACITY);
    }

    public StaticSortedList(int capacity) {
        this.data = new Object[capacity];
        this.capacity = capacity;
        this.window = 0;
        this.size = 0;
    }
    private StaticSortedList(int window, int size, int capacity, Object[] data) {
        this.window = window;
        this.size = size;
        this.capacity = capacity;
        this.data = data;
    }

    /**
     * Inserts the element keeping the list ordered
     * @param obj the element to be inserted
     */

    @Override
    @SuppressWarnings("unchecked")
    public void insert(T obj) {
        if (size == data.length) enlarge();
        int i = 0;
        for (; i < size; i++) {
            if (((T) data[i]).compareTo(obj) > 0) {
                for (int j = size; j > i; j--) {
                    data[j] = data[j - 1];
                }
                break;
            }
        }
        data[i] = obj;
        size++;
    }

    /**
     * @return the current element in the window.
     */

    @Override
    @SuppressWarnings("unchecked")
    public T getActual() {
        if (isVoid()) throw new NullPointerException("The list is empty");
        return (T) data[window];
    }

    /**
     * @return the index of the window.
     */

    @Override
    public int getActualPosition() {
        return window;
    }

    /**
     * Checks if the list is empty
     * @return true if its empty, false if it isn't
     */

    @Override
    public boolean isVoid() {
        return data[0] == null;
    }

    /**
     * Checks if the window is at the end of the list.
     * @return true if the window is at the end of the list, false if it isn't.
     */

    @Override
    public boolean endList() {
        return window == data.length - 1;
    }

    /**
     * @return a clone of the list
     */

    @Override
    public GeneralList<T> clone() {
        Object[] cloned = new Object[data.length];
        for (int i = 0; i < data.length; i++) cloned[i] = data[i];
        return new StaticSortedList<T>(window, size, capacity, cloned);
    }

    /**
     * Moves the window forward.
     */
    @Override
    public void goNext() {
        if (window == size - 1) throw new IndexOutOfBoundsException("Reached the end of the list");
        window++;
    }

    /**
     * Moves the window backwards.
     */

    @Override
    public void goPrev() {
        if (window == 0) throw new IndexOutOfBoundsException("Reached the beginning of the list");
        window--;
    }

    /**
     * Moves the window to the desired index.
     * @param index the index where to move the window.
     */
    @Override
    public void goTo(int index) {
        if (index < 0 || index >= data.length)
            throw new IndexOutOfBoundsException("There is no such index in this list");
        window = index;
    }

    /**
     * Removes the element currently on the window.
     */
    @Override
    public void remove() {
        for (int i = window; i < data.length - 1; i++) {
            data[i] = data[i + 1];
        }
        size--;
        if (window >= size) window = size - 1;
    }

    @Override
    public int size() {
        return size;
    }
    private void enlarge() {
        Object[] tempObjects = new Object[data.length + DEFAULT_CAPACITY];
        for (int i = 0; i < data.length; i++) tempObjects[i] = data[i];
        data = tempObjects;
    }
}
