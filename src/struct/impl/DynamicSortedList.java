package struct.impl;

import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

import java.io.Serializable;

/**
 * DynamicSortedList is an implementation of a SortedList, a data structure that keeps all of its contents ordered
 * and allows random access to all elements, via a moving window.
 *
 * This implementation is based on linked Nodes.
 *
 * @param <T> the data type to be stored
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class DynamicSortedList<T extends Comparable<T>> implements SortedList<T>, Serializable{
    private Node<T> head, window, sentinel;
    private int size;

    public DynamicSortedList(){
        head = new Node<>();
        sentinel = new Node<>();
        head.next = sentinel;
        window = head;
        size = 0;
    }

    /**
     * Inserts the element keeping the list ordered
     * @param obj the element to be inserted
     */

    @Override
    public void insert(T obj) {
        Node<T> newNode = new Node<>(obj);
        window = head;
        while (window.next != sentinel && window.next.obj.compareTo(obj) < 0) {
            window = window.next;
        }
        newNode.next = window.next;
        window.next = newNode;
        size++;
    }

    /**
     * @return the current element in the window.
     */

    @Override
    public T getActual() {
        return window.obj;
    }

    /**
     * @return the index of the window.
     */
    @Override
    public int getActualPosition() {
        int pos = 0;
        if (!isVoid()) {
            Node<T> aux = head;
            for (; aux.next != window; pos++) aux = aux.next;
        }
        return pos;
    }

    /**
     * Checks if the list is empty
     * @return true if its empty, false if it isn't
     */

    @Override
    public boolean isVoid() {
        return head.next == sentinel;
    }

    /**
     * Checks if the window is at the end of the list.
     * @return true if the window is at the end of the list, false if it isn't.
     */

    @Override
    public boolean endList() {
        return window.next == sentinel;
    }

    @Override
    public GeneralList<T> clone() {
        return null;
    }

    /**
     * Moves the window forward.
     */
    @Override
    public void goNext() {
        if(window.next == sentinel) throw new IndexOutOfBoundsException("Reached the end of this List");
        window = window.next;
    }

    /**
     * Moves the window backwards.
     */

    @Override
    public void goPrev() {
        if(window == head.next) throw new IndexOutOfBoundsException("Reached the beginning of this List");
        goBack();
    }

    private void goBack(){
        Node<T> aux = head;
        while(window != aux.next){
            aux = aux.next;
        }
        window = aux;
    }

    /**
     * Moves the window to the desired index.
     * @param index the index where to move the window.
     */
    @Override
    public void goTo(int index) {
        window = head.next;
        for(int i = 0; i < index; i++){
            window = window.next;
        }
    }

    /**
     * Removes the element currently on the window.
     */

    @Override
    public void remove() {
        if(isVoid()) throw new NullPointerException("This List is empty");
        goBack();
        window.next = window.next.next;
        window = window.next;
        if(window == sentinel) goBack();
        size--;
    }

    @Override
    public int size() {
        return size;
    }

    private static class Node<E> implements Serializable{
        E obj;
        Node<E> next;
        Node() {
            obj = null;
            next = null;
        }
        Node(E o) {
            obj = o;
            next = null;
        }
        Node(E o, Node<E> next) {
            this(o);
            this.next = next;
        }
        boolean hasNoObj() {
            return obj == null;
        }
    }
}
