package struct.impl.Tree234;

public abstract class Node <T extends Comparable<T>> {
    private Node<T> father;
    public int type;
    //Interface coordinates
    public int x;
    public int y;

    public abstract Node<T> search(T data);
    public abstract boolean isLeaf();
    public abstract Node<T> insert(T data);
    public abstract void setChild(T data,Node<T> child);
    public Node<T> getFather() {
        return father;
    }
    public abstract Object[] getData();
    public void setFather(Node<T> father) {
        this.father = father;
    }

}
