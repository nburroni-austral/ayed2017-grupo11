package struct.impl.Tree234;

public class Node3<T extends Comparable<T>> extends Node2<T>{
    public T data2;
    private Node<T> center1;

    public Node3() {
        type = 3;
    }

    public Node<T> getCenter1() {
        return center1;
    }

    public void setCenter1(Node<T> center1) {
        if(center1!=null){
            center1.setFather(this);
        }
        this.center1 = center1;
    }

    private Node4<T> convertTo4(T data){
        Node4<T> node4 = new Node4<>();
        node4.setFather(getFather());
        node4.setLeft(getLeft());
        node4.setCenter1(center1);
        node4.setCenter2(center1);
        node4.setRight(getRight());
        if(data.compareTo(data1)<0){
            node4.data3 = data2;
            node4.data2 = data1;
            node4.data1 = data;
        }else{
            if(data.compareTo(data2)<0){
                node4.data1 = data1;
                node4.data2 = data;
                node4.data3 = data2;
            }else{
                node4.data1= data1;
                node4.data2= data2;
                node4.data3= data;
            }
        }
        if(getFather()!=null)
            getFather().setChild(data,node4);
        return node4;
    }

    public Node<T> search(T data) {
        if(this.isLeaf()) return this;
        else{
            if(data.compareTo(data1)<0) return getLeft().search(data);
            else{
                if(data.compareTo(data2)<0) return getCenter1().search(data);
                else{
                    return getRight().search(data);
                }
            }
        }
    }

    public Node<T> insert(T data) {
        return convertTo4(data);
    }

    public void setChild(T data, Node<T> child) {
        if(data.compareTo(data1)<0) setLeft(child);
        else{
            if(data.compareTo(data2)<0) setCenter1(child);
            else{
                setRight(child);
            }
        }
    }
    public Object[] getData() {
        Object[] array = new Object[2];
        array[0] = data1;
        array[1] = data2;
        return array ;
    }
}
