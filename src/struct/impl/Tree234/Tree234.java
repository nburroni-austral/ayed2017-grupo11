package struct.impl.Tree234;

public class Tree234<T extends Comparable<T>> {

    private Node<T> root;

    public Tree234() {
        this.root = new Node2<>();
        this.root.x = 400;
    }

    public Node<T> getRoot() {
        return root;
    }

    public void insert(T data){
        Node<T> node = root.search(data);
        node = node.insert(data);
        Node<T> father = node.getFather();
        if(father!=null){
            while(father.getFather() != null){
                father = father.getFather();
            }
            root = father;
        }
        else{
            root = node;
        }
    }
}
