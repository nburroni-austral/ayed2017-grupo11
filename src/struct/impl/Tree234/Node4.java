package struct.impl.Tree234;

public class Node4<T extends Comparable<T>> extends Node3<T>{
    public T data3;
    private Node center2;

    public Node4() {
        type = 4;
    }

    public Node<T> getCenter2() {
        return center2;
    }

    public void setCenter2(Node center2) {
        if(center2!=null){
            center2.setFather(this);
        }
        this.center2 = center2;
    }

    public Node<T> search(T data) {
        Node node = split();
        return node.search(data);
    }

    public Node<T> insert(T data) {
        setFather(split());
        return getFather().insert(data);
    }

    private Node<T> split() {
        if(getFather()==null){
            setFather(new Node2<>());
        }
        setFather(getFather().insert(data2));

        Node2<T> right = new Node2<>();
        right.data1 = data3;
        right.setFather(getFather());
        right.setLeft(center2);
        right.setRight(getRight());

        Node2<T> left = new Node2<>();
        left.data1 = data1;
        left.setFather(getFather());
        left.setLeft(getLeft());
        left.setRight(getCenter1());

        getFather().setChild(right.data1,right);
        getFather().setChild(left.data1,left);
        return getFather();
    }

    public void setChild(T data, Node<T> child) {
        int comparedWithData1 = data.compareTo(data1);
        int comparedWithData2 = data.compareTo(data2);
        int comparedWithData3 = data.compareTo(data3);
        if(comparedWithData1<0) setLeft(child);
        else{
            if(comparedWithData2<0) setCenter1(child);
            else{
                if(comparedWithData3<0) setCenter2(child);
                else{
                    setRight(child);
                }
            }
        }
    }
    public Object[] getData() {
        Object[] array = new Object[3];
        array[0] = data1;
        array[1] = data2;
        array[2] = data3;
        return array ;
    }
}
