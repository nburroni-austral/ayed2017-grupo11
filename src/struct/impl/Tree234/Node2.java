package struct.impl.Tree234;

public class Node2 <T extends Comparable<T>> extends Node<T> {
    public T data1;
    private Node<T> left;
    private Node<T> right;

    public Node2() {
        type = 2;
    }


    private Node3<T> convertTo3(T data){
        Node3<T> node3 = new Node3<>();
        node3.setFather(this.getFather());
        node3.setLeft(left);
        node3.setRight(right);
        if(data.compareTo(data1)>0){
            node3.data2 = data;
            node3.data1 = data1;
        }else{
            node3.data2 = data1;
            node3.data1 = data;
        }
        if(getFather()!=null)
            getFather().setChild(data, node3);
        return node3;
    }

    public Node<T> search(T data) {
        if(this.isLeaf()) return this;
        else{
            if(data.compareTo(data1)>0){
                return right.search(data);
            }else{
                return left.search(data);
            }
        }
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }

    public Node<T> insert(T data) {
        if(data1==null){
            data1 = data;
            return this;
        }else{
            return convertTo3(data);
        }
    }

    public void setChild(T data, Node<T> child) {
        if(data.compareTo(data1)>0) this.setRight(child);
        else this.setLeft(child);
    }

    public Object[] getData() {
        Object[] array = new Object[1];
        array[0] = data1;
        return array ;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
        if(left!=null){
            left.setFather(this);
        }
    }


    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node<T> right) {
        this.right = right;
        if(right!=null){
            right.setFather(this);
        }
    }

}
