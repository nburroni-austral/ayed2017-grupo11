package main.HorseJumps;

import main.Utils.SwingUtil.ScalingUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

/**
 * JPanel containing a JButton Chess Board, buttons can be marked and all marked buttons will be connected with a line.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class ChessBoardPanel extends JPanel{
    private JButton[][] board;
    private LinkedList<JButton> markedButtons;

    public ChessBoardPanel(ActionListener boardAL) {
        super();
        markedButtons = new LinkedList<>();
        board = new JButton[8][8];
        String leftAndRightCol = " 87654321 ";
        String topAndBottomRow = " ABCDEFGH ";
        setLayout(new GridLayout(10,10));
        double systemScaling = ScalingUtil.getSystemScaling();
        int cellSize = (int) (50* systemScaling);
        int scalingFont = (int) (20* systemScaling);
        Font coordinateFont = new Font("Coordinate", Font.BOLD, scalingFont);
        Dimension cellDimension = new Dimension(cellSize,cellSize);
        for(int col = 0; col < 10; col ++){
            for(int row = 0; row < 10; row++){
                if(col == 0 || col == 9){
                    JLabel currentSquare = new JLabel();
                    currentSquare.setPreferredSize(cellDimension);
                    currentSquare.setHorizontalAlignment(JLabel.CENTER);
                    add(currentSquare);
                    currentSquare.setText(topAndBottomRow.substring(row,row+1));
                    currentSquare.setFont(coordinateFont);
                }
                else if(row == 0 || row == 9){
                    JLabel currentSquare = new JLabel();
                    currentSquare.setPreferredSize(cellDimension);
                    currentSquare.setHorizontalAlignment(JLabel.CENTER);
                    add(currentSquare);
                    currentSquare.setText(leftAndRightCol.substring(col,col+1));
                    currentSquare.setFont(coordinateFont);
                }
                else{
                    JButton currentSquare = new JButton();
                    currentSquare.setPreferredSize(cellDimension);
                    currentSquare.setOpaque(true);
                    add(currentSquare);
                    if((col+row) % 2 != 0) {
                        currentSquare.setBackground(Color.black);
                    }
                    else{
                        currentSquare.setBackground(Color.white);
                    }
                    currentSquare.addActionListener(boardAL);
                    currentSquare.setActionCommand("" + (row-1) + (8-col));
                    board[row-1][8-col] = currentSquare;
                }

            }
        }
    }

    /**
     * Receives the position of a button and logs the position in the grid in order to draw a one-way path connecting all buttons.
     * Meaning that the path cannot turn around and go back the way it came from, thus when backtracking the previous position
     * will be removed.
     *
     * @param coordinate the position of the button in the array.
     */

    public void logMovement(Point coordinate){
        JButton button = board[coordinate.x][coordinate.y];
        if(markedButtons.size() > 1 && markedButtons.lastIndexOf(button) == markedButtons.size()-2){
            JButton prevButton = markedButtons.get(markedButtons.size()-1);
            markedButtons.remove(markedButtons.size()-1);
            if(!markedButtons.contains(prevButton))
                prevButton.setIcon(null);
        }
        else {
            try {
                markedButtons.add(button);
                BufferedImage gameImage = ImageIO.read(new File("./src/main/HorseJumps/Images/red_x.png"));
                Icon redX = new ImageIcon(gameImage);
                button.setIcon(redX);
                button.setDisabledIcon(redX);
            } catch (IOException exc) {
                System.out.println("File not found");
            }
        }
        repaint();
    }

    /**
     * Disables all buttons on the chess board grid.
     */

    public void disableButtons(){
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                board[i][j].setEnabled(false);
            }
        }
    }

    /**
     * Resets the panel, enabling and un-marking the buttons, and clears the marked button list.
     */

    public void reset(){
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                board[i][j].setEnabled(true);
                board[i][j].setIcon(null);
                markedButtons.clear();
            }
        }
    }

    private Point centerOf(JComponent component){
        Rectangle bounds = component.getBounds();
        return new Point(bounds.x + (bounds.width/2), bounds.y + (bounds.height/2));
    }

    /**
     * Paints the panel, with a line joining all marked buttons.
     * @param g Graphics object that paints the panel.
     */

    @Override
    public void paint(Graphics g){
        super.paint(g);
        for(int i = 0; i < markedButtons.size()-1; i++) {
            Point startPoint = centerOf(markedButtons.get(i));
            Point endPoint = centerOf(markedButtons.get(i+1));
            Point middlePoint = new Point(startPoint.x, endPoint.y);
            Graphics2D g2d = (Graphics2D) g.create();
            Line2D line1 = new Line2D.Double(startPoint, middlePoint);
            Line2D line2 = new Line2D.Double(middlePoint, endPoint);
            if(i == markedButtons.size() - 2) g2d.setColor(Color.BLUE); //Current path
            else g2d.setColor(Color.RED);
            g2d.setStroke(new BasicStroke(5));
            g2d.draw(line1);
            g2d.draw(line2);
            g2d.dispose();
        }
    }
}
