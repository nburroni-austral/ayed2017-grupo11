package main.HorseJumps;

import main.HorseJumps.Frames.HorseJumpFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Manages the functionality of HorseJumpFrame.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class HorseJumpController{
    private CoordinateStackPanel coordinatePanel;
    private ChessBoardPanel chessPanel;
    private CoordinateTranslator coordinateTranslator;
    private HorseJumpFrame horseJumpFrame;
    private HorseMovementLogic horseMovementLogic;
    private final int NUM_OF_JUMPS;
    private boolean solutionInProcess;
    private Timer timer;

    public HorseJumpController(int NUM_OF_JUMPS) {
        this.NUM_OF_JUMPS = NUM_OF_JUMPS;
        solutionInProcess = false;
        coordinatePanel = new CoordinateStackPanel(NUM_OF_JUMPS+1,8);
        chessPanel = new ChessBoardPanel(new BoardCoordinateAL());
        coordinateTranslator = new CoordinateTranslator();
        horseJumpFrame = new HorseJumpFrame(chessPanel, coordinatePanel, new NextButtonAL(), new AutoSolveButtonAL(),
                new PauseSolveButtonAL(), new ResetButtonAL());
        horseMovementLogic = new HorseMovementLogic(8);
        timer = new Timer(100, new NextButtonAL());
        timer.setRepeats(true);

        horseJumpFrame.showSelf();
    }

    /**
     * Advances a jump in the HorseMovementLogic object if there is a solution in process
     * and if it is possible to jump.
     */
    private class NextButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!solutionInProcess) horseJumpFrame.showErrorLabel();
            if (solutionInProcess && horseMovementLogic.jump()) {
                chessPanel.logMovement(horseMovementLogic.getCurrentPosition());
                String[][] coordinates = coordinateTranslator.translateCoordinates(horseMovementLogic.getJumpStack(), NUM_OF_JUMPS, 8);
                coordinatePanel.setCoordinatesText(coordinates);
            }
            else{
                if(timer.isRunning()) {
                    reset();
                    timer.stop();
                }
            }
        }
    }

    /**
     * Makes an initialLoad in HorseMovementLogic, sets solutionInProgress to true and updates the user
     * interface.
     */
    private class BoardCoordinateAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            horseJumpFrame.hideErrorLabel();
            Point selectedPoint = coordinateTranslator.parseStringToPoint(e.getActionCommand());
            horseMovementLogic.initialLoad(selectedPoint, NUM_OF_JUMPS);
            chessPanel.logMovement(selectedPoint);
            chessPanel.disableButtons();
            coordinatePanel.setSelectedCoordinate(coordinateTranslator.translatePointToLetterCoordinate(selectedPoint));
            solutionInProcess = true;
        }
    }

    /**
     * Resets the user interface, sets solutionInProcess to false.
     */
    private class ResetButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            reset();
        }
    }

    /**
     * Starts the timer that calls NextButtonAL.
     */
    private class AutoSolveButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if(!solutionInProcess){
                horseJumpFrame.showErrorLabel();
                return;
            }

            if(!timer.isRunning()) {
                timer.stop();
                timer.start();
            }

            else {
                horseJumpFrame.hideErrorLabel();
            }
        }
    }

    /**
     * Pauses the timer if it is running.
     */
    private class PauseSolveButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if(timer.isRunning()) {
                timer.stop();
            }
        }
    }

    private void reset(){
        horseMovementLogic.reset();
        horseJumpFrame.hideErrorLabel();
        chessPanel.reset();
        coordinatePanel.reset();
        solutionInProcess = false;
    }
}
