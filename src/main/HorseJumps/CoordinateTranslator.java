package main.HorseJumps;

import struct.impl.DynamicStack;

import java.awt.*;

/**
 * CoordinateTranslator translates Point coordinates to chess board coordinates
 * and String coordinates to Point.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class CoordinateTranslator {

    /**
     * Translates a Stack of Stack of Points to a two dimensional String array.
     * @param coordinateStack Stack of Stack of Points.
     * @param numOfJumps max number of jumps the horse can do.
     * @param stackMaxSize maximum size of a stack.
     * @return Translated two dimensional String.
     */

    public String[][] translateCoordinates(DynamicStack<DynamicStack<Point>> coordinateStack, int numOfJumps, int stackMaxSize){
        String[][] stringCoordinates = new String[numOfJumps+1][stackMaxSize];
        DynamicStack<DynamicStack<Point>> invertedStack = new DynamicStack<>();
        while(coordinateStack.size() > 0){
            invertedStack.push(coordinateStack.peek());
            coordinateStack.pop();
        }
        int stackNumber = 0;
        while(invertedStack.size() > 0){
            DynamicStack<Point> currentStack = invertedStack.peek();
            invertedStack.pop();
            while(currentStack.size() > 0) {
                stringCoordinates[stackNumber][currentStack.size() - 1] = translatePointToLetterCoordinate(currentStack.peek());
                currentStack.pop();
            }
            stackNumber++;
        }
        return stringCoordinates;
    }

    /**
     * Translates a Point to their corresponding chess board cell.
     * @param point point to be translated.
     * @return Translated String.
     */

    public String translatePointToLetterCoordinate(Point point){
        char x = (char) (point.x + 65);
        int y = point.y + 1;
        return "" + x + y;
    }

    /**
     * Parses a String coordinate to a Point.
     * @param coordinate String coordinate to be parsed.
     * @return Parsed Point.
     */
    public Point parseStringToPoint(String coordinate){
        int x = Integer.parseInt(coordinate.substring(0,1));
        int y = Integer.parseInt(coordinate.substring(1,2));
        return new Point(x, y);
    }
}
