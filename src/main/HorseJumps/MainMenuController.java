package main.HorseJumps;

import main.HorseJumps.Frames.MainMenuFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Manages the functionality of MainMenuFrame.
 */

public class MainMenuController {

    private MainMenuFrame mainMenuFrame;

    public MainMenuController(){
        mainMenuFrame = new MainMenuFrame(new NewGameButtonAL());
        mainMenuFrame.showSelf();
    }

    /**
     * Takes the entered number in MainMenu frame and creates a HorseJumpController with that number,
     * then hides the MainMenuFrame.
     */

    private class NewGameButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int numberOfJumps = mainMenuFrame.getNumberOfJumps();
            if (numberOfJumps > 0) {
                new HorseJumpController(numberOfJumps);
                mainMenuFrame.hideSelf();
            }
        }
    }
}
