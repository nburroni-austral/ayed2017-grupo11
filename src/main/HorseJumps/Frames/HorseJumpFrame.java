package main.HorseJumps.Frames;

import main.HorseJumps.ChessBoardPanel;
import main.HorseJumps.CoordinateStackPanel;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Displays a window with CoordinateStack and ChessBoard panels, "next step", "reset", "auto-solve" and "pause solve" buttons.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class HorseJumpFrame extends JFrame{
    private JLabel errorLabel;

    public HorseJumpFrame(ChessBoardPanel boardPanel, CoordinateStackPanel stackPanel,
                          ActionListener nextAL, ActionListener autoSolveButtonAL,
                          ActionListener pauseSolveButtonAL ,ActionListener resetAL){
        super("Horse Jumps");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(boxUtil.createFiller(50));

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.X_AXIS));
        middleContainer.add(boxUtil.createFiller(50));

        JPanel stackContainer = new JPanel();
        stackContainer.setLayout(new BoxLayout(stackContainer,BoxLayout.Y_AXIS));
        stackContainer.add(stackPanel);
        middleContainer.add(stackContainer);

        middleContainer.add(boxUtil.createFiller(50));
        JPanel boardContainer = new JPanel();
        boardContainer.setLayout(new BoxLayout(boardContainer, BoxLayout.Y_AXIS));
        boardContainer.setAlignmentY(CENTER_ALIGNMENT);
        boardContainer.add(Box.createVerticalGlue());
        boardContainer.add(boardPanel);
        boardContainer.add(Box.createVerticalGlue());

        middleContainer.add(boardContainer);
        middleContainer.add(boxUtil.createFiller(50));

        JPanel buttonContainer = new JPanel();
        buttonContainer.setLayout(new BoxLayout(buttonContainer, BoxLayout.Y_AXIS));

        JPanel errorLabelContainer = new JPanel();
        errorLabelContainer.setLayout(new BoxLayout(errorLabelContainer,BoxLayout.X_AXIS));
        errorLabel = new JLabel("Please click a coordinate");
        errorLabel.setFont(new Font("Error",Font.PLAIN,(int) (15* systemScaling)));
        errorLabelContainer.add(errorLabel);
        hideErrorLabel();

        buttonContainer.add(errorLabelContainer);
        buttonContainer.add(boxUtil.createFiller(25));

        JButton nextButton = boxUtil.createButton("Next step", 200, 50);
        nextButton.addActionListener(nextAL);
        buttonContainer.add(nextButton);
        buttonContainer.add(boxUtil.createFiller(50));
        JButton autoSolveButton = boxUtil.createButton("Auto solve", 200, 50);
        autoSolveButton.addActionListener(autoSolveButtonAL);
        buttonContainer.add(autoSolveButton);
        buttonContainer.add(boxUtil.createFiller(25));
        JButton pauseSolveButton = boxUtil.createButton("Pause solve", 200, 50);
        pauseSolveButton.addActionListener(pauseSolveButtonAL);
        buttonContainer.add(pauseSolveButton);
        buttonContainer.add(boxUtil.createFiller(50));
        JButton resetButton = boxUtil.createButton("Reset", 200, 50);
        resetButton.addActionListener(resetAL);
        buttonContainer.add(resetButton);

        middleContainer.add(buttonContainer);
        middleContainer.add(boxUtil.createFiller(50));

        add(middleContainer);
        add(boxUtil.createFiller(50));

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }

    /**
     * Shows the window
     */

    public void showSelf(){
        setVisible(true);
    }

    /**
     * Hides the window
     */

    public void hideSelf(){
        setVisible(false);
    }

    /**
     * Shows the error label.
     */

    public void showErrorLabel(){
        errorLabel.setForeground(Color.RED);
    }

    /**
     * Hides the error label.
     */

    public void hideErrorLabel(){
        errorLabel.setForeground(getBackground());
    }

}
