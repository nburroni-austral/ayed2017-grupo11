package main.HorseJumps.Frames;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * Displays a window containing a Textfield where numbers from 1 to 9 can be entered to indicate the number of jumps,
 * and a "New Game" button.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class MainMenuFrame extends JFrame{
    private JFormattedTextField numberOfJumpsInput;

    public MainMenuFrame(ActionListener newGameButtonAL) throws HeadlessException {
        super("Horse Jump Menu");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
        add(boxUtil.createFiller(50));

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.Y_AXIS));
        middleContainer.add(boxUtil.createFiller(50));

        JPanel jumpNumberPanel = new JPanel();
        jumpNumberPanel.setLayout(new BoxLayout(jumpNumberPanel, BoxLayout.X_AXIS));
        jumpNumberPanel.add(boxUtil.createFiller(50));

        JLabel enterJumps = new JLabel("Enter number of jumps (1 to 9)");
        int fontSize = (int) (20* systemScaling);
        Font font = new Font("jumps", Font.PLAIN, fontSize);
        enterJumps.setFont(font);
        jumpNumberPanel.add(enterJumps);

        jumpNumberPanel.add(boxUtil.createFiller(50));

        NumberFormatter nf = new NumberFormatter();
        nf.setFormat(NumberFormat.getInstance());
        nf.setMinimum(1);
        nf.setMaximum(9);
        numberOfJumpsInput = new JFormattedTextField(nf);
        int dimensionSize = (int) (40* systemScaling);
        Dimension textSize = new Dimension(dimensionSize, dimensionSize);

        numberOfJumpsInput.setMinimumSize(textSize);
        numberOfJumpsInput.setMaximumSize(textSize);
        numberOfJumpsInput.setPreferredSize(textSize);
        numberOfJumpsInput.setFont(font);
        numberOfJumpsInput.setHorizontalAlignment(JTextField.CENTER);

        jumpNumberPanel.add(numberOfJumpsInput);

        jumpNumberPanel.add(boxUtil.createFiller(50));

        middleContainer.add(jumpNumberPanel);

        middleContainer.add(boxUtil.createFiller(50));

        JButton newGameButton = boxUtil.createButton("New Game", 150, 50);
        newGameButton.addActionListener(newGameButtonAL);

        middleContainer.add(newGameButton);

        middleContainer.add(boxUtil.createFiller(50));

        add(middleContainer);

        add(boxUtil.createFiller(50));

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        pack();
    }

    /**
     * Hides the window.
     */

    public void hideSelf(){
        setVisible(false);
    }

    /**
     * Shows the window
     */

    public void showSelf(){
        setVisible(true);
    }

    /**
     * @return the number entered in the text field.
     */

    public int getNumberOfJumps(){
        requestFocusInWindow();
        if(!numberOfJumpsInput.getText().equals(""))
            return Integer.parseInt(numberOfJumpsInput.getText());
        return 0;
    }
}
