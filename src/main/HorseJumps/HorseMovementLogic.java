package main.HorseJumps;

import struct.impl.DynamicStack;
import java.awt.*;

/**
 * HorseMovementLogic has the logic to calculate every jump a chess horse is
 * able to do given a start position and an amount of jumps.
 * Keeps track of the current horse position.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class HorseMovementLogic {
    private final int MAP_SIZE;
    private int numberOfJumps;
    private DynamicStack<DynamicStack<Point>> jumpStack = new DynamicStack<>();
    private Point currentPosition;
    private Point prevPosition;
    private boolean backtrack;

    public HorseMovementLogic(int MAP_SIZE) {
        this.MAP_SIZE = MAP_SIZE;
    }

    /**
     * Gets everything ready to start making jumps.
     *
     * @param initialPosition the initial horse position.
     * @param numberOfJumps the max amount of jumps.
     */

    public void initialLoad(Point initialPosition, int numberOfJumps){
        backtrack = false;
        prevPosition = new Point(-1,-1);
        currentPosition = initialPosition;
        DynamicStack<Point> initialStack = new DynamicStack<>();
        initialStack.push(initialPosition);
        jumpStack.push(initialStack);
        this.numberOfJumps = numberOfJumps;
    }

    /**
     * Makes a jump if possible while managing the currentPosition and
     * prevPosition values.
     *
     * @return true if a jump was made, false if not (in which case its not possible to keep jumping).
     */

    public boolean jump(){
        if(backtrack || currentPosition.equals(prevPosition)){
            if(!currentPosition.equals(prevPosition)) {
                jumpStack.peek().pop();
                currentPosition = new Point(prevPosition.x,prevPosition.y);
            }
            else{
                if(jumpStack.peek().isEmpty()){
                    jumpStack.pop();
                    if(jumpStack.size() == 1 && jumpStack.peek().size() == 1){
                        return false;
                    }
                    restorePreviousPosition();
                    jumpStack.peek().pop();
                    currentPosition = new Point(prevPosition.x,prevPosition.y);
                    backtrack = false;
                }
                else{
                    currentPosition = jumpStack.peek().peek();
                }
            }
        }
        else{
            DynamicStack<Point> possiblePositions = getPossiblePositions(currentPosition.x,currentPosition.y,prevPosition.x,prevPosition.y);
            jumpStack.push(possiblePositions);
            prevPosition = new Point(currentPosition.x,currentPosition.y);
            currentPosition = possiblePositions.peek();
            if(jumpStack.size() == numberOfJumps+1)
                backtrack = true;
        }
        return true;
    }

    private DynamicStack<Point> getPossiblePositions(int row, int col, int prevRow, int prevCol){
        DynamicStack<Point> possiblePositions = new DynamicStack<>();
        for(int increment = -2; increment <= 2; increment+=4){
            if (row + increment < MAP_SIZE && row + increment >= 0) {
                if (col + 1 < MAP_SIZE) {
                    Point nextPoint = new Point(row + increment, col+1);
                    if(!(nextPoint.x == prevRow && nextPoint.y == prevCol)) {
                        possiblePositions.push(nextPoint);
                    }
                }
                if (col - 1 < MAP_SIZE && col - 1 >= 0) {
                    Point nextPoint = new Point(row + increment, col-1);
                    if(!(nextPoint.x == prevRow && nextPoint.y == prevCol)) {
                        possiblePositions.push(nextPoint);
                    }
                }
            }
            if(col + increment < MAP_SIZE && col + increment >= 0){
                if (row + 1 < MAP_SIZE) {
                    Point nextPoint = new Point(row+1, col + increment);
                    if(!(nextPoint.x == prevRow && nextPoint.y == prevCol)) {
                        possiblePositions.push(nextPoint);
                    }
                }
                if (row - 1 < MAP_SIZE && row - 1 >= 0) {
                    Point nextPoint = new Point(row-1, col + increment);
                    if(!(nextPoint.x == prevRow && nextPoint.y == prevCol)) {
                        possiblePositions.push(nextPoint);
                    }
                }
            }
        }
        return possiblePositions;
    }

    private void restorePreviousPosition(){
        if(jumpStack.size() == 1)
            prevPosition = jumpStack.peek().peek();
        else {
            DynamicStack<Point> currentStack = jumpStack.peek();
            jumpStack.pop();
            prevPosition = jumpStack.peek().peek();
            jumpStack.push(currentStack);
        }
    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    /**
     * Resets the logic as if an initialLoad was never done.
     */

    public void reset(){
        jumpStack.empty();
        numberOfJumps = 0;
        currentPosition = null;
        prevPosition = null;
        backtrack = false;
    }

    /**
     * @return a clone of the jumpStack.
     */

    public DynamicStack<DynamicStack<Point>> getJumpStack(){
        DynamicStack<DynamicStack<Point>> inverted = new DynamicStack<>();
        DynamicStack<DynamicStack<Point>> clone = new DynamicStack<>();
        while (!jumpStack.isEmpty()) {
            inverted.push(new DynamicStack<>());
            while (!jumpStack.peek().isEmpty()) {
                inverted.peek().push(jumpStack.peek().peek());
                jumpStack.peek().pop();
            }
            jumpStack.pop();
        }
        while (!inverted.isEmpty()) {
            jumpStack.push(new DynamicStack<>());
            clone.push(new DynamicStack<>());
            while (!inverted.peek().isEmpty()) {
                jumpStack.peek().push(inverted.peek().peek());
                clone.peek().push((Point) inverted.peek().peek().clone());
                inverted.peek().pop();
            }
            inverted.pop();
        }
        return clone;
    }

}
