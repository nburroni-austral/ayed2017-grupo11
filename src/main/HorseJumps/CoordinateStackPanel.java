package main.HorseJumps;

import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * JPanel containing columns with coordinates
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class CoordinateStackPanel extends JPanel {
    private JLabel[][] coordinates;

    public CoordinateStackPanel(int numOfStacks, int stackSize){
        super();
        coordinates = new JLabel[numOfStacks][stackSize];
        double systemScaling = ScalingUtil.getSystemScaling();
        int borderSize = (int) (5* systemScaling);
        setLayout(new GridLayout(1,numOfStacks, borderSize, 0));
        setBorder(new EmptyBorder(borderSize, borderSize, borderSize, borderSize));
        setBackground(Color.black);
        Font scaledFont = new Font("Font", Font.BOLD, (int) (20* systemScaling));
        int coordinateXSize = (int) (50* systemScaling);
        int coordinateYSize = (int) (30* systemScaling);
        Dimension coordinateDim = new Dimension(coordinateXSize,coordinateYSize);
        for(int stack = 0; stack < numOfStacks; stack++){
            JPanel currentStack = new JPanel(new GridLayout(stackSize, 1));
            currentStack.setBorder(new EmptyBorder(borderSize, borderSize,borderSize,borderSize));
            for(int level = 1; level <= stackSize; level++){
                JLabel stackLevel = new JLabel();
                stackLevel.setPreferredSize(coordinateDim);
                stackLevel.setMinimumSize(coordinateDim);
                stackLevel.setFont(scaledFont);
                stackLevel.setHorizontalAlignment(JLabel.CENTER);
                coordinates[stack][stackSize - level] = stackLevel;
                currentStack.add(stackLevel);
            }
            add(currentStack);
        }
    }

    /**
     * Resets the coordinate columns
     */

    public void reset(){
        for(int i = 0; i<coordinates.length; i++){
            for(int j = 0; j<coordinates[i].length; j++){
                coordinates[i][j].setText("");
            }
        }
    }

    /**
     * Receives a String[][] and displays it in the coordinate columns
     * @param coordinatesText array to be displayed
     */

    public void setCoordinatesText(String[][] coordinatesText){
        for(int i = 0; i<coordinatesText.length; i++){
            for(int j = 0; j<coordinatesText[i].length; j++){
                coordinates[i][j].setText(coordinatesText[i][j]);
            }
        }
    }

    /**
     * Adds the initial coordinate to display
     * @param coordinate
     */

    public void setSelectedCoordinate(String coordinate){
        coordinates[0][0].setText(coordinate);
    }
}
