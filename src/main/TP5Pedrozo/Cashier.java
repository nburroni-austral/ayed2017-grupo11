package main.TP5Pedrozo;


import struct.impl.DynamicQueue;

import java.util.Random;

/**
 * Represents a cashier that takes care of its own Client queue.
 * Keeps track of the amount of served clients, spare time and founds.
 * @author Manuel Pedrozo
 */
public class Cashier {

    private int clientsServed, clientTimeWaited;
    private int spareTime;
    private static double ticketCost = 0.70;
    private double founds;
    private DynamicQueue<Client> clients;

    public Cashier(){
        spareTime = 0;
        clients = new DynamicQueue<>();
    }

    /**
     * Enqueues the received client to the client queue.
     * @param cycle current cycle.
     * @param client client to be added to the queue.
     */
    public void receiveClient(int cycle, Client client){
        clients.enqueue(client);
        client.enqueued(cycle);
    }

    /**
     * Advances the system to the received cycle.
     * @param cycle current cycle.
     */
    public void next(int cycle){
        Random random = new Random();
        if(clients.size() == 0){
            spareTime++;
        }
        else if(random.nextFloat() <= 0.3){
            clientTimeWaited += clients.dequeue().dequeued(cycle);
            clientsServed++;
            founds += ticketCost;
        }
    }

    /**
     * Serves every client in the queue.
     * @param cycle current cycle.
     */
    public void empty(int cycle){
        while(clients.size() != 0) {
            clientTimeWaited += clients.dequeue().dequeued(cycle);
            clientsServed++;
            founds += ticketCost;
        }
    }

    /**
     * @return the average waiting time of the clients in the queue.
     */
    public double getAverageWaitingTime(){
        return clientTimeWaited/clientsServed;
    }

    public double getFounds(){
        return founds;
    }

    public int getSpareTime(){
        return spareTime;
    }
}