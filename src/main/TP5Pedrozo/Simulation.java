package main.TP5Pedrozo;

import main.Utils.Scanner.ScannerUtil;
import struct.impl.DynamicList;

/**
 * Simulation is in charge of running the entire simulation.
 * @author Manuel Pedrozo
 */
public class Simulation {

    private static Metro metro;
    private static int cycleSecondValue;

    public static void main(String[] args){

        ScannerUtil s = new ScannerUtil();
        cycleSecondValue = 10;

        System.out.println("Insert amount of cashiers");
        int cashierAmount = s.getInt();
        metro = new Metro(cashierAmount,5760);

        for(int i = 1;i <= 5760;i++){
            metro.receiveClients(i,makeClients());
            metro.next(i);
        }

        printReport();

    }

    private static Client[] makeClients(){
        Client[] clients = new Client[5];
        for (int i = 0; i < clients.length; i++){
            clients[i] = new Client();
        }
        return clients;
    }

    private static void printReport(){
        DynamicList<Cashier> cashiers = metro.getCashiers();
        for(int j = 0; j < cashiers.size();j++){
            cashiers.goTo(j);
            System.out.println("\n");
            System.out.println("Cashier: " + (j+1));
            System.out.println("Client average waiting time: " + cashiers.getActual().getAverageWaitingTime()*cycleSecondValue + "s");
            System.out.println("Total founds: $" + cashiers.getActual().getFounds());
            System.out.println("Total spare time: " + cashiers.getActual().getSpareTime()*cycleSecondValue + "s");
        }
    }

}