package main.TP5Pedrozo;

/**
 * Represents a client that keeps track of his enqueued time.
 * @author Manuel Pedrozo
 */
public class Client {

    private int enqueuedTime;

    public Client(){
        enqueuedTime = -1;
    }

    /**
     * Sets the enqueuedTime to the received cycle.
     * @param cycle current cycle.
     */
    public void enqueued(int cycle){
        enqueuedTime = cycle;
    }

    /**
     * Calculates the waiting time.
     * @param cycle current cycle.
     * @return waiting time in the queue.
     */
    public int dequeued(int cycle){
        return enqueuedTime = cycle - enqueuedTime;
    }

}
