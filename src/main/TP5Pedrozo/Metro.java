package main.TP5Pedrozo;

import struct.impl.DynamicList;

import java.util.Random;

/**
 * Represents a metro cashier system simulation.
 * @author Manuel Pedrozo
 */
public class Metro {
    private int openTime;
    private DynamicList<Cashier> cashiers;

    public Metro(int cashierAmount, int openTime){
        this.openTime = openTime;
        cashiers = new DynamicList<>();
        while(cashierAmount-1 >= 0){
            cashiers.insertNext(new Cashier());
            cashierAmount--;
        }
    }

    /**
     * Advances the simulation to the received cycle.
     * @param cycle current cycle.
     */
    public void next(int cycle){
        if(openTime - cycle <= 3){
            for (int i = 0; i < cashiers.size(); i++) {
                cashiers.goTo(i);
                cashiers.getActual().empty(cycle);
            }
        }
        else {
            for (int i = 0; i < cashiers.size(); i++) {
                cashiers.goTo(i);
                cashiers.getActual().next(cycle);
            }
        }
    }

    /**
     * Assigns each of the received clients to a random cashier.
     * @param cycle current cycle.
     * @param clients clients to be assigned.
     */
    public void receiveClients(int cycle, Client[] clients){
        Random random = new Random();
        for(int i = 0; i<clients.length; i++){
            cashiers.goTo(random.nextInt(cashiers.size()));
            cashiers.getActual().receiveClient(cycle, clients[i]);
        }
    }

    public DynamicList<Cashier> getCashiers() {
        return cashiers;
    }
}