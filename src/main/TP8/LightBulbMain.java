package main.TP8;

import main.Utils.Scanner.ScannerUtil;
import struct.impl.Exceptions.ObjectAlreadyInTreeException;
import struct.impl.Exceptions.ObjectNotFoundException;

import java.util.LinkedList;

public class LightBulbMain {

    static LightBulbStockManager lb;
    public static void main(String[] args){
        ScannerUtil s = new ScannerUtil();
        lb = new LightBulbStockManager(makeList());
        int in = -1;
        while (in != 0){
            printMenu();
            in = s.getInt();
            switch (in) {
                case 1:
                    newLightBulb(s);
                    break;
                case 2:
                    deleteLightBulb(s);
                    break;
                case 3:
                    updateStock(s);
                    break;
                case 4:
                    printReport();
                    break;
            }

        }
    }

    private static LinkedList<LightBulb> makeList(){
        LinkedList<LightBulb> list = new LinkedList<>();
        list.push(new LightBulb("98541","type1",250,593));
        list.push(new LightBulb("51251","type2",500,1545));
        list.push(new LightBulb("31612","type3",750,3134));
        list.push(new LightBulb("61847","type4",1500,1384));
        list.push(new LightBulb("15468","type5",1250,2942));
        list.push(new LightBulb("65454","type6",1000,62));
        list.push(new LightBulb("24687","type7",750,4847));
        list.push(new LightBulb("58742","type8",500,468));
        return list;
    }

    private static void printMenu(){
        System.out.println("1) New light bulb");
        System.out.println("2) Delete light bulb");
        System.out.println("3) Update light bulb stock");
        System.out.println("4) Report");
        System.out.println("0) Exit");
        System.out.println("\n");
    }

    private static void deleteLightBulb(ScannerUtil s){
        System.out.println("Code:");
        String code = s.getString();
        if(code.equals("-1"))
            return;
        try{
            lb.delete(code);
            System.out.println("Light bulb deleted");
        }
        catch (ObjectNotFoundException e){
            System.out.println("That light bulb does not exist");
        }
        System.out.println("\n");
    }

    private static void updateStock(ScannerUtil s){
        System.out.println("Code:");
        String code = s.getString();
        if(code.equals("-1"))
            return;
        System.out.println("Stock:");
        int stock = s.getInt();
        if(stock < 0){
            return;
        }
        try{
            lb.updateStock(code,stock);
            System.out.println("Stock updated");
        }
        catch (ObjectNotFoundException e){
            System.out.println("Light bulb does not exist");
        }
        System.out.println("\n");

    }

    private static void newLightBulb(ScannerUtil s){
        System.out.println("Light bulb code:");
        String code = s.getString();
        System.out.println("Light bulb type:");
        String type = s.getString();
        System.out.println("Light bulb watts:");
        int watts = s.getInt();
        System.out.println("Light bulb amount:");
        int amount = s.getInt();
        System.out.println("\n");
        try{
            lb.add(new LightBulb(code,type,watts,amount));
            System.out.println("Light bulb added");
        }
        catch (StringTooLongException e){
            System.out.println(e.getMessage());
        }
        catch (ObjectAlreadyInTreeException e){
            System.out.println("Light bulb already exists");
        }
        System.out.println("\n");
    }

    private static void printReport(){
        System.out.println("REPORT");
        System.out.println("\n");
        System.out.println(lb.getReport());
        System.out.println("\n");
        System.out.println("\n");
    }
}
