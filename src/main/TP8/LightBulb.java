package main.TP8;

import java.io.Serializable;

/**
 * LightBulb represents a light bulb and its stock with a code, a type, a watts amount
 * and the amount of that light bulb in stock.
 */
public class LightBulb implements Comparable<LightBulb>, Serializable{
    private String code,type;
    private int watts,amount;

    public LightBulb(String code, String type, int watts, int amount) {
        if(code.length() > 5)
            throw new StringTooLongException("Code",5);
        if(type.length() > 10)
            throw new StringTooLongException("Type",10);
        this.code = code;
        this.type = type;
        this.watts = watts;
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public int getWatts() {
        return watts;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * Precondition: stock > 0
     * @param stock new stock.
     */
    public void updateStock(int stock){
        if(stock >= 0)
            this.amount = stock;
    }

    public String toString(){
        return "Code: " + code + "\nType: " + type + "\nWatts: " + watts + "\nAmount: " + amount + "\n\n";
    }

    /**
     * Compares by code.
     */
    @Override
    public int compareTo(LightBulb lb) {
        return this.code.compareTo(lb.getCode());
    }
}
