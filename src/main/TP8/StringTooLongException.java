package main.TP8;

public class StringTooLongException extends RuntimeException{
    public StringTooLongException(String s, int max){
        super(s + " too long (max: " + max + ")");
    }
}
