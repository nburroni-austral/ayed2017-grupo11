package main.TP8;

import main.TP7.BinaryTreeShower;
import struct.impl.SearchBinaryTree;

import java.util.LinkedList;

/**
 * LightBulbStockManager manages the light bulbs stock by having them stored in a SearchBinaryTree.
 * Is also able to generate a system report.
 */
public class LightBulbStockManager {
    private SearchBinaryTree<LightBulb> lightBulbST;

    public LightBulbStockManager(){
        lightBulbST = new SearchBinaryTree<>();
    }

    public LightBulbStockManager(SearchBinaryTree<LightBulb> sbt){
        lightBulbST = sbt;
    }

    public LightBulbStockManager(LinkedList<LightBulb> list){
        SearchBinaryTreeApi sbtapi = new SearchBinaryTreeApi();
        lightBulbST = sbtapi.migrateFromList(list);
    }

    /**
     * Adds a LightBulb to the system.
     * @param lb LightBulb to be added.
     */
    public void add(LightBulb lb){
        lightBulbST.insert(lb);
    }

    /**
     * Deletes a LightBulb from the system.
     * @param code code of the LightBulb to be deleted.
     */
    public void delete(String code){
        lightBulbST.delete(new LightBulb(code,"",0,0));
    }

    /**
     * Updates the stock of the LightBulb with the received code.
     * @param code code of the LightBulb to be updated.
     * @param stock new stock.
     */
    public void updateStock(String code, int stock){
        lightBulbST.search(new LightBulb(code,"",0,0)).updateStock(stock);
    }

    /**
     * Generates a String that is a light bulb stock report.
     * @return report String.
     */
    public String getReport(){
        BinaryTreeShower bts = new BinaryTreeShower();
        return bts.inOrderString(lightBulbST);
    }
}
