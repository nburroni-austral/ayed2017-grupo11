package main.TP8;

import struct.impl.SearchBinaryTree;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * SearchBinaryTreeApi is an utility class for SearchBinaryTree.
 */
public class SearchBinaryTreeApi {

    /**
     * Puts every object in the received LinkedList and adds it to a new SearchBinaryTree.
     * @param list LinkedList to be migrated from.
     * @param <T> type of the objects in the LinkedList.
     * @return SearchBinaryTree filled with the objects from list.
     */
    public<T extends Serializable & Comparable<T>> SearchBinaryTree<T> migrateFromList(LinkedList<T> list){
        SearchBinaryTree<T> sbt = new SearchBinaryTree();
        while(!list.isEmpty())
            sbt.insert(list.pop());
        return sbt;
    }
}
