package main.WordDistance;

public class WordDistance {

    /**
     * Calculates the Hamming distance between two words.
     *
     * @param word1 source word.
     * @param word2 target word.
     * @return Hamming distance between the words.
     */
    public int hammingDistance(String word1, String word2){
        if(word1.length() != word2.length()) throw new RuntimeException();
        int result = 0;
        for(int i = 0; i < word1.length() && i < word2.length(); i++){
            if(word1.charAt(i) != word2.charAt(i)){
                result++;
            }
        }
        return result;
    }

    /**
     * Calculates the minimum of the received integers.
     *
     * @param a
     * @param b
     * @param c
     * @return the minimum integer.
     */
    private int minimum(int a, int b, int c){
        int minimum = a;
        if(b < minimum)
            minimum = b;
        if(c < minimum)
            minimum = c;
        return minimum;
    }

    /**
     * Calculates the Levenshtein distance between the words.
     *
     * @param word1 source word.
     * @param word2 target word.
     * @return Levenshtein distance between the words.
     */
    public int levenshteinDistance(String word1, String word2){
        int l1 = word1.length();
        int l2 = word2.length();

        if(l1 == 0)
            return l2;
        if(l2 == 0)
            return l1;

        int matrix[][] = new int[l1+1][l2 +1];

        for(int i = 0; i <= l1; i++)
            matrix[i][0] = i;

        for(int j = 0; j <= l2; j++)
            matrix[0][j] = j;

        char ichar;
        char jchar;
        int cost;

        for(int i = 1;i <= l1;i++) {
            ichar = word1.charAt(i - 1);

            for(int j = 1; j <= l2; j++){
                jchar = word2.charAt(j - 1);

                if(ichar == jchar)
                    cost = 0;
                else
                    cost = 1;

                matrix[i][j] = minimum(
                        matrix[i - 1][j]+1,
                        matrix[i][j - 1]+1,
                        matrix[i-1][j-1] + cost);
            }
        }

        return matrix[l1][l2];
    }


}
