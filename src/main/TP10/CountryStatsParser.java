package main.TP10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * CountryStatsParser takes a file containing country statistics, separated by commas and can separate them by population size.
 *
 * @author Tomas Perez Molina
 */

public class CountryStatsParser {
    private BufferedReader br;
    private String filename;

    public CountryStatsParser(String filename)  throws IOException{
        br = new BufferedReader(new FileReader(filename));
        this.filename = filename;
    }

    /**
     * Separates the file in two, one with the countries with less population than specified, another with countries with
     * equal or higher population size. The data to be written in the result files can be changed via the 'option' string.
     * @param population population size to separate the countries by.
     * @param option if 'PBI' only country name and PBI will the written.
     *               if 'POB' only country name and Population will be written.
     *               otherwise everything will be written.
     */

    public void separateByPopulation(int population, String option){
        try{
            FileWriter lessThanPopFW = new FileWriter(filename + "LessThan" + population + ".txt");
            FileWriter moreThanPopFW = new FileWriter(filename + "MoreThan" + population + ".txt");
            String line = br.readLine();
            while(line != null){
                String[] fields = line.split(",");
                int linePop = Integer.parseInt(fields[1]);
                if(linePop >= population) writeCountryData(moreThanPopFW, fields, option);
                else writeCountryData(lessThanPopFW, fields, option);
                line = br.readLine();
            }
            br.close();
            lessThanPopFW.close();
            moreThanPopFW.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void writeCountryData(FileWriter fw, String[] fields, String option) throws IOException{
        fw.write(fields[0]);
        if(!option.equals("PBI")) fw.write("," + fields[1]);
        if(!option.equals("POB")) fw.write("," + fields[2]);
        fw.write("\n");
    }
}
