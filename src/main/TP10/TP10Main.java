package main.TP10;

import main.Utils.Scanner.ScannerUtil;

import java.io.IOException;

/**
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class TP10Main {
    public static void main(String[] args){
        ScannerUtil s = new ScannerUtil();
        int in = -1;
        while (in != 0){
            printMenu();
            in = s.getInt();
            switch (in) {
                case 1:
                    countCharOrLines(s);
                    break;
                case 2:
                    changeCapitalization(s);
                    break;
                case 3:
                    parseCountryFileMenu(s);
                    break;
            }

        }
    }

    private static void printMenu(){
        System.out.println("1. Count characters or lines in a file");
        System.out.println("2. Change a file to upper or lower case");
        System.out.println("3. Parse country file menu");
        System.out.println("0. Exit");
    }

    private static void countCharOrLines(ScannerUtil s){
        System.out.println("Enter the filename");
        String filename = s.getString();
        try {
            FileUtil fu = new FileUtil(filename);
            System.out.println("Enter: \n" +
                    "\"C\" to count characters\n" +
                    "\"CC\" to count a specified character\n" +
                    "\"L\" to count lines");
            String option = s.getString();
            switch (option) {
                case "C":
                    System.out.println(fu.countChars());
                    break;
                case "CC":
                    System.out.println("Enter your chosen char");
                    char chosen = s.getString().charAt(0);
                    System.out.println(fu.countChosenChar(chosen));
                    break;
                case "L":
                    System.out.println(fu.countLines());
                    break;
            }
        } catch (IOException e){
            System.out.println("Cannot find file");
        }

    }

    private static void changeCapitalization(ScannerUtil s){
        System.out.println("Enter the filename");
        String filename = s.getString();
        try {
            FileUtil fu = new FileUtil(filename);
            System.out.println("Enter 0 to change the file to upper case, anything else to change it to lower case");
            String choice = s.getString();
            fu.changeCaptilazation(choice.charAt(0) != '0');
        } catch (IOException e){
            System.out.println("Cannot find file");
        }
    }

    private static void parseCountryFileMenu(ScannerUtil s){
        System.out.println("Enter the filename");
        String filename = s.getString();
        try {
            CountryStatsParser csp = new CountryStatsParser(filename);
            System.out.println("Enter the amount of population to separate the file by:");
            int pop = s.getInt();
            System.out.println("Enter:\n" +
                    "\"PBI\" to write country and PBI only \n" +
                    "\"POB\" to write country and population \n" +
                    "anything else to write everything");
            String option = s.getString().toUpperCase();
            csp.separateByPopulation(pop, option);
        } catch (IOException e){
            System.out.println("Cannot find file");
        }
    }
}
