package main.TP10;

import java.io.*;

/**
 * FileUtil is a class containing methods related to text files, counting chars, lines, changing a file to uppercase.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class FileUtil {
    private FileReader fr;
    private String filename;

    public FileUtil(String filename) throws IOException{
        this.fr = new FileReader(filename);
        this.filename = filename;
    }

    /**
     * Counts the amount of chars in the file
     * @return amount of chars in the file
     */

    public int countChars(){
        int count = 0;
        try {
            while(fr.read() != -1) count++;
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Counts the amount of lines in the file
     * @return amount of lines in the file
     */

    public int countLines(){
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(fr);
            while(br.readLine() != null) count++;
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Takes a char and counts how many times it appears in the file
     * @param a char to be counted
     * @return amount of times 'a' appears in the file
     */

    public int countChosenChar(char a){
        int count = 0;
        int casted = (int) a;
        try {
            int c = fr.read();
            while(c != -1){
                if(c == casted) count++;
                c = fr.read();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Creates a copy of the file but in all upper or lower case, as specified.
     * @param toLower if TRUE the copy will be all lower case, if FALSE it will be all upper case.
     */

    public void changeCaptilazation(boolean toLower){
        try {
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw;
            if(toLower) {
                fw = new FileWriter(filename + "Lower" + ".txt");
            } else {
                fw = new FileWriter(filename + "Upper" + ".txt");
            }
            String line = br.readLine();
            while(line != null){
                if(toLower){
                    fw.write(line.toLowerCase() + "\n");
                } else {
                    fw.write(line.toUpperCase() + "\n");
                }
                line = br.readLine();
            }
            br.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
