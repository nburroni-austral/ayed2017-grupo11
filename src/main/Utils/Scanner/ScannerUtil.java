package main.Utils.Scanner;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A Scanner that makes sure the user inputs the type of data requested.
 */
public class ScannerUtil {
    private Scanner input;

    public ScannerUtil() {
        this.input = new Scanner(System.in);
    }

    public String getString(){
        String result = input.nextLine().trim();
        if(result.isEmpty()){
            System.out.println("Please insert text.");
            return getString();
        }
        return result;
    }

    public int getInt(){
        try {
            int result = input.nextInt();
            input.nextLine();
            return result;
        }
        catch (InputMismatchException exc){
            System.out.println("Please insert a number.");
            input.nextLine();
            return getInt();
        }
    }

    public long getLong(){
        try {
            long result = input.nextLong();
            input.nextLine();
            return result;
        }
        catch (InputMismatchException exc){
            System.out.println("Please insert a number.");
            input.nextLine();
            return getLong();
        }
    }

    /*
    public double[] jacobi(double[][] A, double[] v, double tolerance){
        int[] x = new int[A.length];
        for(int i = 0; i < A.length; i++){
            x[i] = 0;
        }
        double norma = 10;
        for(int i = 0; i < A.length; i++){
            double s = 0;
            for(int j = 0; j < A.length; j++){
                if(i != j){
                    s += A[i][j]*x[j];
                }
            }
        }
    }
    */

}
