package main.Utils.ObjectUtil;

import java.io.*;

/**
 * ObjectSaver provides methods to write and read objects from disk.
 */

public class ObjectSaver {

    public ObjectSaver(){}

    /**
     * Takes an object and creates a *.ser file containing it for easy reading afterwards
     * @param obj the object to save
     * @param fileName the name of the file containing the object
     */

    public void writeObject(Object obj, String fileName){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            fos = new FileOutputStream(fileName + ".ser");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        finally {
            if(fos != null) {
                try {
                    fos.close();
                }
                catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
            if(oos != null) {
                try {
                    oos.close();
                }
                catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
    }

    /**
     * Takes the filename of a *.ser file containing a Java object and returns the object contained in it
     * @param fileName the file name that contains the desired object
     * @return the object contained in the file
     */

    public Object readObject(String fileName){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object obj;
        try{
            fis = new FileInputStream(fileName + ".ser");
            ois = new ObjectInputStream(fis);
            obj = ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
        finally {
            if(fis != null) {
                try {
                    fis.close();
                }
                catch (IOException e){
                    throw new RuntimeException(e.getMessage());
                }
            }
            if(ois != null) {
                try {
                    ois.close();
                }
                catch (IOException e){
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
        return obj;
    }
}
