package main.TP5PerezMolina;

import struct.impl.DynamicList;
import struct.istruct.list.List;

/**
 * Represents a train station for a delta t simulation. It has a list of cashiers and can receive a list of clients
 * that enter the station and interact with them.
 *
 * @author Tomas Perez Molina
 */
public class Station {
    private DynamicList<Cashier> cashiers;

    /**
     * Creates a Station with the specified amount of cashiers, critical time and cycle length.
     * @param amountOfCashiers the number of cashiers in the station.
     * @param criticalTime the time, in seconds, after which the cashiers change their behaviour.
     * @param cycleLength the length of each cycle in seconds.
     */

    public Station(int amountOfCashiers, int criticalTime, int cycleLength){
        this.cashiers = new DynamicList<>();
        for(int i = 0; i < amountOfCashiers; i++){
            cashiers.insertNext(new Cashier(criticalTime, cycleLength));
        }
    }

    /**
     * Takes a list of clients representing a group that enters the station.
     * @param clientList list of clients entering the station.
     */

    public void addClients(List<Client> clientList){
        for (int i = 0; i < clientList.size(); i++) {
            clientList.goTo(i);
            Client client = clientList.getActual();
            int chosenQueue = client.chooseQueue(cashiers.size());
            cashiers.goTo(chosenQueue);
            cashiers.getActual().addClient(client);
        }
    }

    /**
     * Advances to the next cycle in the simulation.
     * @param cycle the current cycle in the simulation.
     */

    public void nextCycle(int cycle){
        for(int i = 0; i < cashiers.size(); i++){
            cashiers.goTo(i);
            cashiers.getActual().nextCycle(cycle);
        }
    }

    /**
     * Returns a report of all the cashiers in the station
     * @return String containing full station report
     */
    public String report(){
        StringBuilder builder = new StringBuilder();
        builder.append("STATION REPORT \n\n");
        for(int i = 0; i < cashiers.size(); i++){
            cashiers.goTo(i);
            builder.append("Cashier ").append(i + 1).append(":\n");
            builder.append(cashiers.getActual().report()).append("\n");
        }
        return builder.toString();
    }
}
