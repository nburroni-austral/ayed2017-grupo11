package main.TP5PerezMolina;

public class StationSimulationMain {


    public static void main(String[] args){
        int amountOfCashiers = 3;
        int cycleLength = 10;
        int totalTime = 57600;
        int criticalTime = 30;
        int clientsPerCycle = 5;

        StationSimulation stationSimulation = new StationSimulation(amountOfCashiers, criticalTime, totalTime, cycleLength, clientsPerCycle);
        stationSimulation.run();
        System.out.println(stationSimulation.results());
    }
}
