package main.TP5PerezMolina;

import struct.impl.DynamicList;

/**
 * Train station simulation that creates Clients that enter a Station object and simulates the interactions within
 * the time the station is open.
 */

public class StationSimulation {
    private Station station;
    private int totalCycles;
    private int clientsPerCycle;
    private int cycleLength;

    /**
     * Creates a StationSimulation
     * @param amountOfCashiers the amount of cashiers in the station.
     * @param criticalTime the time, in seconds, after which the cashiers change behaviour.
     * @param totalTime time the station is open, in seconds
     * @param cycleLength length of each cycle, in seconds.
     * @param clientsPerCycle number of clients that enter the station each cycle.
     */

    public StationSimulation(int amountOfCashiers, int criticalTime, int totalTime, int cycleLength, int clientsPerCycle){
        this.totalCycles = totalTime/cycleLength;
        this.cycleLength = cycleLength;
        this.station = new Station(amountOfCashiers, totalTime - criticalTime, cycleLength);
        this.clientsPerCycle = clientsPerCycle;
    }

    /**
     * Runs the simulation.
     */

    public void run(){
        for(int i = 0; i < totalCycles; i++){
            DynamicList<Client> clientList = new DynamicList<>();
            for(int j = 0; j < clientsPerCycle; j++){
                clientList.insertNext(new Client(i*cycleLength));
            }
            station.addClients(clientList);
            station.nextCycle(i);
        }
    }

    /**
     * @return String containing the simulation results.
     */

    public String results(){
        return station.report();
    }
}
