package main.TP5PerezMolina;

import java.util.Random;

/**
 * Represents a train station client for a delta t simulation. It can choose a random queue
 * and keeps track of the time it spent in it.
 *
 * @author Tomas Perez Molina
 */

public class Client {
    private int initialTime;
    private Random r;

    /**
     * Creates a client with the specified initial time.
     * @param initialTime the time the client enters the station in seconds.
     */

    public Client(int initialTime) {
        this.initialTime = initialTime;
        this.r = new Random();
    }

    /**
     * Takes an integer representing the time the client exits the queue, returns the total time it waited in the queue.
     * @param time the time the client exits the queue.
     * @return total time the client spent in the queue.
     */
    public int exitQueue(int time){
        return time - initialTime;
    }

    /**
     * Takes an integer representing the number of queues to choose from and returns a random number
     * representing the chosen queue.
     * @param numberOfQueues the total number of queues.
     * @return chosen queue.
     */
    public int chooseQueue(int numberOfQueues){
        return r.nextInt(numberOfQueues);
    }
}
