package main.TP5PerezMolina;

import struct.impl.DynamicQueue;

import java.util.Random;

/**
 * Represents a train station cashier for a delta t simulation. Each cashier has a client queue and
 * has a probability to take care of the first client in the queue, adding to the cashiers revenue and dequeuing the client.
 *
 * @author Tomas Perez Molina
 */

public class Cashier {
    private DynamicQueue<Client> queue;
    private float revenue;
    private int servedClients, totalWaitingTime, criticalTime, totalIdleTime, cycleLength;
    private static final float TICKET_PRICE = 0.70f;
    private static final float PROBABILITY = 0.30f;
    private Random r;

    /**
     * Creates a Cashier with the specified criticalTime and cycleLength
     * @param criticalTime the time in seconds when the cashier starts to empty the queue each cycle
     * @param cycleLength the length of a cycle in seconds
     */

    public Cashier(int criticalTime, int cycleLength) {
        queue = new DynamicQueue<>();
        revenue = 0;
        r = new Random();
        this.criticalTime = criticalTime;
        this.cycleLength = cycleLength;
    }

    /**
     * Adds a client to the cashier's queue.
     * @param client the client to be added.
     */
    public void addClient(Client client){
        queue.enqueue(client);
    }

    /**
     * Advances to the next cycle in the simulation.
     * @param cycle the current cycle in the simulation.
     */

    public void nextCycle(int cycle){
        int currentTime = cycle* cycleLength;
        if(!queue.isEmpty()) {
            if (currentTime >= criticalTime) {
                emptyQueues(currentTime);
            } else if (r.nextFloat() <= PROBABILITY) {
                serveClient(currentTime);
            }
        }
        else totalIdleTime += cycleLength;
    }

    private void emptyQueues(int time){
        while(!queue.isEmpty()){
            serveClient(time);
        }
    }

    private void serveClient(int time){
        totalWaitingTime += queue.dequeue().exitQueue(time);
        revenue += TICKET_PRICE;
        servedClients++;
    }

    /**
     * The cashier reports it's median client waiting time, current revenue, and total idle time.
     * @return String containing a full cashier report.
     */

    public String report(){
        return "Median Client Waiting Time: " + (float) totalWaitingTime / servedClients + "\n"
                + "Revenue: " + revenue + "\n"
                + "Idle Time: " + totalIdleTime + "\n";
    }
}
