package main.TP1.Merge;

/**
 * Contains a static method that merges two sorted arrays into a single sorted array.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class Merge{

    /**
     * Receives two sorted arrays and returns a single sorted array with all the elements.
     *
     * @param a first sorted array.
     * @param b second sorted array.
     * @return an array that contains all elements from a and b in sorted order.
     */

    public static <T extends Comparable<T>> Comparable<T>[] merge(T[] a, T[] b){
        int i = 0;
        int j = 0;
        int k = 0;
        Comparable<T>[] c = new Comparable[a.length+b.length];
        while(i < a.length && j < b.length) {
            if ((a[i].compareTo(b[j])) <= 0) {
                c[k] = a[i];
                i++;
                k++;
            } else {
                c[k] = b[j];
                j++;
                k++;
            }
        }
        if(i >= a.length){
            for(; j < b.length; j++, k++){
                c[k] = b[j];
            }
        }
        else{
            for(; i < a.length; i++, k++){
                c[k] = a[i];
            }
        }
        return c;
    }
}
