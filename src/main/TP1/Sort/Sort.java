package main.TP1.Sort;

/**
 * Contains multiple static methods for array sorting using different algorithms
 *
 * <ul>
 *     <li>Bubble sort</li>
 *     <li>Selection sort</li>
 *     <li>Insertion sort</li>
 * </ul>
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class Sort{

    /**
     * Sorts an array using bubble sort.
     *
     * @param array the array to be sorted.
     * @param <T> generic type that extends Comparable<T>.
     */

    public static <T extends Comparable<T>> void bubbleSort(T[] array){
        for(int i = 0; i < array.length-1; i++){
            for(int j = i+1; j < array.length-2; j++) {
                if (array[i].compareTo(array[j]) > 0) {
                    T temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    /**
     * Sorts an array using selection sort.
     *
     * @param array the array to be sorted.
     * @param <T> generic type that extends Comparable<T>.
     */

    public static <T extends Comparable<T>> void selectionSort(T[] array){
        for (int i = 0;i < array.length-1;i++){
            int min = i;
            for (int j = i+1;j < array.length;j++){
                if(array[j].compareTo(array[min]) < 0){
                    min = j;
                }
            }
            if(min != i){
                T aux = array[min];
                array[min] = array[i];
                array[i] = aux;
            }
        }
    }

    /**
     * Sorts an array using a recursive selection sort.
     *
     * @param array the array to be sorted.
     * @param <T> generic type that extends Comparable<T>.
     */

    public static <T extends Comparable<T>> void selectionSortRecursive(T[] array){
        selectionSortRecursiveAux(array,0);
    }

    private static <T extends Comparable<T>> void selectionSortRecursiveAux(T[] array,int i){
        if (i < array.length-1) {
            selectionSortRecursiveAux2(array, i, i + 1, i);
            selectionSortRecursiveAux(array,i+1);
        }
    }

    private static <T extends Comparable<T>> void selectionSortRecursiveAux2(T[] array,int i,int j,int min) {
        if(j < array.length){
            if(array[j].compareTo(array[min]) < 0)
                min = j;
            selectionSortRecursiveAux2(array,i,j+1,min);
        }
        else {
            T aux = array[min];
            array[min] = array[i];
            array[i] = aux;
        }
    }

    /**
     * Sorts an array using insertion sort.
     *
     * @param array the array to be sorted.
     * @param <T> generic type that extends Comparable<T>.
     */

    public static <T extends Comparable<T>> void insertSort(T[] array){
        for(int i = 1;i<array.length;i++) {
            int j = i;
            T valueToInsert = array[i];
            while (j > 0 && array[j - 1].compareTo(valueToInsert) > 0) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = valueToInsert;
        }
    }
}
