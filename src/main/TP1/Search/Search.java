package main.TP1.Search;

/**
 * Contains two static methods for array searching, one using sequential search and another one using binary search.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class Search {

    /**
     * Searches for the number in the array, using sequential search.
     *
     * @param k searched for number.
     * @param a array to search in.
     * @return index of the number in the array, -1 if it's not in the array.
     */

    public static int sequentialSearch(int k, int[]a){
        for(int i = 0; i < a.length; i++){
            if(k == a[i]) return i;
        }
        return -1;
    }

    /**
     * Searches for the number in the sorted array, using binary search.
     *
     * @param k searched for number.
     * @param a sorted array to search in.
     * @return index of the number in the array, -1 if it's not in the array.
     */

    public static int binarySearch(int k, int[] a){
        int start = 0;
        int end = a.length-1;

        int middleIndex = (end+start)/2;
        while(k != a[middleIndex]) {
            if(start < end) {
                if (k < a[middleIndex]) {
                    end = middleIndex-1;
                } else {
                    start = middleIndex+1;
                }
                middleIndex = (end+start) / 2;
            }
            else return -1;
        }
        return middleIndex;
    }
}
