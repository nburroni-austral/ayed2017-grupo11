package main.TP7;

import struct.istruct.BinaryTree;

import java.util.LinkedList;

/**
 * BinaryTreeShower contains methods to print a binary tree in multiple ways:
 * <ul>
 *     <li>Pre-Order</li>
 *     <li>In-Order</li>
 *     <li>Post-Order</li>
 *     <li>Level-Order</li>
 * </ul>
 */

public class BinaryTreeShower {

    /**
     * Prints a binary tree in pre-order.
     * @param b binary tree.
     */

    public void preOrderPrint(BinaryTree b){
        System.out.println(preOrderString(b));
    }

    /**
     * @param b binary tree.
     * @return string of the elements of b in pre-order.
     */

    public String preOrderString(BinaryTree b){
        if(!b.isEmpty())
            return (b.getRoot() + preOrderString(b.getLeft()) + preOrderString(b.getRight()));
        return "";
    }

    /**
     * Prints a binary tree in in-order.
     * @param b binary tree.
     */

    public void inOrderPrint(BinaryTree b){
        System.out.println(inOrderString(b));
    }

    /**
     * @param b binary tree.
     * @return string of the elements of b in in-order.
     */

    public String inOrderString(BinaryTree b){
        if(!b.isEmpty())
            return (inOrderString(b.getLeft()) + b.getRoot() + inOrderString(b.getRight()));
        return "";
    }

    /**
     * Prints a binary tree in post-order.
     * @param b binary tree.
     */

    public void postOrderPrint(BinaryTree b){
        System.out.println(postOrderString(b));
    }

    /**
     * @param b binary tree.
     * @return string of the elements of b in post-order.
     */

    public String postOrderString(BinaryTree b){
        if(!b.isEmpty())
            return (postOrderString(b.getLeft()) + postOrderString(b.getRight()) + b.getRoot());
        return "";
    }

    /**
     * Prints a binary tree in level-order.
     * @param b binary tree.
     */

    public void levelOrderPrint(BinaryTree b){
        System.out.println(levelOrderString(b));
    }

    /**
     * @param b binary tree.
     * @return string of the elements of b in level-order.
     */

    public String levelOrderString(BinaryTree b){
        StringBuilder result = new StringBuilder();
        if(b.isEmpty()) return result.toString();
        if(b.getLeft().isEmpty() || b.getRight().isEmpty()) result.append(b.getRoot());
        LinkedList<LinkedList<Object>> elementList = levelOrderAux(b, new LinkedList<>(), 0);
        for(LinkedList<Object> list: elementList){
            for(Object element: list){
                result.append(element);
            }
        }
        return result.toString();
    }

    private LinkedList<LinkedList<Object>> levelOrderAux(BinaryTree b, LinkedList<LinkedList<Object>> elementList, int level){
        if(!b.isEmpty()) {
            if(elementList.size() < level + 1) {
                elementList.add(new LinkedList<>());
            }
            elementList.get(level).add(b.getRoot());
            levelOrderAux(b.getLeft(), elementList, level + 1);
            levelOrderAux(b.getRight(), elementList, level + 1);
        }
        return elementList;
    }
}
