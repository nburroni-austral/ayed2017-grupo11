package main.TP7;

import struct.istruct.BinaryTree;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * BinaryTreeAPI containing methods that check the multiple properties of binary trees, as well as methods that compare
 * trees with one another.
 */

public class BinaryTreeAPI {

    //Exercise 13

    /**
     * Calculates the weight of a binary tree.
     * Weight: number of descendants + 1.
     * @param b binary tree
     * @return weight of b
     */

    public int weight(BinaryTree b){
        if(b.isEmpty())
            return 0;
        return 1 + weight(b.getLeft()) + weight(b.getRight());
    }

    /**
     * Calculates the amount of leaves of a binary tree.
     * Leaf: tree with no descendants.
     * @param b binary tree
     * @return the amount of leaves in b.
     */

    public int leafAmount(BinaryTree b){
        if(b.isEmpty())
            return 0;
        if(b.getLeft().isEmpty() && b.getRight().isEmpty()){
            return 1;
        }
        return leafAmount(b.getLeft()) + leafAmount(b.getRight());
    }

    /**
     * Calculates the amount of elements in a specific level of a binary tree.
     * @param b binary tree
     * @param level desired level
     * @return amount of elements in level of b
     */

    public int elementsInLevel(BinaryTree b,int level){
        return elementsInLevelAux(b,level,0);
    }

    private int elementsInLevelAux(BinaryTree b, int level, int i){
        if(b.isEmpty()){
            return 0;
        }
        if(i == level){
            return 1;
        }
        return elementsInLevelAux(b.getLeft(),level,i + 1) + elementsInLevelAux(b.getRight(),level,i + 1);
    }

    /**
     * Calculates the amount of times an element is present in a binary tree.
     *
     * @param b binary tree
     * @param element desired element
     * @param <T> type of the elements stored in b
     * @return the amount of times element appears in b
     */

    public <T> int numberOfRepetitions(BinaryTree<T> b, T element){
        if(b.isEmpty()) return 0;
        if(b.getRoot().equals(element)){
            return 1 + numberOfRepetitions(b.getLeft(), element)
                    + numberOfRepetitions(b.getRight(), element);
        }
        return numberOfRepetitions(b.getLeft(), element)
                + numberOfRepetitions(b.getRight(), element);
    }

    /**
     * Calculates the height of a binary tree.
     * Height: length of the path between the root and its most distant leaf
     * @param b binary tree
     * @return the height of b.
     */

    public int height(BinaryTree b){
        if(b.isEmpty() || (b.getLeft().isEmpty() && b.getRight().isEmpty())) return 0;
        return 1 + Math.max(height(b.getLeft()), height(b.getRight()));
    }

    //Exercise14

    /**
     * Checks if two binary trees are equal, which means they have the same shape and elements in the same positions.
     * @param b1 binary tree 1
     * @param b2 binary tree 2
     * @return true if b1 is equal to b2, false if it's not.
     */
    public boolean equals(BinaryTree b1, BinaryTree b2){
        if(b1.isEmpty() && b2.isEmpty()) return true;
        if(b1.isEmpty() || b2.isEmpty()) return false;
        return b1.getRoot().equals(b2.getRoot())
                && equals(b1.getLeft(), b2.getLeft())
                && equals(b1.getRight(), b2.getRight());

    }

    /**
     * Checks if two binary trees are isomorf, which means they have the same shape.
     * @param b1 binary tree 1
     * @param b2 binary tree 2
     * @return true if b1 and b2 are isomorfs, false if they aren't.
     */

    public boolean isomorf(BinaryTree b1, BinaryTree b2){
        if(b1.isEmpty() && b2.isEmpty()) return true;
        if(b1.isEmpty() || b2.isEmpty()) return false;
        return isomorf(b1.getLeft(), b2.getLeft()) && isomorf(b1.getRight(), b2.getRight());
    }

    /**
     * Checks if two binary trees are similar, which means they have the same elements.
     * @param b1 binary tree 1
     * @param b2 binary tree 2
     * @return true if b1 and b2 are similar, false if they aren't.
     */

    public boolean similar(BinaryTree b1, BinaryTree b2){
        LinkedList b1Elements = new LinkedList();
        LinkedList b2Elements = new LinkedList();
        getElements(b1, b1Elements);
        getElements(b2, b2Elements);
        if(b1Elements.size() == b2Elements.size()){
            b1Elements.removeAll(b2Elements);
            return b1Elements.isEmpty();
        }
        return false;
    }

    private LinkedList getElements(BinaryTree b, LinkedList list){
        if(b.isEmpty()) return list;
        list.add(b.getRoot());
        getElements(b.getLeft(), list);
        getElements(b.getRight(), list);
        return list;
    }

    /**
     * Calculates the sum of all the elements in an Integer binary tree
     * @param b binary tree of integers
     * @return sum of all elements in b
     */

    public int sum(BinaryTree<Integer> b){
        if(b.isEmpty())
            return 0;
        return b.getRoot() + sum(b.getLeft()) + sum(b.getRight());
    }

    /**
     * Calculates the sum of all the elements multiple of 3 in an Integer binary tree
     * @param b binary tree of integers
     * @return sum of all elements in b which are a multiple of 3
     */

    public int threeMultipleSum(BinaryTree<Integer> b){
        if(b.isEmpty())
            return 0;
        return b.getRoot() % 3 == 0 ? b.getRoot() + threeMultipleSum(b.getLeft()) + threeMultipleSum(b.getRight()) :
                threeMultipleSum(b.getLeft()) + threeMultipleSum(b.getRight());
    }

    /**
     * Checks if a binary tree is complete, which means all nodes have two or no descendants.
     * @param b binary tree
     * @return true if b is complete, false if it isn't.
     */

    public boolean isComplete(BinaryTree b){
        if(b.isEmpty()){
            return false;
        }
        if(b.getLeft().isEmpty() && b.getRight().isEmpty())
            return true;
        if(!b.getLeft().isEmpty() && b.getRight().isEmpty()
                || b.getLeft().isEmpty() && !b.getRight().isEmpty())
            return false;
        return isComplete(b.getLeft()) && isComplete(b.getRight());
    }

    /**
     * Checks if a binary tree is full, which means it's complete and all it's roots are on the same level.
     * @param b
     * @return
     */

    public boolean isFull(BinaryTree b){
        int height = height(b);
        return isComplete(b) && Math.pow(2,height) == elementsInLevel(b,height);
    }

    /**
     * Checks if a binary tree contains another tree.
     * @param b1 binary tree to search in
     * @param b2 binary tree to search for
     * @return true if b1 contains b2, false if it doesn't.
     */

    public boolean occurs(BinaryTree b1, BinaryTree b2){
        if(b1.isEmpty() || b2.isEmpty()) return false;
        return equals(b1, b2) || (occurs(b1.getLeft(), b2) || occurs(b1.getRight(), b2));
    }

    /**
     * Prints the frontier of a binary tree.
     * Frontier: All the leaves of a binary tree.
     * @param b binary tree
     */

    public void showFrontier(BinaryTree b){
        if(b.getLeft().isEmpty() && b.getRight().isEmpty()) {
            System.out.println(b.getRoot().toString());
            return;
        }
        if(!b.getLeft().isEmpty())
            showFrontier(b.getLeft());
        if(!b.getRight().isEmpty())
            showFrontier(b.getRight());
    }

    /**
     * Creates a list with the frontier of a binary tree.
     * @param b binary tree
     * @param <T> type of elements stored in b.
     * @return list with the frontier of b.
     */

    public<T> ArrayList<T> frontier(BinaryTree<T> b){
        ArrayList<T> list = new ArrayList<T>();
        binaryTreeFrontierAux(b,list);
        return list;
    }

    private<T> void binaryTreeFrontierAux(BinaryTree<T> b, ArrayList<T> list){
        if(b.getLeft().isEmpty() && b.getRight().isEmpty()) {
            list.add(b.getRoot());
            return;
        }
        if(!b.getLeft().isEmpty())
            binaryTreeFrontierAux(b.getLeft(), list);
        if(!b.getRight().isEmpty())
            binaryTreeFrontierAux(b.getRight(), list);
    }

    /**
     * Checks if a tree is stable, which means the descendants are always smaller than their parents
     * @param b binary tree
     * @param <T> type of element stored in b, must be comparable.
     * @return true if b is stable, false if it isn't.
     */

    public <T extends Comparable<T>> boolean isStable(BinaryTree<T> b){
        boolean result = true;
        if(b.isEmpty() || (b.getLeft().isEmpty() && b.getRight().isEmpty())) return true;
        if(!b.getRight().isEmpty()){
            result = b.getRoot().compareTo(b.getRight().getRoot()) >= 0;
        }
        if(!b.getLeft().isEmpty()){
            result &= b.getRoot().compareTo(b.getLeft().getRoot()) >= 0;
        }
        return result && isStable(b.getLeft()) && isStable(b.getRight());
    }
}
