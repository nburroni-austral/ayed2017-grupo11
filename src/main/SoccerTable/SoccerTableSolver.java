package main.SoccerTable;

import javafx.util.Pair;
import main.SoccerTable.Exceptions.IncompatibleSoccerTableExc;
import main.SoccerTable.Exceptions.InvalidResultExc;
import struct.impl.DynamicStack;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * SoccerTableSolver contains method to solve a complete SoccerTable or get possible results from a single match
 */

public class SoccerTableSolver {

    /**
     * Takes a SoccerTable and returns a String containing the results from its matches in continuous order.
     * "X" means the match was a draw.
     * "1" means the local team won.
     * "2" means the away team won.
     * @param table SoccerTable to solve
     * @return String containing the matches results
     */

    public String solve(SoccerTable table){
        ArrayList<DynamicStack<Integer>> partialResults = new ArrayList<>();
        ArrayList<Pair<String, String>> matches = table.getMatches();
        StringBuilder result = new StringBuilder();
        int i = 0;
         do {
            while (i < matches.size() && i >= 0) {
                if (partialResults.size() == i) {
                    DynamicStack<Integer> possibleResults = possibleResults(table, partialResults, i);
                    if (!possibleResults.isEmpty()) {
                        partialResults.add(possibleResults);
                        i++;
                    } else {
                        i--;
                    }
                } else {
                    DynamicStack<Integer> possibleResults = partialResults.get(i);
                    if (possibleResults.size() == 1) {
                        partialResults.remove(i);
                        i--;
                    } else {
                        possibleResults.pop();
                        i++;
                    }
                }
            }
            if(i < 0) throw new IncompatibleSoccerTableExc("Unsolvable table");
            i--;
        } while(!finishedResult(table, partialResults));

        partialResults.forEach(resultStack -> result.append(resultStack.peek() != 0 ? resultStack.peek() : "X").append(" "));

        return result.toString();
    }

    /**
     * Takes a SoccerTable, a list with the previous results and an int indicating the match index and
     * returns the possible results for the chosen match.
     * @param table soccer table containing the chosen match.
     * @param partialResults the previous match results.
     * @param matchIndex the index of the chosen match on the soccer table.
     * @return the possible results for the chosen match.
     */

    public DynamicStack<Integer> possibleResults(SoccerTable table, ArrayList<DynamicStack<Integer>> partialResults, int matchIndex){
        DynamicStack<Integer> possibleResults = new DynamicStack<>();
        ArrayList<Pair<String, String>> matchList = table.getMatches();

        String localTeam = matchList.get(matchIndex).getKey();
        String awayTeam = matchList.get(matchIndex).getValue();

        int currentLocalPoints = getCurrentTeamPoints(matchList, partialResults, localTeam);
        int currentAwayPoints = getCurrentTeamPoints(matchList, partialResults, awayTeam);

        int finalLocalPoints = table.getPointTable().get(localTeam);
        int finalAwayPoints = table.getPointTable().get(awayTeam);

        if(currentLocalPoints < finalLocalPoints){ // local team can gain points
            if(currentAwayPoints < finalAwayPoints){ // away team can gain points
                possibleResults.push(0);
                if(currentAwayPoints + 3 <= finalAwayPoints){ //away team can win
                    possibleResults.push(2);
                }
            }
            if (currentLocalPoints + 3 <= finalLocalPoints) possibleResults.push(1); //local team can win
        }
        else if(currentAwayPoints + 3 <= finalAwayPoints) possibleResults.push(2); //away team can win

        return possibleResults;
    }

    private int getCurrentTeamPoints(ArrayList<Pair<String,String>> matches, ArrayList<DynamicStack<Integer>> partialResults, String team){
        int teamPoints = 0;
        for(int i = 0; i < partialResults.size(); i++){
            if(matches.get(i).getKey().equals(team)){
                switch (partialResults.get(i).peek()){
                    case 0:
                        teamPoints++;
                        break;
                    case 1:
                        teamPoints += 3;
                    case 2:
                        break;
                    default:
                        throw new InvalidResultExc();
                }
            }
            else if(matches.get(i).getValue().equals(team)){
                switch (partialResults.get(i).peek()){
                    case 0:
                        teamPoints++;
                        break;
                    case 2:
                        teamPoints += 3;
                    case 1:
                        break;
                    default:
                        throw new InvalidResultExc();
                }
            }
        }
        return teamPoints;
    }

    private boolean finishedResult(SoccerTable table, ArrayList<DynamicStack<Integer>> partialResults){
        HashMap<String, Integer> resultsPanel = table.getPointTable();

        for (String team: resultsPanel.keySet()){
            if(resultsPanel.get(team) != getCurrentTeamPoints(table.getMatches(), partialResults, team)){
                return false;
            }
        }

        return true;
    }

}
