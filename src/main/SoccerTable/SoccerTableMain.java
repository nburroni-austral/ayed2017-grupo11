package main.SoccerTable;

import main.SoccerTable.Exceptions.IncompatibleSoccerTableExc;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Receives a user input of multiple soccer tables and displays the results of each one.
 */

public class SoccerTableMain {

    /**
     * Runs a menu for the user to input soccer tables and solves them once.
     * If there's a mistake in the input, it asks for another table or for the user to exit the program.
     * @param args
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(
                "Please enter soccer tables with the following form and then '-1' when the input is finished\n\n" +
                "2 1 (Number of teams and number of matches) \n" +
                "Barcelona 3 (Team and score)\n" +
                "Madrid 0\n" +
                "Madrid Barcelona (Match)\n\n");
        boolean repeat;
        do {
            repeat = false;
            String input = scanner.next();
            String nextInput;
            ArrayList<ArrayList<String>> inputLists = new ArrayList<>();
            int i = -1;
            while (!input.equals("-1")) {
                nextInput = scanner.next();
                if (isNumeric(input) && isNumeric(nextInput)) {
                    inputLists.add(new ArrayList<>());
                    i++;
                }
                if (i >= 0) {
                    inputLists.get(i).add(input);
                    inputLists.get(i).add(nextInput);
                }
                input = scanner.next();
            }
            try {
                ArrayList<String> results = solveInput(inputLists);
                if(results.size() > 0) System.out.println("The results are:");
                results.forEach(System.out::println);
            } catch (IncompatibleSoccerTableExc exc) {
                System.out.println(exc.getMessage());
                System.out.println("Enter -1 if you wish to exit");
                repeat = true;
            }
        } while(repeat);

    }

    private static boolean isNumeric(String s){
        return s.matches("-?\\d+(\\.\\d+)?");
    }

    private static ArrayList<String> solveInput(ArrayList<ArrayList<String>> inputLists){
        SoccerTableParser soccerTableParser = new SoccerTableParser();
        SoccerTableSolver soccerTableSolver = new SoccerTableSolver();
        ArrayList<SoccerTable> soccerTables = new ArrayList<>();
        ArrayList<String> results = new ArrayList<>();

        inputLists.forEach(inputList -> soccerTables.add(soccerTableParser.parseToSoccerTable(inputList)));
        soccerTables.forEach(soccerTable -> results.add(soccerTableSolver.solve(soccerTable)));

        return results;
    }
}
