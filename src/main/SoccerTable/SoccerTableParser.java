package main.SoccerTable;

import javafx.util.Pair;
import main.SoccerTable.Exceptions.IncompatibleSoccerTableExc;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Contains a method to parse a list of strings containing a soccer table input into a SoccerTable object
 */

public class SoccerTableParser {

    /**
     * Takes a list of strings containing a soccer table and parses it into a SoccerTable object.
     * @param inputList list of Strings containing a soccer table.
     * @return SoccerTable object containing the data from inputList.
     */

    public SoccerTable parseToSoccerTable(ArrayList<String> inputList){
        Integer numberOfTeams;
        Integer numberOfMatches;

        HashMap<String, Integer> pointTable = new HashMap<>();
        ArrayList<Pair<String, String>> matchList = new ArrayList<>();
        try {
            numberOfTeams = Integer.parseInt(inputList.get(0));
            numberOfMatches = Integer.parseInt(inputList.get(1));

            if(inputList.size() != 2 + numberOfMatches*2 + numberOfTeams*2) throw new IncompatibleSoccerTableExc("Invalid Soccer Table Input");

            int i = 2;
            for (int sums = 0; sums < numberOfTeams; i += 2, sums++) {
                int teamPoints = Integer.parseInt(inputList.get(i + 1));
                if(teamPoints < 0) throw new IncompatibleSoccerTableExc("Invalid Soccer Table Input");
                pointTable.put(inputList.get(i), teamPoints);
            }
            for(int sums = 0; sums < numberOfMatches; i += 2, sums++){
                matchList.add(new Pair<>(inputList.get(i), inputList.get(i+1)));
            }
        }
        catch (NumberFormatException exc){
            throw new IncompatibleSoccerTableExc("Invalid Soccer Table Input");
        }

        return new SoccerTable(new Pair<>(numberOfTeams, numberOfMatches), pointTable, matchList);
    }
}
