package main.SoccerTable.Exceptions;

public class IncompatibleSoccerTableExc extends RuntimeException{
    public IncompatibleSoccerTableExc(String message) {
        super(message);
    }
}
