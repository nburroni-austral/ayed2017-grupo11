package main.SoccerTable.Exceptions;

public class InvalidResultExc extends RuntimeException{
    public InvalidResultExc() {
        super("Invalid results");
    }
}
