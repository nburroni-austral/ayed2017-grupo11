package main.SoccerTable;

import javafx.util.Pair;
import main.SoccerTable.Exceptions.IncompatibleSoccerTableExc;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Represents a soccer table with number of teams, number of matches, table with scores for each team and a list of matches.
 */

public class SoccerTable {
    private Pair<Integer, Integer> teamAndMatchAmount;
    private HashMap<String, Integer> pointTable;
    private ArrayList<Pair<String, String>> matches;

    public SoccerTable(int teamAmount, int matchAmount) {
        this.teamAndMatchAmount = new Pair<>(teamAmount, matchAmount);
        pointTable = new HashMap<>();
        matches = new ArrayList<>();
    }

    public SoccerTable(Pair<Integer, Integer> teamAndMatchAmount, HashMap<String, Integer> pointTable, ArrayList<Pair<String, String>> matches) {
        this.teamAndMatchAmount = teamAndMatchAmount;

        if(pointTable.size() == teamAndMatchAmount.getKey()) this.pointTable = pointTable;
        else throw new IncompatibleSoccerTableExc("Incompatible point table");

        if(matches.size() == teamAndMatchAmount.getValue()) this.matches = matches;
        else throw new IncompatibleSoccerTableExc("Incompatible match table");
    }

    /**
     * Adds a team to the table if there's space, if not it throws IncompatibleSoccerTableExc.
     * @param team the team to be added
     * @param points the points of the team to be added
     * @throws IncompatibleSoccerTableExc if the team list is full
     */

    public void addTeam(String team, int points){
        if(pointTable.size() < teamAndMatchAmount.getKey()){
            pointTable.put(team, points);
        }
        else throw new IncompatibleSoccerTableExc("Team list full");
    }

    /**
     * Removes a team from the table
     * @param team the team to be removed
     */

    public void removeTeam(String team){
        pointTable.remove(team);
    }

    /**
     * Adds a match to the table if the teams are on the team list and there's space,
     * if not it throws IncompatibleSoccerTableExc.
     * @param localTeam the local team in the match
     * @param awayTeam the away team in the match
     * @throws IncompatibleSoccerTableExc if the team list is full or the teams are not listed on the team list.
     */

    public void addMatch(String localTeam, String awayTeam){
        if(matches.size() < teamAndMatchAmount.getValue()){
            if(pointTable.containsKey(localTeam) && pointTable.containsKey(awayTeam)) {
                matches.add(new Pair<>(localTeam, awayTeam));
            }
            else throw new IncompatibleSoccerTableExc("The team names are not listed in the table");
        }
        else throw new IncompatibleSoccerTableExc("Match list full");
    }

    /**
     * Removes a match from the table
     * @param localTeam the local team in the match.
     * @param awayTeam the away team in the match.
     */

    public void removeMatch(String localTeam, String awayTeam){
        matches.remove(new Pair<>(localTeam, awayTeam));
    }

    public Pair<Integer, Integer> getTeamAndMatchAmount() {
        return teamAndMatchAmount;
    }

    public HashMap<String, Integer> getPointTable() {
        return pointTable;
    }

    public ArrayList<Pair<String, String>> getMatches() {
        return matches;
    }
}
