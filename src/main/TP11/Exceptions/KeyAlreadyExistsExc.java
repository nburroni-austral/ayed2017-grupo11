package main.TP11.Exceptions;

/**
 * @author Manuel Pedrozo
 */
public class KeyAlreadyExistsExc extends RuntimeException {
    public KeyAlreadyExistsExc(){
        super("A registry with that key already exists.");
    }
}
