package main.TP11.Exceptions;

/**
 * @author Tomas Perez Molina
 */
public class InvalidUserTypeDataExc extends RuntimeException {
    public InvalidUserTypeDataExc() {
        super("Given data is incompatible with factory attributes");
    }
}
