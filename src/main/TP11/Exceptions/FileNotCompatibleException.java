package main.TP11.Exceptions;

/**
 * @author Manuel Pedrozo
 */
public class FileNotCompatibleException extends RuntimeException{
    public FileNotCompatibleException(){
        super("File not compatible");
    }
}
