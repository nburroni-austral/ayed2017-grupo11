package main.TP11.Exceptions;

/**
 * @author Manuel Pedrozo
 */
public class RegistryNotFoundException extends RuntimeException {
    public RegistryNotFoundException() {
        super("No such registry");
    }
}
