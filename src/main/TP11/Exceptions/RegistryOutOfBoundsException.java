package main.TP11.Exceptions;

/**
 * @author Manuel Pedrozo
 */
public class RegistryOutOfBoundsException extends RuntimeException {
    public RegistryOutOfBoundsException(){
        super("Registry index out of bounds");
    }
}
