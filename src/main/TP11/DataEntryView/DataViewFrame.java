package main.TP11.DataEntryView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;

/**
 * JFrame for viewing, editing and filtering UserType data.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataViewFrame extends JFrame {
    private JLabel counter;
    private int counterNumber;

    public DataViewFrame(String keyName, ActionListener saveButtonAL, WindowListener windowListener, DataViewMenuBar dataViewMenuBar, DataEditPanel dataEditPanel, DataViewPanel dataViewPanel, PlusMinusButtonPanel plusMinusButtonPanel, DataFilterPanel dataFilterPanel){
        super("Data Entry Manager");
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        Font dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 13));

        JScrollPane scrollPaneEdit = new JScrollPane(dataEditPanel);

        JButton saveButton = boxUtil.createButton("Save", 90, 30);
        saveButton.addActionListener(saveButtonAL);

        JPanel editPanel = new JPanel();
        editPanel.setLayout(new BoxLayout(editPanel, BoxLayout.Y_AXIS));
        editPanel.setPreferredSize(boxUtil.createDimension(300, 230));
        editPanel.add(scrollPaneEdit);
        editPanel.add(boxUtil.createFiller(15));
        editPanel.add(saveButton);

        JLabel keyNameLabel = new JLabel(keyName, SwingConstants.CENTER);
        keyNameLabel.setFont(dataFont);
        keyNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel counterLabel = new JLabel("#Items:");
        counterNumber = 0;
        counter = new JLabel(counterNumber + "");
        counterLabel.setFont(dataFont);
        counter.setFont(dataFont);

        JPanel counterPanel = new JPanel();
        counterPanel.setLayout(new BoxLayout(counterPanel, BoxLayout.X_AXIS));
        counterPanel.add(counterLabel);
        counterPanel.add(boxUtil.createFiller(5));
        counterPanel.add(counter);

        JPanel viewAndCounterPanel = new JPanel();
        viewAndCounterPanel.setLayout(new BoxLayout(viewAndCounterPanel, BoxLayout.Y_AXIS));
        viewAndCounterPanel.setPreferredSize(boxUtil.createDimension(180, 230));
        viewAndCounterPanel.add(keyNameLabel);
        viewAndCounterPanel.add(dataViewPanel);
        viewAndCounterPanel.add(counterPanel);

        dataFilterPanel.setPreferredSize(boxUtil.createDimension(180, 230));

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.X_AXIS));
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(editPanel);
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(viewAndCounterPanel);
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(plusMinusButtonPanel);
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(dataFilterPanel);
        middleContainer.add(boxUtil.createFiller(30));

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(boxUtil.createFiller(30));
        add(middleContainer);
        add(boxUtil.createFiller(30));

        dataViewMenuBar.setFont(dataFont);
        setJMenuBar(dataViewMenuBar);

        addWindowListener(windowListener);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Sets the number of items counter
     * @param newCounter new number of items
     */
    public void setCounter(int newCounter){
        counterNumber = newCounter;
        counter.setText(counterNumber + "");
    }

    /**
     * Adds one to the items counter.
     */
    public void upCounter(){
        counterNumber++;
        counter.setText(counterNumber + "");
    }

    /**
     * Subtracts one from the items counter.
     */
    public void downCounter(){
        counterNumber--;
        counter.setText(counterNumber + "");
    }
}
