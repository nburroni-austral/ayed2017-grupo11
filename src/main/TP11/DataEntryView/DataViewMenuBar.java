package main.TP11.DataEntryView;

import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * JMenuBar for the DataViewFrame, containing new file, load file, generate index file and compact menu options.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataViewMenuBar extends JMenuBar {
    public DataViewMenuBar(ActionListener newFileAL, ActionListener loadFileAl, ActionListener generateIndexFileAL, ActionListener compactAL) {
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        Font dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 12));
        JMenu file = new JMenu("File");
        file.setFont(dataFont);
        JMenuItem newFile = new JMenuItem("New");
        newFile.addActionListener(newFileAL);
        JMenuItem loadFile = new JMenuItem("Load");
        loadFile.addActionListener(loadFileAl);
        JMenuItem generateIndexFile = new JMenuItem("Generate index file");
        generateIndexFile.addActionListener(generateIndexFileAL);
        JMenuItem compact = new JMenuItem("Compact");
        compact.addActionListener(compactAL);
        file.add(newFile);
        file.add(loadFile);
        file.add(generateIndexFile);
        file.add(compact);
        newFile.setFont(dataFont);
        loadFile.setFont(dataFont);
        generateIndexFile.setFont(dataFont);
        compact.setFont(dataFont);
        add(file);
    }

}
