package main.TP11.DataEntryView;

import main.TP11.DataEntryLogic.Attribute;
import main.TP11.DataEntryLogic.TypeEnum;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * JPanel for filtering UserType data, receives a list of attributes and creates the JComponents necessary.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataFilterPanel extends JPanel{
    private JComboBox<String> attributeComboBox;
    private JComboBox<String> filterComboBox;
    private JFormattedTextField filterTextField;
    private JPanel filterPanel;
    private JPanel buttonPanel;
    private final double systemScaling = ScalingUtil.getSystemScaling();
    private DefaultFormatterFactory integerFormatter;
    private DefaultFormatterFactory decimalFormatter;
    private DefaultFormatterFactory booleanFormatter;
    private TypeEnum filterType;
    private ArrayList<Attribute> attributes;
    private JButton filterButton;
    private JButton resetFilterButton;

    public DataFilterPanel(ArrayList<Attribute> attributes, ActionListener filterButtonAL, ActionListener resetFilterAL, ActionListener fieldComboBoxAL){
        super();
        this.attributes = attributes;
        Font dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 13));

        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setGroupingUsed(false);
        NumberFormatter nff = new NumberFormatter(nf);
        integerFormatter = new DefaultFormatterFactory(nff);

        DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(false);
        NumberFormatter dnff = new NumberFormatter(df);
        decimalFormatter = new DefaultFormatterFactory(dnff);

        NumberFormat bf = NumberFormat.getIntegerInstance();
        NumberFormatter bnff = new NumberFormatter(bf);
        bnff.setMinimum(0);
        bnff.setMaximum(1);
        booleanFormatter = new DefaultFormatterFactory(bnff);


        attributeComboBox = new JComboBox<>();
        attributeComboBox.setFont(dataFont);
        attributes.forEach(e -> attributeComboBox.addItem(e.getName()));
        attributeComboBox.addActionListener(fieldComboBoxAL);

        String[] filters = {"=", "<", ">"};
        filterComboBox = new JComboBox<>(filters);
        filterComboBox.setFont(dataFont);
        filterTextField = new JFormattedTextField();
        filterTextField.setFont(dataFont);

        filterPanel = new JPanel();
        filterPanel.setLayout(new BoxLayout(filterPanel, BoxLayout.X_AXIS));
        filterPanel.add(filterComboBox);
        filterPanel.add(boxUtil.createFiller(15));
        filterPanel.add(filterTextField);

        filterButton = boxUtil.createButton("Filter", 60, 30);
        filterButton.addActionListener(filterButtonAL);

        resetFilterButton = boxUtil.createButton("Reset", 60, 30);
        resetFilterButton.addActionListener(resetFilterAL);

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(filterButton);
        buttonPanel.add(boxUtil.createFiller(30));
        buttonPanel.add(resetFilterButton);
        buttonPanel.setAlignmentX(CENTER_ALIGNMENT);

        setIntegerFilterOption();

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(attributeComboBox);
        add(boxUtil.createFiller(30));
        add(filterPanel);
        add(boxUtil.createFiller(30));
        add(buttonPanel);
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        Dimension filterPanelDim = new Dimension(preferredSize.getSize().width, (int) (30 * systemScaling));
        filterPanel.setPreferredSize(filterPanelDim);
        filterPanel.setMinimumSize(filterPanelDim);
        filterPanel.setMaximumSize(filterPanelDim);

        attributeComboBox.setPreferredSize(filterPanelDim);
        attributeComboBox.setMinimumSize(filterPanelDim);
        attributeComboBox.setMaximumSize(filterPanelDim);

        Dimension buttonPanelDim = new Dimension(preferredSize.getSize().width, (int) (30 * systemScaling));
        buttonPanel.setPreferredSize(buttonPanelDim);
        buttonPanel.setMaximumSize(buttonPanelDim);
        buttonPanel.setMinimumSize(buttonPanelDim);

        Dimension buttonDim = new Dimension((preferredSize.getSize().width - 50) / 2, (int) (30 * systemScaling));
        filterButton.setPreferredSize(buttonDim);
        filterButton.setMaximumSize(buttonDim);
        filterButton.setMinimumSize(buttonDim);
        resetFilterButton.setPreferredSize(buttonDim);
        resetFilterButton.setMaximumSize(buttonDim);
        resetFilterButton.setMinimumSize(buttonDim);
    }

    private void setStringFilterOption(){
        filterType = TypeEnum.STRING_TYPE;
        filterComboBox.setEnabled(true);
        filterTextField.setFormatterFactory(null);
        filterTextField.setText("");
    }

    private void setIntegerFilterOption(){
        filterType = TypeEnum.INT_TYPE;
        filterComboBox.setEnabled(true);
        filterTextField.setFormatterFactory(integerFormatter);
        filterTextField.setText("");
    }

    private void setDoubleFilterOption(){
        filterType = TypeEnum.DOUBLE_TYPE;
        filterComboBox.setEnabled(true);
        filterTextField.setFormatterFactory(decimalFormatter);
        filterTextField.setText("");
    }

    private void setBooleanFilterOption(){
        filterType = TypeEnum.BOOLEAN_TYPE;
        filterComboBox.setSelectedIndex(0);
        filterComboBox.setEnabled(false);
        filterTextField.setFormatterFactory(booleanFormatter);
        filterTextField.setText("");
    }

    /**
     * Returns the filter command from the panel.
     * 0: Equals command
     * 1: Greater than command
     * -1: Less than command
     * @return filterCommand
     */
    public int getFilterCommand(){
        switch (filterComboBox.getSelectedIndex()){
            case 0:
                return 0;
            case 1:
                return -1;
            case 2:
                return 1;
        }
        throw new IndexOutOfBoundsException();
    }

    public String getFilterValue(){
        return filterTextField.getText();
    }

    public TypeEnum getFilterType(){
        return filterType;
    }

    public String getAttFilterName(){
        return (String) attributeComboBox.getSelectedItem();
    }

    /**
     * Sets the necessary format in the JTextField filter area for the currently selected attribute to filter.
     */
    public void setFilterOption(){
        int selectedIndex = attributeComboBox.getSelectedIndex();
        Attribute selectedAttribute = attributes.get(selectedIndex);
        switch (selectedAttribute.geteType()){
            case STRING_TYPE:
                setStringFilterOption();
                break;
            case INT_TYPE:
                setIntegerFilterOption();
                break;
            case DOUBLE_TYPE:
                setDoubleFilterOption();
                break;
            case BOOLEAN_TYPE:
                setBooleanFilterOption();
                break;
        }
    }
}
