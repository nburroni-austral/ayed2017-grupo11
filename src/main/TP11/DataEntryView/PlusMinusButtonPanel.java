package main.TP11.DataEntryView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * JPanel containing "+" (plus) and "-" (minus) buttons.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class PlusMinusButtonPanel extends JPanel{
    public PlusMinusButtonPanel(ActionListener plusAL, ActionListener minusAL) {
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);

        JButton plusButton = boxUtil.createButton("", 40, 40);
        JButton minusButton = boxUtil.createButton("", 40, 40);
        try {
            BufferedImage plusImage = ImageIO.read(new File("./src/main/TP11/Icons/plusIcon.png"));
            Icon plusIcon = new ImageIcon(plusImage);
            plusButton.setIcon(plusIcon);

            BufferedImage minusImage = ImageIO.read(new File("./src/main/TP11/Icons/minusIcon.png"));
            Icon minusIcon = new ImageIcon(minusImage);
            minusButton.setIcon(minusIcon);
        } catch (IOException exc){
            plusButton.setText("+");
            plusButton.setText("-");
        }


        plusButton.addActionListener(plusAL);
        minusButton.addActionListener(minusAL);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(boxUtil.createFiller(30));
        add(plusButton);
        add(boxUtil.createFiller(30));
        add(minusButton);
        add(boxUtil.createFiller(30));
    }
}
