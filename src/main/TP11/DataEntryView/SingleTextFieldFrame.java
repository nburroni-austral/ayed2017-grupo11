package main.TP11.DataEntryView;

import main.TP11.DataEntryLogic.DocumentSizeFilter;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * JFrame containing a single text field, an "OK" button, a "cancel" button and a warning label.
 * The text field can be formatted as desired, with the Format enum.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class SingleTextFieldFrame extends JFrame{
    private JFormattedTextField textField;
    private JLabel warningLabel;

    public SingleTextFieldFrame(String frameName, String indication, ActionListener okButtonAL, ActionListener backButtonAL, Format format, int limit){
        super(frameName);
        double systemScaling = ScalingUtil.getSystemScaling();
        Font font = new Font("Path", Font.PLAIN, (int) (14 * systemScaling));

        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setParseIntegerOnly(true);
        switch (format){
            case LIMITED_INTEGER:
                nf.setMaximumIntegerDigits(limit);
            case INTEGER:
                textField = new JFormattedTextField(nf);
                break;
            case LIMITED_STRING:
                textField = new JFormattedTextField();
                AbstractDocument doc = (AbstractDocument) textField.getDocument();
                doc.setDocumentFilter(new DocumentSizeFilter(limit));
                break;
            case STRING:
                textField = new JFormattedTextField();
                break;
        }

        textField.setFont(font);

        JLabel enterPath = new JLabel(indication);
        enterPath.setAlignmentX(Component.CENTER_ALIGNMENT);
        enterPath.setFont(font);

        warningLabel = new JLabel("temp text", SwingConstants.CENTER);
        warningLabel.setFont(font);
        warningLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        hideWarningLabel();

        LayoutUtil boxUtil = new LayoutUtil(systemScaling);

        JButton okButton = boxUtil.createButton("OK", 80, 25);
        okButton.addActionListener(okButtonAL);
        JButton backButton = boxUtil.createButton("Back", 80, 25);
        backButton.addActionListener(backButtonAL);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(boxUtil.createFiller(25));
        buttonPanel.add(okButton);
        buttonPanel.add(boxUtil.createFiller(25));
        buttonPanel.add(backButton);
        buttonPanel.add(boxUtil.createFiller(25));

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(enterPath);
        add(textField);
        add(warningLabel);
        add(buttonPanel);

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        pack();
    }

    /**
     * @return text in the textField
     */
    public String getText(){
        return textField.getText();
    }

    /**
     * Hides the warning label
     */
    public void hideWarningLabel(){
        warningLabel.setForeground(getBackground());
    }

    /**
     * Shows a warning label with the given text
     * @param text text to be displayed
     */
    public void setAndShowWarningLabel(String text){
        warningLabel.setText(text);
        warningLabel.setForeground(Color.RED);
    }

    public enum Format{
        STRING,
        INTEGER,
        LIMITED_STRING,
        LIMITED_INTEGER
    }
}
