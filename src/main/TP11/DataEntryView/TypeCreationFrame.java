package main.TP11.DataEntryView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;

/**
 * JFrame containing a TypeCreationPanel for creating a UserType list of attributes.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class TypeCreationFrame extends JFrame{
    public TypeCreationFrame(TypeCreationPanel typeCreationPanel){
        super("New Data Type");
        LayoutUtil boxUtil = new LayoutUtil(ScalingUtil.getSystemScaling());

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.X_AXIS));
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(typeCreationPanel);
        middleContainer.add(boxUtil.createFiller(30));

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(boxUtil.createFiller(30));
        add(middleContainer);
        add(boxUtil.createFiller(30));

        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
