package main.TP11.DataEntryView;

import main.TP11.DataEntryLogic.Attribute;
import main.TP11.DataEntryLogic.DocumentSizeFilter;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import main.Utils.SwingUtil.SimpleCheckboxStyle;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.text.NumberFormat;
import java.util.ArrayList;


/**
 * JPanel for editing UserType data, receives a list of attributes and creates the JComponents necessary.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataEditPanel extends JPanel{

    private NumberFormat intFormat;
    private NumberFormat doubleFormat;
    private ArrayList<JTextArea> stringFields;
    private ArrayList<JFormattedTextField> intFields;
    private ArrayList<JFormattedTextField> doubleFields;
    private ArrayList<JCheckBox> checkBoxes;
    private Dimension numberPanelSize;
    private LayoutUtil boxUtil;
    private boolean insertingKey;
    private final int PANEL_WIDTH;
    private final int PANEL_HEIGHT;
    private Font dataFont;
    private double systemScaling;

    public DataEditPanel(ArrayList<Attribute> attributes){
        super();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        systemScaling = ScalingUtil.getSystemScaling();
        dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 13));
        boxUtil = new LayoutUtil(systemScaling);
        stringFields = new ArrayList<>();
        intFields = new ArrayList<>();
        doubleFields = new ArrayList<>();
        checkBoxes = new ArrayList<>();
        insertingKey = true;
        PANEL_WIDTH = 285;
        PANEL_HEIGHT = 25;
        numberPanelSize = boxUtil.createDimension(PANEL_WIDTH, PANEL_HEIGHT);

        intFormat = NumberFormat.getInstance();
        intFormat.setParseIntegerOnly(true);
        intFormat.setGroupingUsed(false);

        doubleFormat = NumberFormat.getInstance();
        doubleFormat.setParseIntegerOnly(false);
        doubleFormat.setGroupingUsed(false);

        Dimension labelDim = boxUtil.createDimension(90,25);
        for(Attribute attribute: attributes){
            String name = attribute.getName();
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            JLabel label = new JLabel(name, SwingConstants.CENTER);
            label.setFont(dataFont);
            label.setMinimumSize(labelDim);
            label.setMaximumSize(labelDim);
            label.setPreferredSize(labelDim);
            panel.add(label);
            panel.add(boxUtil.createFiller(5));
            switch (attribute.geteType()){
                case STRING_TYPE:
                    addStringField(panel, attribute.getSize());
                    break;
                case INT_TYPE:
                    addIntField(panel);
                    setNumberPanelSize(panel);
                    break;
                case BOOLEAN_TYPE:
                    addBooleanField(panel);
                    setNumberPanelSize(panel);
                    break;
                case DOUBLE_TYPE:
                    addDoubleField(panel);
                    setNumberPanelSize(panel);
                    break;
            }
            add(panel);
        }
    }

    private void addStringField(JPanel panel, int length){
        JTextArea textArea = new JTextArea();
        textArea.setFont(dataFont);
        AbstractDocument doc = (AbstractDocument) textArea.getDocument();
        doc.setDocumentFilter(new DocumentSizeFilter(length));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        JScrollPane textScroll = new JScrollPane(textArea);
        setTextPanelSize(panel, length);
        panel.add(textScroll);
        stringFields.add(textArea);
    }

    private void addIntField(JPanel panel){
        JFormattedTextField textField = new JFormattedTextField(intFormat);
        textField.setFont(dataFont);
        textField.setText("0");
        panel.add(textField);
        intFields.add(textField);
        if(insertingKey){
            textField.setEditable(false);
            insertingKey = false;
        }
    }

    private void addBooleanField(JPanel panel){
        JCheckBox checkBox = new JCheckBox();
        checkBox.setIcon(new SimpleCheckboxStyle((int) (15 * systemScaling)));
        panel.add(checkBox);
        checkBoxes.add(checkBox);
    }

    private void addDoubleField(JPanel panel){
        JFormattedTextField textField = new JFormattedTextField(doubleFormat);
        textField.setFont(dataFont);
        textField.setText("0.0");
        panel.add(textField);
        doubleFields.add(textField);
    }

    private void setNumberPanelSize(JPanel panel){
        panel.setPreferredSize(numberPanelSize);
        panel.setMaximumSize(numberPanelSize);
        panel.setMinimumSize(numberPanelSize);
    }

    private void setTextPanelSize(JPanel panel, int length){
        int height = PANEL_HEIGHT * Math.min(length/100 + 1, 5);
        Dimension textPanelDim = boxUtil.createDimension(PANEL_WIDTH, height);
        panel.setPreferredSize(textPanelDim);
        panel.setMinimumSize(textPanelDim);
        panel.setMaximumSize(textPanelDim);
    }

    public ArrayList<String> getStringFields(){
        ArrayList<String> result = new ArrayList<>();
        stringFields.forEach(field -> result.add(field.getText()));
        return result;
    }

    public ArrayList<Integer> getIntFields(){
        ArrayList<Integer> result = new ArrayList<>();
        intFields.forEach(field -> result.add(Integer.valueOf(field.getText())));
        return result;
    }

    public ArrayList<Double> getDoubleFields(){
        ArrayList<Double> result = new ArrayList<>();
        doubleFields.forEach(field -> result.add(Double.valueOf(field.getText())));
        return result;
    }

    public ArrayList<Boolean> getBooleanFields(){
        ArrayList<Boolean> result = new ArrayList<>();
        checkBoxes.forEach(button -> result.add(button.isSelected()));
        return result;
    }

    public void setStringFields(ArrayList<String> strings){
        for(int i = 0; i < strings.size(); i++){
            stringFields.get(i).setText(strings.get(i));
        }
    }
    public void setIntFields(ArrayList<Integer> integers){
        for(int i = 0; i < integers.size(); i++){
            intFields.get(i).setText(integers.get(i) + "");
        }
    }
    public void setDoubleFields(ArrayList<Double> doubles){
        for(int i = 0; i < doubles.size(); i++){
            doubleFields.get(i).setText(doubles.get(i) + "");
        }
    }
    public void setBooleanFields(ArrayList<Boolean> booleans){
        for(int i = 0; i < booleans.size(); i++){
            checkBoxes.get(i).setSelected(booleans.get(i));
        }
    }
}
