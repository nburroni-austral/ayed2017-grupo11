package main.TP11.DataEntryView;

import main.TP11.DataEntryLogic.DocumentSizeFilter;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AbstractDocument;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * JPanel for creating a UserType list of attributes
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class TypeCreationPanel extends JPanel{

    private JTable dataTable;
    private DefaultTableModel tableModel;
    private JTextField dataNameEntry;
    private JComboBox<String> dataTypeEntry;
    private JFormattedTextField lengthField;
    private final List<String> dataTypes = Arrays.asList("Text", "Integer", "Decimal", "Boolean");
    private JLabel warningLabel;

    public TypeCreationPanel(ActionListener comboBoxAL, PlusMinusButtonPanel plusMinusButtonPanel, ActionListener doneBtnAL,  ActionListener okBtnAL, ActionListener cancelBtnAL, ListSelectionListener listListener){
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);

        Font dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 13));


        Object[] columnNames = {"Field Name", "Field Type", "Length"};
        tableModel = new DefaultTableModel(0, 3){
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        tableModel.setColumnIdentifiers(columnNames);
        Object[] keyRow = {"Key", "Integer", "-"};
        tableModel.addRow(keyRow);
        dataTable = new JTable(tableModel);
        dataTable.getSelectionModel().addListSelectionListener(listListener);
        dataTable.setFont(dataFont);
        dataTable.getTableHeader().setFont(dataFont);
        dataTable.getColumnModel().getColumn(0).setMinWidth((int) (systemScaling * 130));
        dataTable.setRowHeight((int) (systemScaling * 15));

        JScrollPane scrollPane = new JScrollPane(dataTable);
        scrollPane.setPreferredSize(new Dimension(250, 250));

        dataNameEntry = new JTextField();
        Dimension dataNameDim = boxUtil.createDimension(150, 30);
        dataNameEntry.setMinimumSize(dataNameDim);
        dataNameEntry.setPreferredSize(dataNameDim);
        dataNameEntry.setMaximumSize(dataNameDim);
        dataTypeEntry = new JComboBox<>();
        dataTypes.forEach(dataTypeEntry::addItem);
        dataTypeEntry.setFont(dataFont);
        dataTypeEntry.addActionListener(comboBoxAL);
        dataNameEntry.setFont(dataFont);
        AbstractDocument dataNameEntryDoc = (AbstractDocument) dataNameEntry.getDocument();
        dataNameEntryDoc.setDocumentFilter(new DocumentSizeFilter(20));


        JPanel listPanel = new JPanel();
        listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.X_AXIS));
        listPanel.add(scrollPane);
        listPanel.add(boxUtil.createFiller(30));
        listPanel.add(plusMinusButtonPanel);

        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        NumberFormatter intFormatter = new NumberFormatter(nf);
        intFormatter.setMinimum(1);
        intFormatter.setMaximum(10000);
        lengthField = new JFormattedTextField(intFormatter);
        Dimension lengthFieldDimension = boxUtil.createDimension(50, 30);
        lengthField.setMinimumSize(lengthFieldDimension);
        lengthField.setPreferredSize(lengthFieldDimension);
        lengthField.setMaximumSize(lengthFieldDimension);

        JButton okButton = boxUtil.createButton("OK", 50, 30);
        okButton.setFont(dataFont);
        okButton.addActionListener(okBtnAL);

        JPanel dataEntryPanel = new JPanel();
        dataEntryPanel.setLayout(new BoxLayout(dataEntryPanel, BoxLayout.X_AXIS));
        dataEntryPanel.add(dataNameEntry);
        dataEntryPanel.add(boxUtil.createFiller(15));
        dataEntryPanel.add(dataTypeEntry);
        dataEntryPanel.add(boxUtil.createFiller(15));
        dataEntryPanel.add(lengthField);
        dataEntryPanel.add(boxUtil.createFiller(15));
        dataEntryPanel.add(okButton);

        warningLabel = new JLabel("temp value");
        warningLabel.setFont(dataFont);
        warningLabel.setAlignmentX(CENTER_ALIGNMENT);
        hideWarningLabel();

        JButton doneButton = boxUtil.createButton("Done", 100, 30);
        doneButton.addActionListener(doneBtnAL);
        JButton cancelButton = boxUtil.createButton("Cancel", 100, 30);
        cancelButton.addActionListener(cancelBtnAL);

        JPanel doneCancelBtnPanel = new JPanel();
        doneCancelBtnPanel.setLayout(new BoxLayout(doneCancelBtnPanel, BoxLayout.X_AXIS));
        doneCancelBtnPanel.add(doneButton);
        doneCancelBtnPanel.add(boxUtil.createFiller(30));
        doneCancelBtnPanel.add(cancelButton);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(listPanel);
        add(boxUtil.createFiller(30));
        add(dataEntryPanel);
        add(doneCancelBtnPanel);
        add(boxUtil.createFiller(15));
        add(warningLabel);
        add(boxUtil.createFiller(15));
        add(doneCancelBtnPanel);
    }

    /**
     * Adds a new attribute row
     */
    public void addField(){
        Object[] row = {"Field Name", "Integer", "-"};
        tableModel.addRow(row);
    }

    /**
     * Removes the field selected on the table
     */
    public void removeSelectedField(){
        int selectedIndex = dataTable.getSelectedRow();
        if(selectedIndex > 0) {
            tableModel.removeRow(dataTable.getSelectedRow());
        }
    }

    /**
     * Takes the data in the selected row and inserts it in the editing panel
     */
    public void startEditingSelectedRow(){
        int selectedIndex = dataTable.getSelectedRow();
        if(selectedIndex >= 0) {
            dataNameEntry.setText((String) tableModel.getValueAt(selectedIndex, 0));
            lengthField.setText((String) tableModel.getValueAt(selectedIndex, 2));
            if(selectedIndex == 0){
                dataTypeEntry.setSelectedIndex(dataTypes.indexOf("Integer"));
                dataTypeEntry.setEditable(false);
                lengthField.setEditable(false);
            }
            else {
                String dataType = (String) tableModel.getValueAt(selectedIndex, 1);
                dataTypeEntry.setSelectedIndex(dataTypes.indexOf(dataType));
                dataTypeEntry.setEditable(true);
                if(dataType.equals("Text")){
                    lengthField.setEditable(true);
                } else {
                    lengthField.setEditable(false);
                }
            }
        }
    }

    /**
     * Enables the length field
     */
    public void enableLengthField(){
        lengthField.setEditable(true);
    }

    /**
     * Takes the data in the editing panel and inserts it in the selected row
     */
    public void setSelectedRowData(){
        int selectedIndex = dataTable.getSelectedRow();
        if(selectedIndex >= 0) {
            tableModel.setValueAt(dataNameEntry.getText(), selectedIndex, 0);
            tableModel.setValueAt(dataTypeEntry.getSelectedItem(), selectedIndex, 1);
            tableModel.setValueAt(lengthField.getText(), selectedIndex, 2);
        }
    }

    /**
     * Hides the warning label
     */
    public void hideWarningLabel(){
        warningLabel.setForeground(getBackground());
    }

    /**
     * Shows a warning label with the given text
     * @param text text to be displayed
     */
    public void setAndShowWarningLabel(String text){
        warningLabel.setText(text);
        warningLabel.setForeground(Color.RED);
    }

    /**
     * @return data in the attribute table
     */
    public Vector getData(){
        return tableModel.getDataVector();
    }

    /**
     * @return type selected in the type combo box.
     */
    public String getSelectedType(){
        return (String) dataTypeEntry.getSelectedItem();
    }
}
