package main.TP11.DataEntryView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.ArrayList;

/**
 * JPanel containing a list for displaying UserType data
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataViewPanel extends JPanel {
    private JList dataList;
    private DefaultListModel<String> listModel;

    public DataViewPanel(ListSelectionListener listListener){
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        Font dataFont = new Font("Data", Font.PLAIN, (int) (systemScaling * 13));

        listModel = new DefaultListModel<>();
        dataList = new JList<>(listModel);
        dataList.setFont(dataFont);
        dataList.getSelectionModel().addListSelectionListener(listListener);

        JScrollPane scrollPane = new JScrollPane(dataList);
        Dimension scrollPaneDim = boxUtil.createDimension(180, 190);
        scrollPane.setPreferredSize(scrollPaneDim);
        scrollPane.setMinimumSize(scrollPaneDim);
        scrollPane.setMaximumSize(scrollPaneDim);
        add(scrollPane);
    }

    /**
     * @return selected list index
     */
    public int getSelectedIndex(){
        return dataList.getSelectedIndex();
    }

    /**
     * Removes the desired index from the list.
     * @param index index to be removed
     */
    public void removeLine(int index){
        if(index >= 0) listModel.remove(index);
    }

    /**
     * Gets the desired index from the list
     * @param index desired value index
     * @return value at the desired index.
     */
    public String getLine(int index){
        return listModel.get(index);
    }

    /**
     * Sets the list to be displayed
     * @param strings list of new values to be displayed.
     */
    public void setList(ArrayList<String> strings){
        listModel.clear();
        strings.forEach(listModel::addElement);
    }

    /**
     * Adds a value to the list
     * @param line value to be added.
     */
    public void addLine(String line){
        listModel.addElement(line);
    }
}
