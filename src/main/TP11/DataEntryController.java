package main.TP11;

import javafx.util.Pair;
import main.TP11.DataEntryLogic.*;
import main.TP11.DataEntryView.*;
import main.TP11.Exceptions.KeyAlreadyExistsExc;
import main.TP11.Exceptions.RegistryNotFoundException;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Controller for the TypeCreationFrame, DataViewFrame and SingleTextFieldFrame.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class DataEntryController {
    private TypeCreationFrame typeCreationFrame;
    private TypeCreationPanel typeCreationPanel;

    private DataViewFrame dataViewFrame;
    private DataViewPanel dataViewPanel;
    private DataEditPanel dataEditPanel;
    private DataFilterPanel dataFilterPanel;

    private DataViewMenuBar dataViewMenuBar;

    private SingleTextFieldFrame pathFrame;
    private SingleTextFieldFrame newUserTypeKeyFrame;

    private String path;
    private UserTypeFile userTypeFile;
    private UserTypeFactory userTypeFactory;

    private boolean fileOpened;
    private boolean filtered;
    private boolean pathFrameOpened;
    private boolean userKeyFrameOpened;
    private boolean typeCreationFrameOpened;

    public DataEntryController() {
        ArrayList<Attribute> attributes = new ArrayList<>();

        fileOpened = false;
        filtered = false;
        pathFrameOpened = false;
        userKeyFrameOpened = false;
        typeCreationFrameOpened = false;
        dataEditPanel = new DataEditPanel(attributes);
        dataViewPanel = new DataViewPanel(new ViewDataListListener());
        dataFilterPanel = new DataFilterPanel(attributes, new TempAL(), new TempAL(), new TempAL());

        PlusMinusButtonPanel plusMinusButtonsViewFrame = new PlusMinusButtonPanel(new TempAL(), new TempAL());
        dataViewMenuBar = new DataViewMenuBar(new NewFileButtonAL(), new LoadFileButtonAL(), new AutoIndexButtonAL(), new CompactButtonAL());
        dataViewFrame = new DataViewFrame("KeyName", new TempAL(), new WindowCloseListener(), dataViewMenuBar, dataEditPanel, dataViewPanel, plusMinusButtonsViewFrame, dataFilterPanel);
    }

        private class TempAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {}
        }

    //DataViewMenuBar
        private class NewFileButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pathFrameOpened) pathFrame.dispose();
                pathFrame = new SingleTextFieldFrame("New File", "Enter the file name", new OKButtonNewFilePathAL(), new BackButtonFilePathAL(), SingleTextFieldFrame.Format.STRING, 0);
                pathFrameOpened = true;
            }
        }

        private class LoadFileButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pathFrameOpened) pathFrame.dispose();
                pathFrame = new SingleTextFieldFrame("Load File", "Enter the file name", new OKButtonLoadFilePathAL(), new BackButtonFilePathAL(), SingleTextFieldFrame.Format.STRING, 0);
                pathFrameOpened = true;
            }
        }

        private class AutoIndexButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileOpened) {
                    userTypeFile.autoIndex();
                }
            }
        }

        private class CompactButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileOpened){
                    userTypeFile.compact();
                }
            }
        }
    //

    //TypeCreationFrame
        private class OKButtonCreationAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                typeCreationPanel.setSelectedRowData();
            }
        }

        private class CancelButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                typeCreationFrame.dispose();
                typeCreationFrameOpened = false;
            }
        }

        private class PlusButtonTypeCreationAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                typeCreationPanel.addField();
            }
        }

        private class MinusButtonTypeCreationAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                typeCreationPanel.removeSelectedField();
            }
        }

        private class DataEntryListListener implements ListSelectionListener{
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    typeCreationPanel.startEditingSelectedRow();
                }
            }
        }

        private class DoneButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Attribute> attributes = VectorToAttribute.vectorToAttribute(typeCreationPanel.getData());
                    userTypeFactory = new UserTypeFactory(attributes, 0);
                    UserTypeFile prevFile = userTypeFile;
                    userTypeFile = new UserTypeFile(path, attributes, 20, 0);
                    if (fileOpened) prevFile.close();
                    fileOpened = true;
                    typeCreationFrame.dispose();
                    typeCreationFrameOpened = false;
                    setUpGUI();
                } catch (NumberFormatException exc){
                    typeCreationPanel.setAndShowWarningLabel("All text fields should have a length");
                }
            }
        }

    private void setUpGUI() {
        dataViewFrame.dispose();
        dataEditPanel = new DataEditPanel(userTypeFile.getAttributes());
        dataViewPanel = new DataViewPanel(new ViewDataListListener());
        dataFilterPanel = new DataFilterPanel(userTypeFile.getAttributes(), new FilterButtonAL(), new ResetFilterAL(), new FilterFieldComboBoxAL());
        PlusMinusButtonPanel plusMinusButtonsViewFrame = new PlusMinusButtonPanel(new PlusButtonViewFrameAL(), new MinusButtonViewFrameAl());
        dataViewFrame = new DataViewFrame(userTypeFactory.getKeyName(), new SaveButtonAL(), new WindowCloseListener(), dataViewMenuBar, dataEditPanel, dataViewPanel, plusMinusButtonsViewFrame, dataFilterPanel);
        filtered = true;
        resetList();
    }

    private void resetList(){
        if(filtered) {
            ArrayList<String> keys = userTypeFile.getReport().stream().map(UserType::getCode).map(code -> code + "").collect(Collectors.toCollection(ArrayList::new));
            dataViewPanel.setList(keys);
            dataViewFrame.setCounter(keys.size());
            filtered = false;
        }
    }

        private class DataTypeComboBoxAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(typeCreationPanel.getSelectedType().equalsIgnoreCase("text")){
                    typeCreationPanel.enableLengthField();
                }
            }
        }
    //

    //FilePathFrame
        private class OKButtonNewFilePathAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                path = pathFrame.getText();
                pathFrame.dispose();
                pathFrameOpened = false;
                typeCreationPanel = new TypeCreationPanel(new DataTypeComboBoxAL(), new PlusMinusButtonPanel(new PlusButtonTypeCreationAL(), new MinusButtonTypeCreationAL()), new DoneButtonAL(), new OKButtonCreationAL(), new CancelButtonAL(), new DataEntryListListener());
                if(typeCreationFrameOpened) typeCreationFrame.dispose();
                typeCreationFrame = new TypeCreationFrame(typeCreationPanel);
                typeCreationFrameOpened = true;
            }
        }

        private class OKButtonLoadFilePathAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                path = pathFrame.getText();
                try {
                    UserTypeFile prevFile = userTypeFile;
                    userTypeFile = new UserTypeFile(path);
                    if(fileOpened) prevFile.close();
                    userTypeFactory = new UserTypeFactory(userTypeFile.getAttributes(), 0);
                    fileOpened = true;
                    pathFrame.dispose();
                    pathFrameOpened = false;
                    setUpGUI();
                } catch (FileNotFoundException exc){
                    pathFrame.setAndShowWarningLabel("The file does not exist");
                }
            }
        }

        private class BackButtonFilePathAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                pathFrame.dispose();
                pathFrameOpened = false;
            }
        }
    //

    //DataViewFrame
        private class WindowCloseListener implements WindowListener{
            @Override
            public void windowOpened(WindowEvent e) {}

            @Override
            public void windowClosing(WindowEvent e) {
                if(fileOpened) userTypeFile.close();
            }

            @Override
            public void windowClosed(WindowEvent e) {}

            @Override
            public void windowIconified(WindowEvent e) {}

            @Override
            public void windowDeiconified(WindowEvent e) {}

            @Override
            public void windowActivated(WindowEvent e) {}

            @Override
            public void windowDeactivated(WindowEvent e) {}
        }

        private class SaveButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    UserType updatedUserType = userTypeFactory.createFullUserType(
                            dataEditPanel.getStringFields(),
                            dataEditPanel.getIntFields(),
                            dataEditPanel.getDoubleFields(),
                            dataEditPanel.getBooleanFields());
                    userTypeFile.updateRegistry(updatedUserType);
                } catch (RegistryNotFoundException exc){
                    exc.printStackTrace();
                }
            }
        }

        private class PlusButtonViewFrameAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if(userKeyFrameOpened) newUserTypeKeyFrame.dispose();
                newUserTypeKeyFrame = new SingleTextFieldFrame("New Data Entry", "Enter the " + userTypeFactory.getKeyName(), new OKButtonNewUserTypeAL(), new CancelButtonNewUserTypeAL(), SingleTextFieldFrame.Format.INTEGER, 0);
                userKeyFrameOpened = true;
            }
        }

        private class MinusButtonViewFrameAl implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = dataViewPanel.getSelectedIndex();
                if(selectedIndex >= 0){
                    Integer key = Integer.valueOf(dataViewPanel.getLine(selectedIndex));
                    dataViewPanel.removeLine(selectedIndex);
                    userTypeFile.delete(key);
                    dataViewFrame.downCounter();
                }
            }
        }

        private class ViewDataListListener implements ListSelectionListener{
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    int selectedIndex = dataViewPanel.getSelectedIndex();
                    if(selectedIndex >= 0) {
                        Integer key = Integer.valueOf(dataViewPanel.getLine(selectedIndex));
                        UserType selected = userTypeFile.search(key);
                        ArrayList<String> strings = selected.getStringArray().stream().map(Pair::getValue).collect(Collectors.toCollection(ArrayList::new));
                        ArrayList<Integer> integers = selected.getIntegerArray().stream().map(Pair::getValue).collect(Collectors.toCollection(ArrayList::new));
                        ArrayList<Double> doubles = selected.getDoubleArray().stream().map(Pair::getValue).collect(Collectors.toCollection(ArrayList::new));
                        ArrayList<Boolean> booleans = selected.getBooleanArray().stream().map(Pair::getValue).collect(Collectors.toCollection(ArrayList::new));
                        dataEditPanel.setStringFields(strings);
                        dataEditPanel.setIntFields(integers);
                        dataEditPanel.setDoubleFields(doubles);
                        dataEditPanel.setBooleanFields(booleans);
                    }
                }
            }
        }

        private class FilterButtonAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                String comparable = dataFilterPanel.getFilterValue();
                if(!comparable.equalsIgnoreCase("")) {
                    int value = dataFilterPanel.getFilterCommand();
                    String attName = dataFilterPanel.getAttFilterName();
                    TypeEnum filterType = dataFilterPanel.getFilterType();
                    ArrayList<UserType> userTypes = userTypeFile.getByCondition(IntValueToPredicateFilter.intValueToPredicate(value, filterType, attName, comparable));
                    ArrayList<String> keys = userTypes.stream().map(UserType::getCode).map(code -> code + "").collect(Collectors.toCollection(ArrayList::new));
                    dataViewPanel.setList(keys);
                    dataViewFrame.setCounter(keys.size());
                    filtered = true;
                }
            }
        }

        private class ResetFilterAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                resetList();
            }
        }

        private class FilterFieldComboBoxAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                dataFilterPanel.setFilterOption();
            }
        }
    //

    //NewUserTypeFrame
        private class OKButtonNewUserTypeAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    Integer key = Integer.valueOf(newUserTypeKeyFrame.getText());
                    userTypeFile.writeRegistry(userTypeFactory.createBaseUserType(key));
                    dataViewPanel.addLine(key + "");
                    dataViewFrame.upCounter();
                    newUserTypeKeyFrame.dispose();
                    userKeyFrameOpened = false;
                } catch (KeyAlreadyExistsExc exc){
                    newUserTypeKeyFrame.setAndShowWarningLabel(exc.getMessage());
                } catch (NumberFormatException exc){
                    newUserTypeKeyFrame.setAndShowWarningLabel("Please enter an integer");
                }
            }
        }

        private class CancelButtonNewUserTypeAL implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                newUserTypeKeyFrame.dispose();
                userKeyFrameOpened = false;
            }
        }
    //
}
