package main.TP11.DataEntryLogic;

import java.util.function.Predicate;

/**
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class IntValueToPredicateFilter {
    public static Predicate<UserType> intValueToPredicate(int value, TypeEnum attType, String attName, String comparable){
        switch (attType){
            case STRING_TYPE:
                return stringPredicate(value, attName, comparable);
            case INT_TYPE:
                return intPredicate(value, attName, Integer.valueOf(comparable));
            case DOUBLE_TYPE:
                return doublePredicate(value, attName, Double.valueOf(comparable));
            case BOOLEAN_TYPE:
                return booleanPredicate(attName, comparable.equals("1"));
        }
        throw new RuntimeException("Invalid Type");
    }

    private static Predicate<UserType> stringPredicate(int value, String attName, String comparable){
        switch (value){
            case 0:
                return userType -> userType.compareStringAtt(attName, comparable) == 0;
            case 1:
                return userType -> userType.compareStringAtt(attName, comparable) > 0;
            case -1:
                return userType -> userType.compareStringAtt(attName, comparable) < 0;
        }
        throw new RuntimeException("Invalid value");
    }

    private static Predicate<UserType> intPredicate(int value, String attName, Integer comparable){
        switch (value){
            case 0:
                return userType -> userType.compareIntAtt(attName, comparable) == 0;
            case 1:
                return userType -> userType.compareIntAtt(attName, comparable) > 0;
            case -1:
                return userType -> userType.compareIntAtt(attName, comparable) < 0;
        }
        throw new RuntimeException("Invalid value");
    }

    private static Predicate<UserType> doublePredicate(int value, String attName, Double comparable){
        switch (value){
            case 0:
                return userType -> userType.compareDoubleAtt(attName, comparable) == 0;
            case 1:
                return userType -> userType.compareDoubleAtt(attName, comparable) > 0;
            case -1:
                return userType -> userType.compareDoubleAtt(attName, comparable) < 0;
        }
        throw new RuntimeException("Invalid value");
    }

    private static Predicate<UserType> booleanPredicate(String attName, Boolean comparable){
        if(comparable){
            return userType -> userType.getBooleanAtt(attName);
        } else {
            return userType -> !userType.getBooleanAtt(attName);
        }
    }
}
