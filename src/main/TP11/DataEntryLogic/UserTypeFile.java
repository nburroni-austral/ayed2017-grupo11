package main.TP11.DataEntryLogic;

import javafx.util.Pair;
import main.TP11.Exceptions.FileNotCompatibleException;
import main.TP11.Exceptions.KeyAlreadyExistsExc;
import main.TP11.Exceptions.RegistryNotFoundException;
import main.Utils.ObjectUtil.ObjectSaver;
import struct.impl.SearchBinaryTree;

import java.io.*;
import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * Manages a binary file for a UserType system
 * @author Manuel Pedrozo
 * @author Tomas Peres Molina
 */
public class UserTypeFile {

    private IndexFileStrategy indexFileStrategy;
    private File f;
    private RandomAccessFile raf;
    private int attributeNameSize;
    private ArrayList<Attribute> attributes;
    private int codePosition;
    private int registrySize;
    private int configSize;

    public UserTypeFile(String name, ArrayList<Attribute> attributes, int ans, int codePosition) {
        this.indexFileStrategy = new NoIndexFileStrategy();
        configSize = 0;
        registrySize = 0;
        attributeNameSize = ans;
        this.codePosition = codePosition;
        f = new File("./src/main/TP11/Files/" + name);
        try {
            //f.createNewFile();
            raf = new RandomAccessFile(f, "rw");
            if(f.exists()) {
                raf.setLength(0);
            }
            setUpFile(attributes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UserTypeFile(String name) throws FileNotFoundException {
        f = new File("./src/main/TP11/Files/" + name);
        File index = new File("./src/main/TP11/Files/Sbt" + name + ".ser");
        if (index.exists()){
            ObjectSaver objectSaver = new ObjectSaver();
            try {
                indexFileStrategy = new SbtIndexFileStrategy((SearchBinaryTree<KeyPositionPair>) objectSaver.readObject("./src/main/TP11/Files/Sbt" + name));
            }
            catch (Exception e){
                e.printStackTrace();
                this.indexFileStrategy = new NoIndexFileStrategy();
            }
        }
        else {
            this.indexFileStrategy = new NoIndexFileStrategy();
        }
        if(f.exists()) {
            raf = new RandomAccessFile(f, "rw");
            loadFile();
        }
        else{
            throw new FileNotFoundException();
        }
    }

    private void loadFile(){
        try {
            if(raf.length() == 0)
                throw new FileNotCompatibleException();
            goStart();
            attributes = new ArrayList<>();
            int atributesAmount = raf.readInt();
            attributeNameSize = raf.readInt();
            codePosition = raf.readInt();
            configSize += 12;
            configSize += (atributesAmount) * (attributeNameSize*2 +4);
            readString(attributeNameSize);
            raf.readInt();
            registrySize++;
            for(int i = 1; i < atributesAmount; i++){
                String name = readString(attributeNameSize);
                int type = raf.readInt();
                int size = 0;
                switch (type){
                    case 0: {
                        size = raf.readInt();
                        configSize += 4;
                        registrySize += size*2;
                        break;
                    }
                    case 1: {
                        registrySize += 4;
                        break;
                    }
                    case 2: {
                        registrySize += 8;
                        break;
                    }
                    case 3: {
                        registrySize += 1;
                        break;
                    }
                }
                Attribute a = new Attribute(name,size, TypeEnum.values()[type]);
                attributes.add(a);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setUpFile(ArrayList<Attribute> attributes){
        this.attributes = attributes;
        try {
            raf.writeInt(attributes.size()+1);
            raf.writeInt(attributeNameSize);
            raf.writeInt(codePosition);
            raf.writeChars(adaptString("Active", attributeNameSize));
            raf.writeInt(TypeEnum.BOOLEAN_TYPE.ordinal());
            configSize += ((attributes.size() + 1) * (attributeNameSize*2 +4)) + 12;
            registrySize++;
            for (Attribute p: attributes) {
                raf.writeChars(adaptString(p.getName(), attributeNameSize));
                raf.writeInt(p.geteType().ordinal());
                switch (p.geteType().ordinal()){
                    case 0: {
                        raf.writeInt(p.getSize());
                        configSize += 4;
                        registrySize += p.getSize()*2;
                        break;
                    }
                    case 1: {
                        registrySize += 4;
                        break;
                    }
                    case 2: {
                        registrySize += 8;
                        break;
                    }
                    case 3:{
                        registrySize += 1;
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the received UserType if its not already in the file.
     * @param userType
     */
    public void writeRegistry(UserType userType){
        try {
            searchRegistry(userType.getCode());
            throw new KeyAlreadyExistsExc();
        }
        catch (RegistryNotFoundException e) {
            goEnd();
            indexFileStrategy.indexWrite(new KeyPositionPair(userType.getCode(), registryAmount()));
            writeRegistryAtCurrentPosition(userType);
        }
    }

    /**
     * Updates the UserType if its found in the file.
     * @param userType
     */
    public void updateRegistry(UserType userType){
        try {
            raf.seek(searchRegistry(userType.getCode()).getValue());
            writeRegistryAtCurrentPosition(userType);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Deletes the UserType with the given key from the file.
     * @param key
     */
    public void delete(int key){
        try {
            raf.seek(searchRegistry(key).getValue());
            indexFileStrategy.delete(key);
            raf.writeBoolean(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeRegistryAtCurrentPosition(UserType registry){
        int stringCounter = 0;
        int intCounter = 0;
        int doubleCounter = 0;
        int booleanCounter = 0;
        try {
            raf.writeBoolean(registry.isActive());
            for (Attribute p : attributes) {
                switch (p.geteType().ordinal()) {
                    case 0: {
                        raf.writeChars(adaptString(registry.getStringArray().get(stringCounter).getValue(),
                                p.getSize()));
                        stringCounter++;
                        break;
                    }
                    case 1: {
                        raf.writeInt(registry.getIntegerArray().get(intCounter).getValue());
                        intCounter++;
                        break;
                    }
                    case 2: {
                        raf.writeDouble(registry.getDoubleArray().get(doubleCounter).getValue());
                        doubleCounter++;
                        break;
                    }
                    case 3:{
                        raf.writeBoolean(registry.getBooleanArray().get(booleanCounter).getValue());
                        booleanCounter++;
                        break;
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads an UserType starting at the current Position.
     * @return Read UserType
     */
    public UserType readRegistry(){
        UserType userType = new UserType();
        userType.setCodePosition(codePosition);
        try {
            double registry = (raf.getFilePointer() - configSize)/(registrySize);
            if(raf.getFilePointer() < configSize)
                goToReg(0);
            else if(registry%10 != 0)
                goToReg((int)registry);
            if(!raf.readBoolean()){
                userType.deactivate();
            }
            for(Attribute p: attributes){
                switch (p.geteType().ordinal()){
                    case 0: {
                        String s = readString(p.getSize());
                        userType.addString(p.getName(), p.getSize(), s);
                        break;
                    }
                    case 1: {
                        Integer i = raf.readInt();
                        userType.addInteger(p.getName(), i);
                        break;
                    }
                    case 2: {
                        Double d = raf.readDouble();
                        userType.addDouble(p.getName(), d);
                        break;
                    }
                    case 3: {
                        userType.addBoolean(p.getName(), raf.readBoolean());
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userType;
    }

    /**
     * Searches the file for the UserType with the given key.
     * @param key
     * @return Found UserType
     * @throws RegistryNotFoundException if there is no UserType with the given key
     */
    public UserType search(int key) throws RegistryNotFoundException{
        return searchRegistry(key).getKey();
    }

    private Pair<UserType, Long> searchRegistry(int key) throws RegistryNotFoundException{
        Pair<UserType,Long> r = null;
        try {
             r = indexFileStrategy.indexSearch(this, key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return r;
    }

    private String adaptString(String s, int size){
        String r = s;
        if(s.length() > size)
            r = s.substring(0, size);
        else
            for(int i = s.length(); i < size; i++)
                r = r + " ";
        return r;
    }

    private String shortenString(String s){
        String r = s;
        int c = 0;
        for(int i = s.length()-1; i >= 0; i--)
            if(r.charAt(i) == ' ') {
                c++;
            }
            else
                break;
        r = r.substring(0, r.length() - c);
        return r;
    }

    private String readString(int size) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i++){
            try {
                sb.append(raf.readChar());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return shortenString(sb.toString());
    }

    private void goStart(){
        try {
            raf.seek(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void goEnd(){
        try {
            raf.seek(raf.length());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the amount of registry written in the file
     */
    public int registryAmount() {
        try {
            return (int)(raf.length() - configSize)/registrySize;
        }
        catch (IOException e){
            return 0;
        }
    }

    /**
     * Moves the reader to the start of a given registry
     * @param reg
     */
    public void goToReg(int reg){
        try {
            if(reg <= registryAmount())
                raf.seek((reg * registrySize) + configSize); //TODO dis ok?
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the size in bytes of a registry
     * @return the size of a registry
     */
    public int getRegistrySize(){
        return registrySize;
    }

    /**
     * @return the current pointed position in the file.
     * @throws IOException
     */
    public long getFilePointer() throws IOException {
        return raf.getFilePointer();
    }

    /**
     * Generates a Search Binary Tree tha is an index file.
     * @return the generated structure
     */
    public SearchBinaryTree<KeyPositionPair> generateIndexFile(){
        SearchBinaryTree<KeyPositionPair> sbt = new SearchBinaryTree<>();
        goToReg(0);
        for(int i = 0; i < registryAmount(); i++){
            sbt.insert(new KeyPositionPair(readRegistry().getCode(),i));
        }
        return sbt;
    }

    /**
     * Closes and stops all ongoing processes.
     * Saves the indexFile to disk.
     */
    public void close(){
        try {
            indexFileStrategy.saveToDisk("./src/main/TP11/Files/Sbt" + f.getName());
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes the file.
     * @throws IOException
     */
    public void deleteFile() throws IOException {
        raf.close();
        f.delete();
    }

    public void setIndexFileStrategy(IndexFileStrategy ifs){
        indexFileStrategy = ifs;
    }

    /**
     * Generates an index file and changes the strategy.
     */
    public void autoIndex(){
        indexFileStrategy = new SbtIndexFileStrategy(generateIndexFile());
    }

    /**
     * Compacts the file eliminating all deactivated registry.
     */
    public void compact(){
        File auxf = new File("./src/main/TP11/Files/aaux");
        try {
            //auxf.createNewFile();
            RandomAccessFile auxRAF = new RandomAccessFile(auxf, "rw");
            goStart();
            for(int i = 0; i < configSize; i++){
                auxRAF.write(raf.read());
            }
            auxRAF.close();
            UserTypeFile auxUTF = new UserTypeFile("aaux");
            for(int i = 0; i < registryAmount(); i++){
                UserType userType = readRegistry();
                if(userType.isActive())
                    auxUTF.writeRegistryAtCurrentPosition(userType);
            }
            auxUTF.close();
            String name = f.getPath();
            deleteFile();
            File auxf2 = new File(name);
            System.out.println(auxf.renameTo(auxf2));
            f = new File(name);
            raf = new RandomAccessFile(f, "rw");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<UserType> getByCondition(Predicate<UserType> predicate){
        goToReg(0);
        ArrayList<UserType> r = new ArrayList<>();
        for (int i = 0; i < registryAmount(); i++){
            UserType userType = readRegistry();
            if(userType.isActive() && predicate.test(userType)){
                r.add(userType);
            }
        }
        return r;
    }

    /**
     * @return an ArrayList with all the written UserType
     */
    public ArrayList<UserType> getReport(){
        goToReg(0);
        ArrayList<UserType> r = new ArrayList<>();
        for (int i = 0; i < registryAmount(); i++){
            UserType userType = readRegistry();
            if(userType.isActive()){
                r.add(userType);
            }
        }
        return r;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }
}