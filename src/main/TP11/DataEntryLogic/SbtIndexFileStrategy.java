package main.TP11.DataEntryLogic;

import javafx.util.Pair;
import main.TP11.Exceptions.RegistryNotFoundException;
import main.Utils.ObjectUtil.ObjectSaver;
import struct.impl.Exceptions.ObjectNotFoundException;
import struct.impl.SearchBinaryTree;

import java.io.IOException;
import java.io.Serializable;

/**
 * IndexFileStrategy that uses a BinarySearchTree
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class SbtIndexFileStrategy implements IndexFileStrategy{

    private SearchBinaryTree<KeyPositionPair> sbt;

    public SbtIndexFileStrategy(SearchBinaryTree<KeyPositionPair> sbt){
        this.sbt = sbt;
    }

    @Override
    public void indexWrite(KeyPositionPair keyPositionPair) {
        sbt.insert(keyPositionPair);
    }

    @Override
    public Pair<UserType, Long> indexSearch(UserTypeFile userTypeFile, int key) throws IOException {
        Long pos;
        try {
            KeyPositionPair r = sbt.search(new KeyPositionPair(key, -1));
            userTypeFile.goToReg(r.getPosition());
            pos = userTypeFile.getFilePointer();
        }
        catch (ObjectNotFoundException e){
            throw new RegistryNotFoundException();
        }
        return new Pair<>(userTypeFile.readRegistry(), pos);
    }

    @Override
    public void delete(int key) {
        sbt.delete(new KeyPositionPair(key, -1));
    }

    @Override
    public Serializable getIndexStructure() {
        return sbt;
    }

    @Override
    public void saveToDisk(String path) {
        ObjectSaver objectSaver = new ObjectSaver();
        objectSaver.writeObject(sbt, path);
    }
}