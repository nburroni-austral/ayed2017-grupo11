package main.TP11.DataEntryLogic;

import javafx.util.Pair;

import java.io.IOException;
import java.io.Serializable;

/**
 * An IndexFileStrategy is an strategy a UserTypeFile can take to work with an index file
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public interface IndexFileStrategy {
    void indexWrite(KeyPositionPair keyPositionPair);
    Pair<UserType, Long> indexSearch(UserTypeFile userTypeFile, int key) throws IOException;
    void delete(int key);
    Serializable getIndexStructure();
    void saveToDisk(String path);
}
