package main.TP11.DataEntryLogic;

import main.TP11.Exceptions.InvalidUserTypeDataExc;

import java.util.ArrayList;

/**
 * Factory of user types that creates UserTypes with the same attributes and code positions to ensure equal types.
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class UserTypeFactory {
    private ArrayList<Attribute> stringAttributes;
    private ArrayList<Attribute> intAttributes;
    private ArrayList<Attribute> doubleAttributes;
    private ArrayList<Attribute> booleanAttributes;
    private int codePosition;

    public UserTypeFactory(ArrayList<Attribute> attributes, int codePosition) {
        this.codePosition = codePosition;
        stringAttributes = new ArrayList<>();
        intAttributes = new ArrayList<>();
        doubleAttributes = new ArrayList<>();
        booleanAttributes = new ArrayList<>();
        for (Attribute attribute: attributes) {
            switch (attribute.geteType()){
                case STRING_TYPE:
                    stringAttributes.add(attribute);
                    break;
                case INT_TYPE:
                    intAttributes.add(attribute);
                    break;
                case DOUBLE_TYPE:
                    doubleAttributes.add(attribute);
                    break;
                case BOOLEAN_TYPE:
                    booleanAttributes.add(attribute);
                    break;
            }
        }
    }

    /**
     * Takes the fields to be used to create a new UserType and returns a fully constructed UserType
     * @param strings string values of the result UserType
     * @param integers integer values of the result UserType
     * @param doubles double values of the result UserType
     * @param booleans boolean values of the result UserType
     * @return fully constructed UserType with the given values.
     */
    public UserType createFullUserType(ArrayList<String> strings, ArrayList<Integer> integers, ArrayList<Double> doubles, ArrayList<Boolean> booleans){
        if(strings.size() != stringAttributes.size() || integers.size() != intAttributes.size() ||
                doubles.size() != doubleAttributes.size() || booleans.size() != booleanAttributes.size()){
            throw new InvalidUserTypeDataExc();
        } else {
            UserType result = new UserType();
            for(int i = 0; i < stringAttributes.size(); i++){
                Attribute stringAttribute = stringAttributes.get(i);
                result.addString(stringAttribute.getName(), stringAttribute.getSize(), strings.get(i));
            }
            for(int i = 0; i < intAttributes.size(); i++){
                Attribute intAttribute = intAttributes.get(i);
                result.addInteger(intAttribute.getName(), integers.get(i));
            }
            for(int i = 0; i < doubleAttributes.size(); i++){
                Attribute doubleAttribute = doubleAttributes.get(i);
                result.addDouble(doubleAttribute.getName(), doubles.get(i));
            }
            for(int i = 0; i < booleanAttributes.size(); i++){
                Attribute booleanAttribute = booleanAttributes.get(i);
                result.addBoolean(booleanAttribute.getName(), booleans.get(i));
            }
            result.setCodePosition(codePosition);
            return result;
        }
    }

    /**
     * Creates an empty UserType with just a key.
     * @param key UserType key
     * @return empty UserType
     */
    public UserType createBaseUserType(Integer key){
        UserType result = new UserType();
        result.setCodePosition(codePosition);
        result.addInteger(intAttributes.get(codePosition).getName(), key);
        for(int i = 0; i < stringAttributes.size(); i++){
            Attribute stringAttribute = stringAttributes.get(i);
            result.addString(stringAttribute.getName(), stringAttribute.getSize(), "");
        }
        for(int i = 0; i < intAttributes.size(); i++){
            Attribute intAttribute = intAttributes.get(i);
            result.addInteger(intAttribute.getName(), 0);
        }
        for(int i = 0; i < doubleAttributes.size(); i++){
            Attribute doubleAttribute = doubleAttributes.get(i);
            result.addDouble(doubleAttribute.getName(), 0.0);
        }
        for(int i = 0; i < booleanAttributes.size(); i++){
            Attribute booleanAttribute = booleanAttributes.get(i);
            result.addBoolean(booleanAttribute.getName(), false);
        }
        return result;
    }

    /**
     * @return name of the key for the UserTypes the factory produces
     */
    public String getKeyName(){
        return intAttributes.get(codePosition).getName();
    }
}
