package main.TP11.DataEntryLogic;

import javafx.util.Pair;
import main.TP11.Exceptions.RegistryNotFoundException;

import java.io.IOException;
import java.io.Serializable;

/**
 * IndexFileStrategy that does not use an index file.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class NoIndexFileStrategy implements IndexFileStrategy{

    @Override
    public void indexWrite(KeyPositionPair key) {
    }

    @Override
    public Pair<UserType, Long> indexSearch(UserTypeFile userTypeFile, int key) throws IOException {
        userTypeFile.goToReg(0);
        for(int i = 0; i < userTypeFile.registryAmount(); i++){
            UserType userType = userTypeFile.readRegistry();
            if(userType.isActive() && userType.getCode() == key)
                return new Pair<>(userType, userTypeFile.getFilePointer()- userTypeFile.getRegistrySize());
        }
        throw new RegistryNotFoundException();
    }

    @Override
    public void delete(int key) {
    }

    @Override
    public Serializable getIndexStructure() {
        return null;
    }

    @Override
    public void saveToDisk(String path) {
    }
}
