package main.TP11.DataEntryLogic;

import java.io.Serializable;

/**
 * A pair of a key and a value that implements Comparable
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class KeyPositionPair implements Comparable<KeyPositionPair>, Serializable {

    private int key;
    private int position;

    public KeyPositionPair(int key, int position){
        this.key = key;
        this.position = position;
    }

    public int getKey() {
        return key;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public int compareTo(KeyPositionPair i) {
        return key < i.getKey() ? -1 : key > i.getKey() ? 1 : 0;
    }
}
