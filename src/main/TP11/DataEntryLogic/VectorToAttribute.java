package main.TP11.DataEntryLogic;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Class containing a method to make Attributes from Vectors
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class VectorToAttribute {

    /**
     * Takes a vector of strings representing Attributes and creates a list of the corresponding attributes.
     * @param vector containing attribute data
     * @return list of attributes
     */
    public static ArrayList<Attribute> vectorToAttribute(Vector vector){
        ArrayList<Attribute> attributes = new ArrayList<>(vector.size());
        Vector<Vector<String>> v = (Vector<Vector<String>>) vector;
        for (Vector<String> attribute: v) {
            TypeEnum dataType = TypeEnum.fromString(attribute.get(1));
            int size = 0;
            if(dataType == TypeEnum.STRING_TYPE){
                size = Integer.parseInt(attribute.get(2));
            }
            Attribute result = new Attribute(attribute.get(0), size, dataType);
            attributes.add(result);
        }
        return attributes;
    }
}
