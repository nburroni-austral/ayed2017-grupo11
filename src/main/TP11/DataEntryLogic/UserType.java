package main.TP11.DataEntryLogic;

import javafx.util.Pair;

import java.util.ArrayList;

/**
 * UserType represents a type that is created by a user.
 * Supports String, Integer, Double, Boolean.
 * The codePosition indicates the position of the code in the integer array.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class UserType implements Comparable<Integer>{
    private ArrayList<Pair<Pair<String,Integer>,String>> stringArray;
    private ArrayList<Pair<String,Integer>> integerArray;
    private ArrayList<Pair<String,Double>> doubleArray;
    private ArrayList<Pair<String,Boolean>> booleanArray;
    private boolean active;
    private int codePosition;

    public UserType(){
        stringArray = new ArrayList<>();
        integerArray = new ArrayList<>();
        doubleArray = new ArrayList<>();
        booleanArray = new ArrayList<>();
        active = true;
        codePosition = 0;
    }

    /**
     * Adds a String to the userType.
     * @param name
     * @param size
     * @param data
     */
    public void addString(String name, int size, String data){
        stringArray.add(new Pair<>(new Pair<>(name, size),data));
    }

    /**
     * Adds a Integer to the userType.
     * @param name
     * @param data
     */
    public void addInteger(String name, Integer data){
        integerArray.add(new Pair<>(name, data));
    }

    /**
     * Adds a Double to the userType.
     * @param name
     * @param data
     */
    public void addDouble(String name, Double data){
        doubleArray.add(new Pair<>(name, data));
    }

    /**
     * Adds a Boolean to the userType.
     * @param name
     * @param data
     */
    public void addBoolean(String name, Boolean data){
        booleanArray.add(new Pair<>(name, data));
    }

    public ArrayList<Pair<String, Boolean>> getBooleanArray() {
        return booleanArray;
    }

    public boolean isActive(){
        return active;
    }

    public void deactivate(){
        active = false;
    }

    public void setCodePosition(int codePosition){
        this.codePosition = codePosition;
    }

    public int getCodePosition(){
        return codePosition;
    }

    public int getCode(){
        return integerArray.get(codePosition).getValue();
    }

    public ArrayList<Pair<Pair<String, Integer>, String>> getStringArray() {
        return stringArray;
    }

    public ArrayList<Pair<String, Integer>> getIntegerArray() {
        return integerArray;
    }

    public ArrayList<Pair<String, Double>> getDoubleArray() {
        return doubleArray;
    }

    /**
     * Edits the string in the given position
     * @param string
     * @param position
     */
    public void editString(String string, int position){
        if(position < stringArray.size())
            stringArray.set(position, new Pair<>(new Pair<>(stringArray.get(position).getKey().getKey(), stringArray.get(position).getKey().getValue()), string));
    }

    /**
     * Edits the integer in the given position
     * @param data
     * @param position
     */
    public void editInteger(Integer data, int position){
        if(position < integerArray.size() && position != codePosition)
            integerArray.set(position, new Pair<>(integerArray.get(position).getKey(), data));
    }

    /**
     * Edits the Double in the given position
     * @param data
     * @param position
     */
    public void editDouble(Double data, int position) {
        if (position < doubleArray.size())
            doubleArray.set(position, new Pair<>(doubleArray.get(position).getKey(), data));
    }

    /**
     * Edits the Boolean in the given position
     * @param data
     * @param position
     */
    public void editBoolean(Boolean data, int position) {
        if (position < booleanArray.size())
            booleanArray.set(position, new Pair<>(booleanArray.get(position).getKey(), data));
    }

    @Override
    public int compareTo(Integer integer) {
        return getCode() > integer ? 1 : getCode() < integer ? -1 : 0;
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (Pair<Pair<String, Integer>, String> s: stringArray){
            stringBuilder.append(s.getKey().getKey());
            stringBuilder.append(": ");
            stringBuilder.append(s.getValue());
            stringBuilder.append(" ");
        }
        for (Pair<String, Integer> s: integerArray){
            stringBuilder.append(s.getKey());
            stringBuilder.append(": ");
            stringBuilder.append(s.getValue());
            stringBuilder.append(" ");
        }
        for (Pair<String, Double> s: doubleArray){
            stringBuilder.append(s.getKey());
            stringBuilder.append(": ");
            stringBuilder.append(s.getValue());
            stringBuilder.append(" ");
        }
        for (Pair<String, Boolean> s: booleanArray){
            stringBuilder.append(s.getKey());
            stringBuilder.append(": ");
            stringBuilder.append(s.getValue());
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public int compareStringAtt(String attName, String value){
        Pair<Pair<String,Integer>,String> stringAtt = stringArray.stream().filter(e -> e.getKey().getKey().equalsIgnoreCase(attName)).findFirst().get();
        return stringAtt.getValue().compareTo(value);
    }

    public int compareIntAtt(String attName, Integer value){
        Pair<String,Integer> intAtt = integerArray.stream().filter(e -> e.getKey().equalsIgnoreCase(attName)).findFirst().get();
        return intAtt.getValue().compareTo(value);
    }

    public int compareDoubleAtt(String attName, Double value){
        Pair<String,Double> doubleAtt = doubleArray.stream().filter(e -> e.getKey().equalsIgnoreCase(attName)).findFirst().get();
        return doubleAtt.getValue().compareTo(value);
    }

    public boolean getBooleanAtt(String attName){
        return booleanArray.stream().filter(e -> e.getKey().equalsIgnoreCase(attName)).findFirst().get().getValue();
    }
}
