package main.TP11.DataEntryLogic;

/**
 * Enum for supported types.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public enum TypeEnum {
    STRING_TYPE("Text"),
    INT_TYPE("Integer"),
    DOUBLE_TYPE("Decimal"),
    BOOLEAN_TYPE("Boolean");

    private String text;

    TypeEnum(String text){
        this.text = text;
    }

    public static TypeEnum fromString(String s){
        for (TypeEnum e: TypeEnum.values()) {
            if(e.text.equalsIgnoreCase(s)) return e;
        }
        return null;
    }
}
