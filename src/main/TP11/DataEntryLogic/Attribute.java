package main.TP11.DataEntryLogic;

/**
 * Represents an attribute with a name, a TypeEnum and a size
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class Attribute {
    private String name;
    private int size;
    private TypeEnum eType;

    public Attribute(String name, int size, TypeEnum eType) {
        this.name = name;
        this.size = size;
        this.eType = eType;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public TypeEnum geteType() {
        return eType;
    }
}
