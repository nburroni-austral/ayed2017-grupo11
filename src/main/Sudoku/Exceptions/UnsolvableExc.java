package main.Sudoku.Exceptions;

public class UnsolvableExc extends RuntimeException {
    public UnsolvableExc(){
        super("Unsolvable Sudoku");
    }
}
