package main.TP4.PalindromeAnalizer;

import struct.impl.DynamicQueue;
import struct.impl.DynamicStack;

/**
 * PalindromeAnalyzer has a method that checks if a String is a palindrome
 */

public class PalindromeAnalyzer {
    private DynamicQueue<Character> buffer;
    private DynamicStack<Character> stack;

    public PalindromeAnalyzer() {
        buffer = new DynamicQueue<>();
        stack = new DynamicStack<>();
    }

    /**
     * Checks if a String is a palindrome.
     * @param s string to check
     * @return true if s is palindrome, false if it isn't.
     */

    public boolean isPalindrome(String s){
        String sCopy;
        if(s.length() % 2 != 0){
            sCopy = s.substring(0, s.length()/2);
            sCopy += s.substring(s.length()/2 + 1);
        }
        else sCopy = s;
        sCopy = sCopy.toLowerCase();

        for(int i = 0; i<sCopy.length(); i++){
            buffer.enqueue(sCopy.charAt(i));
        }

        while(!buffer.isEmpty()){
            char currentChar = buffer.dequeue();
            if(!stack.isEmpty() && currentChar == stack.peek()) stack.pop();
            else stack.push(currentChar);
        }

        boolean result = stack.size() == 0;
        stack.empty();
        buffer.empty();

        return result;
    }
}
