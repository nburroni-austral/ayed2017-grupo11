package main.TP4.PriorityQueue;

import struct.impl.DynamicQueue;

import java.util.ArrayList;

/**
 * PriorityQueue is a TDA that manages queues with different priorities.
 * When enqueueing, the user specifies the element-to-enqueue priority.
 * When doing a dequeue, the element is the first from the highest
 * priority queue.
 * @param <Q> The PriorityQueue data type.
 */
public class PriorityQueue <Q> {
    private ArrayList<DynamicQueue<Q>> queues;

    public PriorityQueue(int priorities){
        queues = new ArrayList<>();
        while(priorities > 0) {
            queues.add(new DynamicQueue<Q>());
            priorities--;
        }
    }

    /**
     * Adds the element to the corresponding priority queue.
     * @param element Element to be added.
     * @param priority Priority of the element.
     */
    public void enqueue(Q element, int priority){
        if(priority > 0 && priority <= queues.size()){
            queues.get(priority-1).enqueue(element);
        }
    }

    /**
     * Dequeues the element from the highest priority non-empty queue.
     * @return the dequeued element.
     */
    public Q dequeue(){
        for (DynamicQueue<Q> queue: queues) {
            if(!queue.isEmpty())
                return queue.dequeue();
        }
        return null;
    }

}
