package main.TP4.BankSimulation;

import struct.impl.DynamicQueue;

import java.util.LinkedList;

/**
 * Represents a bank with a queue and cashiers.
 */
public class BankSingleQ extends Bank {
    private DynamicQueue<Client> queue;
    private LinkedList<Cashier> cashiers;
    private boolean open = true;
    private int timeOpened;

    public BankSingleQ(int timeOpened) {
        queue = new DynamicQueue<>();
        cashiers = new LinkedList<>();
        this.timeOpened = timeOpened;
    }

    /**
     * @return a linked list with the shortest queue/s.
     */
    @Override
    public LinkedList<Integer> getShortestQueuesIndex() {
        LinkedList<Integer> result = new LinkedList<>();
        result.add(0);
        return result;
    }

    /**
     *
     * @return amount of waiting clients.
     */
    @Override
    public int getWaitingClientNumber() {
        return queue.size();
    }

    /**
     * Adds a client to the queue.
     * @param client client to be added.
     * @param queueIndex index of the queue where the client will be added.
     * @return true if the client was added to the queue.
     */
    @Override
    public boolean addToQueue(Client client, int queueIndex){
        if(open) {
            queue.enqueue(client);
            return true;
        }
        return false;
    }

    /**
     * Adds a cashier to the arraylist of cashiers.
     * @param cashier cashier to be added.
     */
    @Override
    public void addCashier(Cashier cashier) {
        cashiers.add(cashier);
    }

    /**
     * Forwards a step in the bank functionality.
     * @param actualTime current real time.
     */
    public void forward(int actualTime){
        if(actualTime >= timeOpened) open = false;
        if(open || queue.size() > 0) {
            LinkedList<Integer> availableCashierIndexes = new LinkedList<>();
            for (int i = 0; i < cashiers.size(); i++) {
                if (!cashiers.get(i).isWorking(actualTime)) {
                    availableCashierIndexes.add(i);
                }
            }
            while(!queue.isEmpty() && !availableCashierIndexes.isEmpty()) {
                Client nextClient = queue.dequeue();
                Integer selectedCashierIndex = nextClient.selectCashier(availableCashierIndexes);
                cashiers.get(selectedCashierIndex).serve(actualTime);
                availableCashierIndexes.remove(selectedCashierIndex);
            }
        }
    }

    public int getTimeOpened() {
        return timeOpened;
    }

    /**
     * @param actualTime current real time.
     * @return true if the bank is still open.
     */
    public boolean isWorking(int actualTime){
        return getWaitingClientNumber() > 0 || open || !cashiersFree(actualTime);
    }

    /**
     * Checks if all the cashiers are not working.
     * @param actualTime current real time.
     * @return true if all the cashiers are free.
     */
    public boolean cashiersFree(int actualTime){
        boolean cashiersFree = true;
        for (Cashier cashier: cashiers) {
            if(cashier.isWorking(actualTime)) {
                cashiersFree = false;
                break;
            }
        }
        return cashiersFree;
    }

    /**
     * Resets the bank state.
     */
    public void reset(){
        open = true;
        for(Cashier cashier: cashiers){
            cashier.reset();
        }
    }
}
