package main.TP4.BankSimulation;

import java.util.LinkedList;

/**
 * BankSimulation takes a bank and a client factory in order to run a simulation of a bank, where clients enter, queue and
 * interact with cashiers.
 */

public class BankSimulation {
    private Bank bank;
    private ClientFactory factory;
    private int actualClients, finishedTime;

    public BankSimulation(Bank bank, ClientFactory factory){
        this.bank = bank;
        this.factory = factory;
    }

    /**
     * Runs a single simulation of the bank
     */

    public void run(){
        reset();
        for(int i = 1; bank.isWorking(i); i++) {
            if(factory.isFinished() && bank.cashiersFree(i+1) && bank.getWaitingClientNumber() == 0){
                if(finishedTime == 0) finishedTime = i;
            }
            LinkedList<Client> clients = factory.forward(i);
            for (Client client : clients) {
                if (client.enterBank(bank)) actualClients++;
            }
            bank.forward(i);
        }
    }

    public int getActualClients() {
        return actualClients;
    }

    /**
     * @return the extra time taken after the bank is closed
     */

    public int getExtraTime() {
        return finishedTime - bank.getTimeOpened() > 0 ? finishedTime - bank.getTimeOpened() : 0;
    }

    public int getFinishedTime(){
        return finishedTime;
    }

    public int getCreatedClients(){
        return factory.getTotalClients();
    }

    private void reset(){
        factory.reset();
        bank.reset();
        actualClients = 0;
        finishedTime = 0;
    }
}

