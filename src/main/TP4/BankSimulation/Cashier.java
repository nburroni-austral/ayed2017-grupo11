package main.TP4.BankSimulation;

import java.util.Random;

/**
 * Represents a cashier with a working state that remains working
 * for a random amount of time between min and max.
 */
public class Cashier {

    private boolean working;
    private Random random;
    private int min,max,endTime;

    public Cashier(int min, int max){
        working = false;
        this.min = min;
        this.max = max;
        random = new Random();
    }

    /**
     * If the cashier is not already working, changes the working
     * state to false and calculates how much time is going to
     * remain working.
     * @param time current real time.
     */
    public void serve(int time){
        if(!working) {
            working = true;
            endTime = (random.nextInt((max + 1 - min)) + min) + time;
        }
    }

    /**
     * Changes the working state to false if the received
     * time is greater than the endTime.
     * @param time current real time.
     * @return true if the cashier is working.
     */
    public boolean isWorking(int time){
        if(time >= endTime) working = false;
        return working;
    }

    /**
     * Sets the endTime to 0.
     */
    public void reset(){
        endTime = 0;
    }

}
