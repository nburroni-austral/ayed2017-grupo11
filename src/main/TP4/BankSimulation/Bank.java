package main.TP4.BankSimulation;

import java.util.LinkedList;

/**
 * Represents a bank with queue/s and cashiers.
 */
public abstract class Bank {
    public abstract LinkedList<Integer> getShortestQueuesIndex();
    public abstract int getWaitingClientNumber();
    public abstract boolean addToQueue(Client client, int queueIndex);
    public abstract void addCashier(Cashier cashier);
    public abstract void forward(int actualTime);
    public abstract int getTimeOpened();
    public abstract boolean isWorking(int actualTime);
    public abstract void reset();
    public abstract boolean cashiersFree(int actualTime);
}
