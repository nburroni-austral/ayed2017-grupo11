package main.TP4.BankSimulation;

import java.util.LinkedList;
import java.util.Random;

/**
 * ClientFactory creates a random number of Clients, up to a maximum, at certain intervals of time.
 */

public class ClientFactory {
    private int totalClients, createdClients, endTime, interval, minClients, maxClients, initialMin, initialMax;
    private Random random;
    private boolean finished = false;

    public ClientFactory(int totalClients, int interval, int minClients, int maxClients) {
        this.totalClients = totalClients;
        this.interval = interval;
        this.minClients = minClients;
        this.initialMin = minClients;
        this.maxClients = maxClients;
        this.initialMax = maxClients;
        random = new Random();
    }

    /**
     * Moves the factory in time to the time received
     * @param actualTime the factory's new time
     * @return a list of the clients created
     */

    public LinkedList<Client> forward(int actualTime){
        int clientDifference = totalClients - createdClients;
        LinkedList<Client> resultList = new LinkedList<>();
        if(!finished) {
            if (clientDifference > 0) {
                if (actualTime > endTime) {
                    endTime += interval;
                    if (clientDifference < maxClients) {
                        maxClients = clientDifference;
                        if (clientDifference < minClients) minClients = 0;
                    }
                    int numberOfClients = random.nextInt(maxClients + 1 - minClients) + minClients;
                    createdClients += numberOfClients;
                    while (numberOfClients > 0) {
                        resultList.add(new Client());
                        numberOfClients--;
                    }
                }
            }
            if (clientDifference == 0) finished = true;
        }
        return resultList;
    }

    public int getTotalClients() {
        return totalClients;
    }

    /**
     * Resets the factory so it can be used again.
     */

    public void reset(){
        createdClients = 0;
        endTime = 0;
        minClients = initialMin;
        maxClients = initialMax;
        finished = false;
    }

    public boolean isFinished() {
        return finished;
    }
}
