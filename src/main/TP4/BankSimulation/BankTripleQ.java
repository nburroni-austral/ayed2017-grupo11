package main.TP4.BankSimulation;

import struct.impl.DynamicQueue;
import java.util.LinkedList;

/**
 * Represents a bank with queues and cashiers.
 */
public class BankTripleQ extends Bank {
    private LinkedList<DynamicQueue<Client>> queues;
    private LinkedList<Cashier> cashiers;
    private boolean open;
    private int timeOpened;

    public BankTripleQ(int timeOpened) {
        this.timeOpened = timeOpened;
        queues = new LinkedList<>();
        cashiers = new LinkedList<>();
        open = true;
    }

    /**
     * Forwards a step in the bank functionality.
     * @param time current real time.
     */
    public void forward(int time){
        if(open || getWaitingClientNumber() > 0){
            for (int i = 0; i < cashiers.size(); i++) {
                if(!queues.get(i).isEmpty() && !cashiers.get(i).isWorking(time)){
                    cashiers.get(i).serve(time);
                    queues.get(i).dequeue();
                }
            }
        }
        if(time >= timeOpened)
            open = false;
    }

    /**
     * Adds a client to the queue.
     * @param client client to be added.
     * @param index index of the queue where the client will be added.
     * @return true if the client was added to the queue.
     */
    @Override
    public boolean addToQueue(Client client, int index){
        if (!open) return false;
        queues.get(index).enqueue(client);
        return true;
    }

    /**
     * Adds a cashier to the arraylist of cashiers.
     * @param cashier cashier to be added.
     */
    @Override
    public void addCashier(Cashier cashier){
        cashiers.add(cashier);
        queues.add(new DynamicQueue<>());
    }

    /**
     * @return a linked list with the shortest queue/s.
     */
    @Override
    public LinkedList<Integer> getShortestQueuesIndex() {
        LinkedList<Integer> result = new LinkedList<>();
        int min = queues.get(0).size();
        result.add(0);
        for (int i = 1; i < queues.size(); i++) {
            if(queues.get(i).size() < min){
                result.clear();
                result.add(i);
            }
            else if(queues.get(i).size() == min) result.add(i);
        }
        return result;
    }

    /**
     *
     * @return amount of waiting clients.
     */
    @Override
    public int getWaitingClientNumber() {
        int result = 0;
        for (DynamicQueue queue : queues) {
            result += queue.size();
        }
        return result;
    }

    public int getTimeOpened() {
        return timeOpened;
    }

    /**
     * @param actualTime current real time.
     * @return true if the bank is still open.
     */
    public boolean isWorking(int actualTime){
        return getWaitingClientNumber() > 0 || open || !cashiersFree(actualTime);
    }

    /**
     * Checks if all the cashiers are not working.
     * @param actualTime current real time.
     * @return true if all the cashiers are free.
     */
    public boolean cashiersFree(int actualTime){
        boolean cashiersFree = true;
        for (Cashier cashier: cashiers) {
            if(cashier.isWorking(actualTime)) {
                cashiersFree = false;
                break;
            }
        }
        return cashiersFree;
    }

    /**
     * Resets the bank state.
     */
    public void reset(){
        open = true;
        for(Cashier cashier: cashiers){
            cashier.reset();
        }
    }
}
