package main.TP4.BankSimulation;

import java.util.LinkedList;
import java.util.Random;

/**
 * Client represents a bank client in a simulation where it can enter a bank and randomly choose where to go, even refuse
 * to enter the bank altogether.
 */

public class Client {
    private Random random = new Random();

    /**
     * The client tries to enter the bank, returns whether it entered or not
     * @param bank the bank to enter
     * @return true if the client entered the bank, false if it didn't
     */

    public boolean enterBank(Bank bank){
        if(bank.getWaitingClientNumber() > 4 && bank.getWaitingClientNumber() <= 8){
            if(random.nextFloat() < 0.25) return false;
        }
        else if(bank.getWaitingClientNumber() > 8){
            if(random.nextFloat() < 0.5) return false;
        }
        LinkedList<Integer> queuesIndex = bank.getShortestQueuesIndex();
        int selectedQueueIndex = queuesIndex.get(random.nextInt(queuesIndex.size()));
        return bank.addToQueue(this, selectedQueueIndex);
    }

    /**
     * Given a list of numbers representing cashiers the client chooses one at random
     * @param cashiersIndex a list containing numbers representing cashier positions
     * @return the chosen cashier's position
     */

    public int selectCashier(LinkedList<Integer> cashiersIndex){
        return cashiersIndex.get(random.nextInt(cashiersIndex.size()));
    }
}
