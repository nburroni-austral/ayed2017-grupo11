package main.TP9;

import main.TP9.Exceptions.NoValueExc;
import main.TP9.TextEditorLogic.IntRange;
import main.TP9.TextEditorLogic.SoundexString;
import main.TP9.TextEditorLogic.SpellingChecker;
import main.TP9.TextEditorView.HintsFrame;
import main.TP9.TextEditorView.MistakeListPanel;
import main.TP9.TextEditorView.TextEditorFrame;
import main.TP9.TextEditorView.TextEditorPanel;
import struct.impl.HashTable.OAHashTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Controller managing the functionality of the TextEditorFrame and HintsFrame
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class TextEditorController {

    private HintsFrame hintsFrame;
    private TextEditorPanel textEditorPanel;
    private SpellingChecker spellingChecker;
    private MistakeListPanel mistakeListPanel;
    private IntRange mistakeRange;
    private String selectedHint;

    public TextEditorController(OAHashTable<SoundexString> dictionary){
        textEditorPanel = new TextEditorPanel(new TextEditorDL(), new CheckButtonAL());
        mistakeListPanel = new MistakeListPanel(new MistakeButtonAL());
        new TextEditorFrame(textEditorPanel, mistakeListPanel);
        spellingChecker = new SpellingChecker(dictionary);
    }

    private class TextEditorDL implements DocumentListener{
        @Override
        public void insertUpdate(DocumentEvent e) {
            clearMistakes();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            insertUpdate(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            //Plain text components do not fire these events.
        }
    }

    private class MistakeButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            selectedHint = "";
            mistakeListPanel.hideWarning();
            try {
                int index = mistakeListPanel.getSelectedIndex();
                String mistake = mistakeListPanel.get(index);
                mistakeRange = textEditorPanel.getMarkedPositions(index);
                hintsFrame = new HintsFrame(spellingChecker.getHints(mistake), new CorrectBtnAl(), new CancelBtnAl());
            } catch (NoValueExc exc){
                mistakeListPanel.showWarning();
            }
        }
    }

    private class CheckButtonAL implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            checkMistakes();
        }
    }

    private void checkMistakes() {
        clearMistakes();
        String text = textEditorPanel.getText();
        ArrayList<String> mistakes = spellingChecker.checkString(text);
        mistakes.forEach(mistakeListPanel::addMistake);
        spellingChecker.getStringIndexes(text, mistakes).forEach(textEditorPanel::mark);
    }

    private void clearMistakes() {
        textEditorPanel.unmarkAll();
        mistakeListPanel.clear();
    }

    private class CorrectBtnAl implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            selectedHint = hintsFrame.getSelectedHint();
            hintsFrame.dispose();
            if(selectedHint.length() > 0) {
                textEditorPanel.unmark(mistakeRange);
                textEditorPanel.removeString(mistakeRange);
                textEditorPanel.insertString(mistakeRange.getBegin(), selectedHint);
            }
            checkMistakes();
        }
    }

    private class CancelBtnAl implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            hintsFrame.dispose();
        }
    }
}
