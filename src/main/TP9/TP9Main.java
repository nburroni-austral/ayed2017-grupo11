package main.TP9;

import main.TP9.TextEditorLogic.SoundexString;
import struct.impl.HashTable.OAHashTable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class TP9Main {

    public static void main(String[] args){
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("./src/main/TP9/words_alpha.txt")));
            ArrayList<SoundexString> strings = br.lines().map(SoundexString::new).collect(Collectors.toCollection(ArrayList::new));
            int tableSize = strings.stream().map(e -> e.hashCode(Integer.MAX_VALUE)).max(Comparator.naturalOrder()).get();
            OAHashTable<SoundexString> hashTable = new OAHashTable<>(tableSize);
            strings.forEach(hashTable::insert);
            new TextEditorController(hashTable);
        } catch (IOException IOexc){
            System.out.println("File not found");
        }
    }
}
