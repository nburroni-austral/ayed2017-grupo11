package main.TP9.TextEditorLogic;

/**
 * Represents an integer range.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class IntRange {
    private int begin;
    private int end;

    public IntRange(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }

    /**
     * @return length of the range.
     */
    public int getLength(){
        return end - begin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntRange)) return false;

        IntRange intRange = (IntRange) o;

        if (begin != intRange.begin) return false;
        return end == intRange.end;
    }
}
