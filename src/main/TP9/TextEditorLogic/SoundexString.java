package main.TP9.TextEditorLogic;

import struct.istruct.Hashable;

import java.io.Serializable;

/**
 * String wrapper that changes its hashcode to one depending on the Soundex function.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class SoundexString implements Hashable, Comparable<SoundexString>, Serializable{
    private String string;

    public SoundexString(String string) {
        this.string = string;
    }

    /**
     * Returns the hashcode of the object.
     * @param tableSize size of the hashtable where the object will be inserted.
     * @return object hashcode.
     */
    @Override
    public int hashCode(int tableSize) {
        String soundexString = Soundex.soundex(string);
        int letterNumber = soundexString.charAt(0) - 65;
        int letterCode = letterNumber * 343;
        int parsedCode = Integer.parseInt(soundexString.substring(1));
        int decimalCode = Integer.parseInt("" + parsedCode, 7);
        if(decimalCode != 0) decimalCode -= 48 * (letterNumber + 1);
        else decimalCode -= 48 * letterNumber;
        return (letterCode + decimalCode) % tableSize;
    }

    public String toString(){
        return string;
    }

    @Override
    public int compareTo(SoundexString o) {
        return o.string.compareTo(o.string);
    }

    @Override
    public boolean equals(Object obj) {
        return obj.equals(string);
    }
}
