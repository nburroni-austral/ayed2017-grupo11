package main.TP9.TextEditorLogic;

import struct.impl.DynamicList;
import struct.impl.HashTable.OAHashTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * SpellingChecker has a dictionary that is a hash table and works as a spelling checker.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class SpellingChecker {

    private OAHashTable<SoundexString> dictionary;

    public SpellingChecker(OAHashTable<SoundexString> dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * Checks for spelling mistakes y the received text and returns
     * an ArrayList of those words.
     * @param text
     * @return ArrayList with the mistakes.
     */
    public ArrayList<String> checkString(String text){
        return Arrays.stream(text.split("[^a-zA-Z]"))
                .filter(word -> word.length() > 0)
                .filter(word -> !dictionary.exists(new SoundexString(word)))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Given a word, returns an ArrayList with possible corrections
     * @param word
     * @return ArrayList with possible corrections
     */
    public ArrayList<String> getHints(String word){
         DynamicList<SoundexString> s = dictionary.getAll(new SoundexString(word));
         ArrayList<String> hints = new ArrayList<>();
         for(int i = 0; i<s.size(); i++) {
            s.goTo(i);
            hints.add(s.getActual().toString());
         }
         return hints;
    }

    /**
     * Searches the text for the received mistakes.
     * @param text text to search
     * @param mistakes ArrayList with the mistakes.
     * @return ArrayList with IntRanges of the mistakes in the text.
     */
    public ArrayList<IntRange> getStringIndexes(String text, ArrayList<String> mistakes){
        ArrayList<IntRange> mistakesIndexes = new ArrayList<>();
        int begin = 0;
        for (String mistake: mistakes) {
            begin = text.indexOf(mistake, begin);
            int end = begin + mistake.length();
            mistakesIndexes.add(new IntRange(begin, end));
            begin = end;
        }
        return mistakesIndexes;
    }
}
