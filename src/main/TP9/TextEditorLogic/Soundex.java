package main.TP9.TextEditorLogic;

import main.TP9.Exceptions.EmptyStringExc;

/**
 * Class containing a method that returns the Soundex code of a given string.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class Soundex {

    /**
     * Takes a String and returns its Soundex code. That is, a code representing that string phonetically.
     * @param s given String
     * @return soundex code of s.
     */
    public static String soundex(String s) {
        if(s.length() <= 0) throw new EmptyStringExc();
        char[] x = s.toUpperCase().toCharArray();
        char firstLetter = x[0];

        // convert letters to numeric code
        for (int i = 0; i < x.length; i++) {
            switch (x[i]) {

                case 'B':
                case 'F':
                case 'P':
                case 'V':
                    x[i] = '1';
                    break;

                case 'C':
                case 'G':
                case 'J':
                case 'K':
                case 'Q':
                case 'S':
                case 'X':
                case 'Z':
                    x[i] = '2';
                    break;

                case 'D':
                case 'T':
                    x[i] = '3';
                    break;

                case 'L':
                    x[i] = '4';
                    break;

                case 'M':
                case 'N':
                    x[i] = '5';
                    break;

                case 'R':
                    x[i] = '6';
                    break;

                default:
                    x[i] = '0';
                    break;
            }
        }

        // remove duplicates
        StringBuilder output = new StringBuilder("" + firstLetter);
        for (int i = 1; i < x.length; i++)
            if (x[i] != x[i-1] && x[i] != '0')
                output.append(x[i]);

        // pad with 0's or truncate
        output.append("0000");
        return output.substring(0, 4);
    }

}
