package main.TP9.TextEditorView;

import main.TP9.Exceptions.NoValueExc;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * JPanel containing a list of strings representing spelling mistakes.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class MistakeListPanel extends JPanel{

    private JList mistakeList;
    private JLabel warning;
    private DefaultListModel<String> mistakeListModel;

    public MistakeListPanel(ActionListener mistakeButtonAL) {
        super();
        double systemScaling = ScalingUtil.getSystemScaling();
        LayoutUtil boxUtil = new LayoutUtil(systemScaling);

        Font font = new Font("Mistakes", Font.PLAIN, (int) (18 * systemScaling));

        warning = new JLabel("Please select a word");
        warning.setFont(font);
        warning.setAlignmentX(CENTER_ALIGNMENT);
        hideWarning();

        mistakeListModel = new DefaultListModel<>();
        mistakeList = new JList<>(mistakeListModel);
        mistakeList.setFont(font);
        JScrollPane scrollPane = new JScrollPane(mistakeList);

        JButton mistakeButton = boxUtil.createButton("Correct", 100, 30);
        mistakeButton.setAlignmentX(CENTER_ALIGNMENT);
        mistakeButton.addActionListener(mistakeButtonAL);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(scrollPane);
        add(boxUtil.createFiller(15));
        add(warning);
        add(boxUtil.createFiller(15));
        add(boxUtil.createFiller(30));
        add(mistakeButton);
    }

    /**
     * Adds a mistake to the list.
     * @param mistake mistake to be added.
     */

    public void addMistake(String mistake){
        mistakeListModel.addElement(mistake);
    }

    /**
     * @param index index of the desired mistake
     * @return mistake in the given index
     */
    public String get(int index){
        return mistakeListModel.get(index);
    }

    /**
     * @return selected index in the list.
     */
    public int getSelectedIndex(){
        int selectedIndex = mistakeList.getSelectedIndex();
        if(selectedIndex < 0) throw new NoValueExc("No selected value");
        return selectedIndex;
    }

    /**
     * Clears the list
     */
    public void clear(){
        mistakeListModel.clear();
    }

    /**
     * Hides the warning label.
     */
    public void hideWarning(){
        warning.setForeground(getBackground());
    }

    /**
     * Shows the warning label.
     */
    public void showWarning(){
        warning.setForeground(Color.RED);
    }

}
