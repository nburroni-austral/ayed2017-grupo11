package main.TP9.TextEditorView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;

/**
 * JFrame containing the textEditor and mistakeList panels.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class TextEditorFrame extends JFrame{

    public TextEditorFrame(TextEditorPanel textEditorPanel, MistakeListPanel mistakeListPanel){
        super("Text Editor");

        LayoutUtil boxUtil = new LayoutUtil(ScalingUtil.getSystemScaling());

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.X_AXIS));
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(textEditorPanel);
        middleContainer.add(boxUtil.createFiller(30));
        middleContainer.add(mistakeListPanel);
        middleContainer.add(boxUtil.createFiller(30));

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(boxUtil.createFiller(30));
        add(middleContainer);
        add(boxUtil.createFiller(30));

        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
