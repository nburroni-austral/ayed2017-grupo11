package main.TP9.TextEditorView;


import main.TP9.TextEditorLogic.IntRange;
import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * JPanel containing a text area that can be marked in red and a "Check" button.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */
public class TextEditorPanel extends JPanel {

    private JTextArea textArea;
    private ArrayList<IntRange> markedPositions;
    private boolean marked;
    private ArrayList<Object> highlights;

    public TextEditorPanel(DocumentListener DL, ActionListener checkBtnAL) {
        super();
        marked = false;
        markedPositions = new ArrayList<>();
        highlights = new ArrayList<>();
        double systemScaling = ScalingUtil.getSystemScaling();

        textArea = new JTextArea(10, 30);
        textArea.getDocument().addDocumentListener(DL);
        textArea.setFont(new Font("Editor", Font.PLAIN, (int) (15 * systemScaling)));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        JScrollPane textScrollPane = new JScrollPane(textArea);

        LayoutUtil boxUtil = new LayoutUtil(systemScaling);
        JButton markButton = boxUtil.createButton("Check", 200, 30);
        markButton.addActionListener(checkBtnAL);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(textScrollPane);
        add(boxUtil.createFiller(30));
        add(markButton);
    }

    /**
     * Marks a range of characters in the text area.
     * @param range range to be marked.
     */

    public void mark(IntRange range){
        marked = true;
        Highlighter highlighter = textArea.getHighlighter();
        Highlighter.HighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
        try {
            highlights.add(highlighter.addHighlight(range.getBegin(), range.getEnd(), painter));
        } catch (BadLocationException exc){
            exc.printStackTrace();
        }
        markedPositions.add(range);
        validate();
    }

    /**
     * Returns the marked range in the given index of the list.
     * @param index index of the desired range.
     * @return range in the given index.
     */

    public IntRange getMarkedPositions(int index){
        return markedPositions.get(index);
    }

    /**
     * Unmarks the given range characters in the text area.
     * @param range range to be unmarked.
     */
    public void unmark(IntRange range){
        for (IntRange mistakeRange: markedPositions) {
            if(mistakeRange.equals(range)){
                int mistakeIndex = markedPositions.indexOf(mistakeRange);
                textArea.getHighlighter().removeHighlight(highlights.get(mistakeIndex));
                highlights.remove(mistakeIndex);
                break;
            }
        }
        validate();
    }

    /**
     * Removes a range of characters in the text area.
     * @param range range of characters to be removed.
     */
    public void removeString(IntRange range){
        try {
            Document document = textArea.getDocument();
            document.remove(range.getBegin(), range.getLength());
        } catch (BadLocationException exc){
            exc.printStackTrace();
        }
    }

    /**
     * Inserts a string in the text area on an specific index.
     * @param begin index where to insert the string.
     * @param s String to be inserted.
     */
    public void insertString(int begin, String s){
        try {
            textArea.getDocument().insertString(begin, s, null);
        } catch (BadLocationException exc){
            exc.printStackTrace();
        }
    }

    /**
     * Unmarks everything in the text area.
     */
    public void unmarkAll(){
        if(marked){
            textArea.getHighlighter().removeAllHighlights();
            markedPositions.clear();
            validate();
            marked = false;
        }
    }

    /**
     * @return text in the text area.
     */
    public String getText() {
        return textArea.getText();
    }
}