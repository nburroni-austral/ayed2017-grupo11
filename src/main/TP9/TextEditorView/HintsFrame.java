package main.TP9.TextEditorView;

import main.Utils.SwingUtil.LayoutUtil;
import main.Utils.SwingUtil.ScalingUtil;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * JFrame containing a list of strings, a cancel and a correct button. The list represents hints for the TextEditor.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */

public class HintsFrame extends JFrame{

    private JList<String> list;

	public HintsFrame(ArrayList<String> hints, ActionListener correctBtnAl, ActionListener cancelBtnAl){
        super("Hints");
		double systemScaling = ScalingUtil.getSystemScaling();
		LayoutUtil boxUtil = new LayoutUtil(systemScaling);

        list = new JList<>();
        list.setFont(new Font("Hints", Font.PLAIN, (int) (20 * systemScaling)));
		setUpList(hints);
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setPreferredSize(new Dimension((int) (systemScaling * 130),(int) (systemScaling * 250)));

		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		btnPanel.setSize(new Dimension(list.getWidth(), list.getHeight()));

		JButton correctBtn = boxUtil.createButton("Correct" , (int) (systemScaling * 100), (int) (systemScaling * 25));
		correctBtn.addActionListener(correctBtnAl);
		btnPanel.add(correctBtn);

		JButton cancelBtn = boxUtil.createButton("Cancel" , (int) (systemScaling * 100), (int) (systemScaling * 25));
		cancelBtn.addActionListener(cancelBtnAl);
		btnPanel.add(cancelBtn);

        JPanel middleContainer = new JPanel();
        middleContainer.setLayout(new BoxLayout(middleContainer, BoxLayout.Y_AXIS));
        middleContainer.add(scrollPane);
        middleContainer.add(boxUtil.createFiller(10));
        middleContainer.add(btnPanel);

        JPanel horizontalContainer = new JPanel();
        horizontalContainer.setLayout(new BoxLayout(horizontalContainer, BoxLayout.X_AXIS));
        horizontalContainer.add(boxUtil.createFiller(30));
		horizontalContainer.add(middleContainer);
		horizontalContainer.add(boxUtil.createFiller(30));

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        getContentPane().add(boxUtil.createFiller(30));
		getContentPane().add(horizontalContainer);
		getContentPane().add(boxUtil.createFiller(30));

		setResizable(false);
		pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}

    private void setUpList(ArrayList<String> hints){
        DefaultListModel<String> listModel = new DefaultListModel<>();
        hints.forEach(listModel::addElement);
        list.setModel(listModel);
    }

    /**
     * Gets the currently selected string.
     * @return string at selected index.
     */
    public String getSelectedHint(){
    	return list.getSelectedValue();
	}
}
