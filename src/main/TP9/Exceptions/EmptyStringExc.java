package main.TP9.Exceptions;

/**
 * @author Tomas Perez Molina
 */
public class EmptyStringExc extends RuntimeException {
    public EmptyStringExc() {
        super("The string is empty");
    }
}
