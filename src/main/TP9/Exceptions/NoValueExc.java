package main.TP9.Exceptions;

/**
 * @author Tomas Perez Molina
 */
public class NoValueExc extends RuntimeException {
    public NoValueExc(String message) {
        super(message);
    }
}
