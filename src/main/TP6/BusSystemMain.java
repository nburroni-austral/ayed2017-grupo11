package main.TP6;

import main.Utils.Scanner.ScannerUtil;
public class BusSystemMain {

    static BusSystem bs;
    public static void main(String[] args){
        ScannerUtil s = new ScannerUtil();
        bs = new BusSystem();
        int in = -1;
        while (in != 0){
            printMenu();
            in = s.getInt();
            switch (in) {
                case 1:
                    addBus(s);
                    break;
                case 2:
                    removeBus(s);
                    break;
                case 3:
                    printBusReport();
                    break;
                case 4:
                    printLineReport(s);
                    break;
                case 5:
                    printDisabledSuitable();
                    break;
                case 6:
                    printMoreThanNSeats(s);
                    break;
                case 7:
                    saveList(s);
                    break;
                case 8:
                    loadList(s);
                    break;
            }

        }
    }

    private static void printMenu(){
        System.out.println("1) Add Bus");
        System.out.println("2) Remove Bus");
        System.out.println("3) Full Report");
        System.out.println("4) Line Report");
        System.out.println("5) Report amount of disabled suitable buses per line");
        System.out.println("6) Report amount of buses with more than X seats");
        System.out.println("7) Save bus list");
        System.out.println("8) Load bus list");
        System.out.println("0) Exit");
        System.out.println("\n");
    }

    private static void addBus(ScannerUtil s){
        System.out.println("Enter the line number");
        int line = s.getInt();
        System.out.println("Enter the intern number");
        int intern = s.getInt();
        System.out.println("Enter the amount of seats");
        int seats = s.getInt();
        System.out.println("Is it disabled suitable? (y/n)");
        boolean disabledSuitable;
        while (true) {
            String input = s.getString();
            if (input.equals("y")) {
                disabledSuitable = true;
                break;
            }
            else if (input.equals("n")) {
                disabledSuitable = false;
                break;
            }
        }
        Bus b = new Bus(line, intern, seats, disabledSuitable);
        bs.addBus(b);
    }

    private static void removeBus(ScannerUtil s){
        System.out.println("Enter the line number");
        int line = s.getInt();
        System.out.println("Enter the intern number");
        int intern = s.getInt();
        Bus b = new Bus(line, intern, 0, false);
        bs.deleteBus(b);
    }

    private static void printBusReport(){
        System.out.println(bs.report());
    }

    private static void printDisabledSuitable(){
        System.out.println(bs.reportDisabledSuitablePerLine());
    }

    private static void printMoreThanNSeats(ScannerUtil s){
        System.out.println("Enter the minimum amount of seats");
        int amount = s.getInt();
        System.out.println(bs.reportMoreThanNSeats(amount));
    }

    private static void saveList(ScannerUtil s){
        System.out.println("Enter the file name");
        String fileName = s.getString();
        bs.saveToFile(fileName);
    }

    private static void loadList(ScannerUtil s){
        System.out.println("Enter the file name");
        String fileName = s.getString();
        bs.recoverList(fileName);
    }

    private static void printLineReport(ScannerUtil s){
        System.out.println("Enter the line number");
        int line = s.getInt();
        System.out.println(bs.reportLine(line));
    }

}