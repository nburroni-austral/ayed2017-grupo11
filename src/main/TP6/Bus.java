package main.TP6;

import java.io.Serializable;

/**
 * Bus represents a bus with a line, intern, amount of seats and if it is disabled Suitable.
 */

public class Bus implements Comparable<Bus>, Serializable{
    private int line, intern, seats;
    private boolean disabledSuitable;

    public Bus(int line, int intern, int seats, boolean disabledSuitable){
        this.line = line;
        this.intern = intern;
        this.seats = seats;
        this.disabledSuitable = disabledSuitable;
    }

    public int getLine() {
        return line;
    }

    public int getIntern() {
        return intern;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isDisabledSuitable() {
        return disabledSuitable;
    }

    @Override
    public int compareTo(Bus o) {
        if(line == o.getLine())
            return intern < o.getIntern() ? -1 : intern > o.getIntern() ? -1 : 0;
        return line < o.getLine() ? -1 : 1;
    }

    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("Line: ").append(line).append("\n")
                .append("Intern: ").append(intern).append("\n")
                .append("Seats: ").append(seats).append("\n")
                .append("Disabled Suitable: ").append(disabledSuitable ? "yes" : "no").append("\n");
        return  result.toString();
    }
}