package main.TP6;

import main.Utils.ObjectUtil.ObjectSaver;
import struct.impl.DynamicSortedList;

/**
 * BusSystem manages a DynamicSortedList of buses and is able to make reports from it.
 */
public class BusSystem {
    private DynamicSortedList<Bus> buses;

    public BusSystem(){
        buses = new DynamicSortedList<>();
    }

    /**
     * Adds a bus to the buses list
     * @param bus bus to add
     */
    public void addBus(Bus bus){
        buses.insert(bus);
    }

    /**
     * Deletes a bus from the list
     * @param bus bus to delete
     */
    public void deleteBus(Bus bus) {
        int index = getIndex(bus);
        if (index >= 0) {
            buses.goTo(index);
            buses.remove();
        }
    }

    private int getIndex(Bus bus){
        for(int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if(buses.getActual().compareTo(bus) == 0)
                return i;
        }
        return -1;
    }

    /**
     * Creates a bus report.
     * @return the created report
     */
    public String report(){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            stringBuilder.append(buses.getActual().toString()).append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Creates a report of a bus line
     * @param line line to make a report from
     * @return the created report
     */
    public String reportLine(int line){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if(buses.getActual().getLine() == line)
                stringBuilder.append(buses.getActual().toString()).append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Creates a report of the disabled suitable buses per line
     * @return the created report
     */
    public String reportDisabledSuitablePerLine(){
        StringBuilder report = new StringBuilder();
        int line = -1;
        int disCounter = 0;
        for(int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if(buses.getActual().getLine() != line){
                if(line != -1){
                    report.append("Disabled suitable buses: ").append(disCounter).append("\n");
                    disCounter = 0;
                }
                line = buses.getActual().getLine();
                report.append("LINE ").append(line).append(": \n");
            }
            disCounter += buses.getActual().isDisabledSuitable() ? 1 : 0;
        }
        if(line != -1)
            report.append("Disabled suitable buses: ").append(disCounter).append("\n");

        return report.toString();
    }

    /**
     * Creates a report of buses with more than N seats per line
     * @param seats N amount of seats
     * @return the created report
     */
    public String reportMoreThanNSeats(int seats){
        StringBuilder report = new StringBuilder();
        int line = -1;
        int counter = 0;
        for(int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if(buses.getActual().getLine() != line){
                if(line != -1){
                    report.append("Buses with more than ").append(seats).append(" seats: ").append(counter).append("\n");
                    counter = 0;
                }
                line = buses.getActual().getLine();
                report.append("LINE ").append(" ").append(line).append(": \n");
            }
            counter += buses.getActual().getSeats() > seats ? 1 : 0;
        }

        if(line != -1)
            report.append("Buses with more than ").append(seats).append(" seats: ").append(counter).append("\n");

        return report.toString();
    }

    /**
     * Saves the buses list to a file
     * @param filename name of the file
     */
    public void saveToFile(String filename){
        ObjectSaver objectSaver = new ObjectSaver();
        objectSaver.writeObject(buses, filename);
    }

    /**
     * Reads the buses list from a file
     * @param filename name of the file
     */
    public void recoverList(String filename){
        ObjectSaver objectSaver = new ObjectSaver();
        DynamicSortedList<Bus> aux = (DynamicSortedList<Bus>) objectSaver.readObject(filename);
        if (aux != null)
            buses = aux;
    }
}
