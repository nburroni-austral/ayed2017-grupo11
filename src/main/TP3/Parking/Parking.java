package main.TP3.Parking;

import struct.impl.DynamicStack;

/**
 * Represents a parking lot with a one way entrance.
 * Keeps record of the income.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class Parking {

    private DynamicStack<Car> parking;
    private int capacity, income, cost;

    public Parking(){
        parking = new DynamicStack<>();
        capacity = 50;
        cost = 5;
        income = 0;
    }

    /**
     * Adds the received car to the top of the parking Stack.
     * Sums 5 to the income.
     * @param car Car to be parked.
     */
    public void parkCar(Car car){
        if(!isFull()) {
            parking.push(car);
            income += cost;
        }
    }

    /**
     * Adds the received car to the top of the parking Stack.
     * @param car
     */
    private void reparkCar(Car car){
        parking.push(car);
    }

    /**
     * Checks if the parking is full.
     * @return True if the parking is full, false if not.
     */
    public boolean isFull(){
        return parking.size() >= capacity;
    }

    /**
     * Removes the received car from the parking Stack
     * if the car is in it. Otherwise, does nothing.
     * The parking Stack order remains the same.
     * @param car Car to be removed.
     */
    public void removeCar(Car car){
        Car[] street = new Car[parking.size()];
        int i = -1;
        while(!parking.peek().equals(car) && !parking.isEmpty()){
            street[++i] = parking.peek();
            parking.pop();
        }
        if(!parking.isEmpty())
            parking.pop();
        for(;i >= 0;i--){
            reparkCar(street[i]);
        }
    }

    /**
     * Removes the  from the parking Stack
     * if the car is in it. Otherwise, does nothing.
     * The parking Stack order remains the same.
     * @param place Index in the parking Stack where the car
     *              to be removed is.
     */
    public void removeCar(int place){
        if(place > parking.size() || place > capacity)
            return;

        Car[] street = new Car[place-1];
        for(int i = 0; i < place-1;i++){
            street[i] = parking.peek();
            parking.pop();
        }
        parking.pop();
        for(int i = place-2;i >= 0;i--){
            reparkCar(street[i]);
        }
    }

    public String toString(){
        return parking.toString();
    }

    public int getIncome(){
        return income;
    }

}
