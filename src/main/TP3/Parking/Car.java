package main.TP3.Parking;

/**
 * Represents a car with a licensePlate, a model and a color.
 *
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class Car{
    private String licensePlate, model, color;

    public Car(String licensePlate, String model, String color) {
        this.licensePlate = licensePlate;
        this.model = model;
        this.color = color;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    /**
     * Decides whether the received car is equals to this one
     * by comparing their licensePlate attribute.
     * @param car Car to be compared to.
     * @return True if they are equals, false if not.
     */
    public boolean equals(Car car){
        return car.getLicensePlate().equals(licensePlate);
    }

    public String toString(){
        return "License plate: " + licensePlate;
    }
}
