package main.TP3.StringCalculator.Exceptions;

public class InvalidCharExc extends RuntimeException {
    public InvalidCharExc() {
        super("Invalid Character");
    }
}
