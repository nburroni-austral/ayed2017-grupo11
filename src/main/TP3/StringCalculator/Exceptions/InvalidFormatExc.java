package main.TP3.StringCalculator.Exceptions;

public class InvalidFormatExc extends RuntimeException {
    public InvalidFormatExc() {
        super("Invalid Format");
    }
}
