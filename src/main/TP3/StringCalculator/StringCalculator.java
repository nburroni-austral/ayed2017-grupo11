package main.TP3.StringCalculator;

import main.TP3.StringCalculator.Exceptions.InvalidCharExc;
import main.TP3.StringCalculator.Exceptions.InvalidFormatExc;
import struct.impl.DynamicStack;

/**
 * StringCalculator has a method to receive a String with a valid mathematical operation format and return its result.
 *
 * @author Tomas Perez Molina
 * @author Manuel Pedrozo
 */

public class StringCalculator {
    private String currentOperation;

    /**
     * Receives a String starting with a number, with alternating operation signs "+, -, /, *" and ending in a number.
     * The operation must be valid without using parenthesis, so "5*-6" does not work, as the correct way to write it is "5*(-6)".
     * Returns the operation result as a double.
     *
     * @param operation String containing the mathematical operation
     * @return operation result
     */

    public double calculate(String operation){
        DynamicStack<Double> finalSumStack = new DynamicStack<>();
        currentOperation = operation;
        Double current = extractNumber(0);
        finalSumStack.push(current);
        while(currentOperation.length() > 0){
            switch (currentOperation.charAt(0)){
                case '+':
                    current = extractNumber(1);
                    finalSumStack.push(current);
                    break;
                case '-':
                    current = extractNumber(1);
                    finalSumStack.push(-current);
                    break;
                case '*':
                    current = extractNumber(1);
                    current *= finalSumStack.peek();
                    finalSumStack.pop();
                    finalSumStack.push(current);
                    break;
                case '/':
                    current = extractNumber(1);
                    current = finalSumStack.peek() / current;
                    finalSumStack.pop();
                    finalSumStack.push(current);
                    break;
                default:
                    throw new InvalidCharExc();
            }
        }
        double result = 0;
        while(finalSumStack.size() > 0){
            result += finalSumStack.peek();
            finalSumStack.pop();
        }
        return result;
    }

    /**
     * Takes an integer marking the position of the first number in the currentOperation String and returns that number.
     * Cuts the currentOperation string so it now starts with an operation symbol.
     *
     * @param start position of the first number in currentOperation
     * @return the first number in currentOperation
     */

    private Double extractNumber(int start){
        int numberEnd = start;
        for (; numberEnd < currentOperation.length(); numberEnd++) {
            char current = currentOperation.charAt(numberEnd);
            if(!Character.isDigit(current)){
                break;
            }
        }
        try {
            Integer result = Integer.parseInt(currentOperation.substring(start, numberEnd));
            currentOperation = currentOperation.substring(numberEnd);
            return new Double(result);
        } catch (NumberFormatException exc){
            throw new InvalidFormatExc();
        }
    }

}
