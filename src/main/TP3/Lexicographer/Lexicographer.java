package main.TP3.Lexicographer;

import struct.impl.DynamicStack;

import java.io.*;

/**
 * Lexicographer reads a text file and verifies that every
 * brace, bracket and parenthesis opened is correctly closed.
 * @author Manuel Pedrozo
 * @author Tomas Perez Molina
 */
public class Lexicographer {

    private DynamicStack<Character> symbolStack;

    public Lexicographer(){
        symbolStack = new DynamicStack<>();
    }

    /**
     * Reads a text file and verifies that every brace,
     * bracket and parenthesis opened is correctly closed.
     *
     * @param filename String that specifies the name of the file to be checked.
     * @return true if all braces, brackets and parenthesis in the file are correctly closed.
    false if not.
     */
    public boolean checkFile(String filename){

        BufferedReader reader = null;

        try {

            File file = new File(filename);
            reader = new BufferedReader(new FileReader(file));

            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null){
                lineNumber++;
                if(!checkLine(line)){
                    System.out.println("Error in line " + lineNumber);
                    return false;
                }
            }
            if(symbolStack.size() > 0){
                System.out.println("'" + symbolStack.peek() + "' was never closed");
                return false;
            }

        } catch (IOException e) {
            System.out.println("File not found");
        } finally {
            symbolStack.empty();
            try {
                reader.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * Iterates through the characters in the received line:
     * - If the character is an opening brace, bracket or parenthesis,
     * then it is added to the symbolStack Stack.
     * - If the character is a closing brace, bracket or parenthesis
     * that closes the top symbol in the symbolStack, the symbolStack
     * is popped. Otherwise, returns false.
     *
     * @param line String line to be checked.
     * @return false if closing a never opened brace, bracket or parenthesis
        is attempted. True if not.
     */
    private boolean checkLine(String line){
        for (int i = 0; i < line.length(); i++) {
            int symbolType = symbolType(line.charAt(i));
            if(symbolType == 2) {
                if (!closesTopSymbol(line.charAt(i)))
                    return false;
                symbolStack.pop();
            }
            else if(symbolType == 1)
                symbolStack.push(line.charAt(i));
        }
        return true;
    }

    /**
     * Determines the type of character received.
     *
     * @param c Character to be analysed.
     * @return 1 if c is an opening brace, bracket or parenthesis.
            2 if c is a closing brace, bracket or parenthesis.
            0 if c is neither a brace, bracket nor parenthesis.
     */
    private int symbolType(Character c){
        if(c == '{' || c == '(' || c == '[')
            return 1;
        if(c == '}' || c == ')' || c == ']')
            return 2;
        return 0;
    }

    private boolean closesTopSymbol(char c){
        if(symbolStack.isEmpty())
            return false;

        switch (c){
            case '}': return symbolStack.peek() == '{';
            case ')': return symbolStack.peek() == '(';
            case ']': return symbolStack.peek() == '[';
        }

        return false;
    }


}
