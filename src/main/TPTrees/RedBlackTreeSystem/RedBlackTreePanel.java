package main.TPTrees.RedBlackTreeSystem;

import main.Utils.SwingUtil.ScalingUtil;
import struct.impl.RedBlackTree.NodeColor;
import struct.impl.RedBlackTree.RedBlackNode;
import struct.impl.RedBlackTree.RedBlackTree;
import javax.swing.*;
import java.awt.*;

/**
 * @author Tomas Perez Molina
 */
public class RedBlackTreePanel extends JPanel {
    private RedBlackTree tree;
    private int X_CONSTANT;
    private int Y_CONSTANT = 100;
    private int nodeSize = 50;

    public RedBlackTreePanel(RedBlackTree tree){
        super();
        this.tree = tree;
        setVisible(true);
        double systemScaling = ScalingUtil.getSystemScaling();
        Y_CONSTANT = (int) (70 * systemScaling);
        int scaledFont = (int) (12 * systemScaling);
        nodeSize = (int) (40 * systemScaling);
        setFont(new Font("Arial",Font.BOLD,scaledFont));
        setup(tree.getRoot());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        paint(tree.getRoot(), g);
    }

    private void paint(RedBlackNode node, Graphics g){
        if(node != null) {
            g.setColor(Color.BLACK);
            if(node.left != null) g.drawLine(node.x, node.y, node.left.x, node.left.y);
            if(node.right != null) g.drawLine(node.x, node.y, node.right.x, node.right.y);
            if (node.type == NodeColor.RED) g.setColor(Color.RED);
            g.fillOval(node.x - nodeSize/2, node.y - nodeSize/2, nodeSize, nodeSize);
            g.setColor(Color.WHITE);
            drawCenteredString(node.data.toString(), g, new Rectangle(node.x - nodeSize/2, node.y - nodeSize/2, nodeSize, nodeSize));
            paint(node.left, g);
            paint(node.right, g);
        }
    }

    private void setup(RedBlackNode node){
        int levels = getLevels(node);
        setCoordinates(node, 0, levels - 1, 1);
        Dimension panelSize = new Dimension((int)(Math.pow(2, levels) - 1) * nodeSize, (levels + 1) * Y_CONSTANT);
        setMinimumSize(panelSize);
        setPreferredSize(panelSize);
        setMaximumSize(panelSize);
        setSize(panelSize);
        X_CONSTANT = panelSize.width / node.x / 2;
        scaleCoordinates(node);
    }

    private void setCoordinates(RedBlackNode node, int level, int maxLevel, int nodeNumber) {
        if(node != null){
            int startX = 0;
            for(int i = 0; i < maxLevel - level + 1; i++){
                startX += Math.pow(2, i-1);
            }
            node.x = (int) (startX + 1 + (nodeNumber - 1) * Math.pow(2, maxLevel - level + 1));
            node.y = (level + 1);
            setCoordinates(node.left, level + 1, maxLevel, (2 * nodeNumber) - 1);
            setCoordinates(node.right, level + 1, maxLevel, (2 * nodeNumber));
        }
    }

    private void scaleCoordinates(RedBlackNode node){
        if(node != null){
            node.x *= X_CONSTANT;
            node.y *= Y_CONSTANT;
            scaleCoordinates(node.left);
            scaleCoordinates(node.right);
        }
    }

    private int getLevels(RedBlackNode node){
        if(node == null) return 0;
        return 1 + Math.max(getLevels(node.left), getLevels(node.right));
    }

    private void drawCenteredString(String text, Graphics g, Rectangle rect){
        FontMetrics metrics = g.getFontMetrics();
        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        g.drawString(text, x, y);
    }
}
