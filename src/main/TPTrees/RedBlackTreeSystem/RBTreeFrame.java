package main.TPTrees.RedBlackTreeSystem;

import struct.impl.RedBlackTree.RedBlackTree;

import javax.swing.*;

/**
 * @author Tomas Perez Molina
 */
public class RBTreeFrame extends JFrame {

    public RBTreeFrame(RedBlackTree tree){
        super("Red Black Tree");
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(new RedBlackTreePanel(tree));
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
    }
}
