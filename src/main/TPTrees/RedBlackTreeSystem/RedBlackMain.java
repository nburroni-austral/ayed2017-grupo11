package main.TPTrees.RedBlackTreeSystem;
import main.Utils.Scanner.ScannerUtil;
import struct.impl.Exceptions.ObjectAlreadyInTreeException;
import struct.impl.RedBlackTree.RedBlackTree;

/**
 * @author Tomas Perez Molina
 */
public class RedBlackMain {
    public static void main(String[] args) {
        ScannerUtil scannerUtil = new ScannerUtil();
        System.out.println("-- RedBlackTree creator --");
        System.out.println("Insert a DNI, 0 to exit");
        Integer dni = scannerUtil.getInt();
        RedBlackTree<Integer> tree = new RedBlackTree<>(dni);
        boolean treeCreated = false;
        while (dni > 0) {
            if(!treeCreated) treeCreated = true;
            else{
                try{
                    tree.insert(dni);
                }
                catch (ObjectAlreadyInTreeException exc){
                    System.out.println(exc.getMessage());
                }
            }
            dni = scannerUtil.getInt();
        }
        if(treeCreated) new RBTreeFrame(tree);
    }
}
