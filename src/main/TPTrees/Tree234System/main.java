package main.TPTrees.Tree234System;

import struct.impl.Exceptions.ObjectAlreadyInTreeException;
import struct.impl.Tree234.Tree234;
import main.Utils.Scanner.ScannerUtil;

public class main {

    public static void main(String[] args) {
        ScannerUtil scannerUtil = new ScannerUtil();
        System.out.println("-- Tree 234 creator --");
        System.out.println("Insert a DNI, 0 to exit");
        Integer dni = scannerUtil.getInt();
        Tree234<Integer> tree;
        if(dni > 0) {
            tree = new Tree234<>();
            while (dni > 0) {
                tree.insert(dni);
                dni = scannerUtil.getInt();
            }
            new View(tree);
        }
    }
}
