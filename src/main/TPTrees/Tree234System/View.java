package main.TPTrees.Tree234System;
import struct.impl.Tree234.Tree234;

import javax.swing.*;
import java.awt.*;

public class View {
    private JFrame frame;
    private JPanel panel;
    private JPanel drawPanel;
    private Tree234 tree;
    private Image im;
    private Graphics g;

    public View(Tree234 tree) {
        this.tree = tree;
        frame = new JFrame("Tree 234");
        frame.setPreferredSize(new Dimension(1920,1080));
        frame.setMinimumSize(new Dimension(1920,1080));
        panel = new JPanel(new BorderLayout());
        drawPanel = new JPanel();
        panel.add(drawPanel,BorderLayout.CENTER);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.pack();
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        gameRender();
        paintScreen();
    }

    private void drawTree(Graphics g) {
        NodeView.draw(tree.getRoot(),g,drawPanel.getWidth()/2);
    }

    private void gameRender()
    {
        if (im == null){
            im = drawPanel.createImage(drawPanel.getWidth(), drawPanel.getHeight());
            if (im == null) {
                System.out.println("dbImage is null");
                return;
            }
            else{
                g = im.getGraphics();
            }

        }
        g.setColor(Color.white);
        g.fillRect (0, 0, drawPanel.getWidth(), drawPanel.getHeight());
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial",Font.BOLD,14));
        drawTree(g);

    }
    private void paintScreen()
    {
        Graphics g;
        try {
            g = drawPanel.getGraphics();
            if ((g != null) && (im != null))
                g.drawImage(im, 0, 0, null);
                g.dispose();
        }
        catch (Exception e)
        { System.out.println("Graphics context error: " + e); }
    }
}
